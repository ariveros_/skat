﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace SKATDesarrollo.Migrations
{
    public partial class HistPuntoScada : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "HistPuntoScada",
                columns: table => new
                {
                    idPuntoScada = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    nroPuntoScada = table.Column<int>(type: "int", nullable: false),
                    fechaCreacionPunto = table.Column<DateTime>(type: "datetime2", nullable: true),
                    fechaFinPunto = table.Column<DateTime>(type: "datetime2", nullable: true),
                    activo = table.Column<bool>(type: "bit", nullable: true),
                    idEquipo = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HistPuntoScada", x => x.idPuntoScada);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "HistPuntoScada");
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace SKATDesarrollo.Migrations
{
    public partial class HistEstadoTiempo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "HistEstadoTiempo",
                columns: table => new
                {
                    idEstadoTiempo = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    estadoTiempo = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    activo = table.Column<bool>(type: "bit", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HistEstadoTiempo", x => x.idEstadoTiempo);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "HistEstadoTiempo");
        }
    }
}

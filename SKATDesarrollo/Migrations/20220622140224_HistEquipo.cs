﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace SKATDesarrollo.Migrations
{
    public partial class HistEquipo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "HistEquipo",
                columns: table => new
                {
                    idEquipo = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    nombreEquipo = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    maxPotencia = table.Column<double>(type: "float", nullable: true),
                    activo = table.Column<bool>(type: "bit", nullable: true),
                    idEstacion = table.Column<int>(type: "int", nullable: true),
                    idTipoEquipo = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HistEquipo", x => x.idEquipo);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "HistEquipo");
        }
    }
}

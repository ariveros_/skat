﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace SKATDesarrollo.Migrations
{
    public partial class HistTipoEquipoTipoMedida : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "HistTipoEquipoTipoMedida",
                columns: table => new
                {
                    idTipoEquipoTipoMedida = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    idTipoEquipo = table.Column<int>(type: "int", nullable: true),
                    idTipoMedida = table.Column<int>(type: "int", nullable: true),
                    activo = table.Column<bool>(type: "bit", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HistTipoEquipoTipoMedida", x => x.idTipoEquipoTipoMedida);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "HistTipoEquipoTipoMedida");
        }
    }
}

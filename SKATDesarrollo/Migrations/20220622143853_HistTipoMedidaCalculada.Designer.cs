﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using SKATDesarrollo.Context;

#nullable disable

namespace SKATDesarrollo.Migrations
{
    [DbContext(typeof(SegSkaContext))]
    [Migration("20220622143853_HistTipoMedidaCalculada")]
    partial class HistTipoMedidaCalculada
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "6.0.2")
                .HasAnnotation("Relational:MaxIdentifierLength", 128);

            SqlServerModelBuilderExtensions.UseIdentityColumns(modelBuilder, 1L, 1);

            modelBuilder.Entity("SKATDesarrollo.Models.HistEquipo", b =>
                {
                    b.Property<int>("idEquipo")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("idEquipo"), 1L, 1);

                    b.Property<bool?>("activo")
                        .HasColumnType("bit")
                        .HasColumnName("activo");

                    b.Property<int?>("idEstacion")
                        .HasColumnType("int")
                        .HasColumnName("idEstacion");

                    b.Property<int?>("idTipoEquipo")
                        .HasColumnType("int")
                        .HasColumnName("idTipoEquipo");

                    b.Property<double?>("maxPotencia")
                        .HasColumnType("float")
                        .HasColumnName("maxPotencia");

                    b.Property<string>("nombreEquipo")
                        .HasColumnType("nvarchar(max)")
                        .HasColumnName("nombreEquipo");

                    b.HasKey("idEquipo");

                    b.ToTable("HistEquipo", (string)null);
                });

            modelBuilder.Entity("SKATDesarrollo.Models.HistEstacion", b =>
                {
                    b.Property<int>("idEstacion")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("idEstacion"), 1L, 1);

                    b.Property<bool?>("activo")
                        .HasColumnType("bit")
                        .HasColumnName("activo");

                    b.Property<string>("nombreEstacion")
                        .HasColumnType("nvarchar(max)")
                        .HasColumnName("nombreEstacion");

                    b.Property<string>("zonaEstacion")
                        .HasColumnType("nvarchar(max)")
                        .HasColumnName("zonaEstacion");

                    b.HasKey("idEstacion");

                    b.ToTable("HistEstacion", (string)null);
                });

            modelBuilder.Entity("SKATDesarrollo.Models.HistEstadoTiempo", b =>
                {
                    b.Property<int>("idEstadoTiempo")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("idEstadoTiempo"), 1L, 1);

                    b.Property<bool?>("activo")
                        .HasColumnType("bit")
                        .HasColumnName("activo");

                    b.Property<string>("estadoTiempo")
                        .HasColumnType("nvarchar(max)")
                        .HasColumnName("estadoTiempo");

                    b.HasKey("idEstadoTiempo");

                    b.ToTable("HistEstadoTiempo", (string)null);
                });

            modelBuilder.Entity("SKATDesarrollo.Models.HistPuntoScada", b =>
                {
                    b.Property<int>("idPuntoScada")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("idPuntoScada"), 1L, 1);

                    b.Property<bool?>("activo")
                        .HasColumnType("bit")
                        .HasColumnName("activo");

                    b.Property<DateTime?>("fechaCreacionPunto")
                        .HasColumnType("datetime2")
                        .HasColumnName("fechaCreacionPunto");

                    b.Property<DateTime?>("fechaFinPunto")
                        .HasColumnType("datetime2")
                        .HasColumnName("fechaFinPunto");

                    b.Property<int?>("idEquipo")
                        .HasColumnType("int")
                        .HasColumnName("idEquipo");

                    b.Property<int>("nroPuntoScada")
                        .HasColumnType("int")
                        .HasColumnName("nroPuntoScada");

                    b.HasKey("idPuntoScada");

                    b.ToTable("HistPuntoScada", (string)null);
                });

            modelBuilder.Entity("SKATDesarrollo.Models.HistRegistroMedida", b =>
                {
                    b.Property<int>("idRegistroMedida")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("idRegistroMedida"), 1L, 1);

                    b.Property<bool?>("activo")
                        .HasColumnType("bit")
                        .HasColumnName("activo");

                    b.Property<DateTime?>("fFin")
                        .HasColumnType("datetime2")
                        .HasColumnName("fFin");

                    b.Property<DateTime?>("fInicio")
                        .HasColumnType("datetime2")
                        .HasColumnName("fInicio");

                    b.Property<int?>("idPuntoScada")
                        .HasColumnType("int")
                        .HasColumnName("idPuntoScada");

                    b.HasKey("idRegistroMedida");

                    b.ToTable("HistRegistroMedida", (string)null);
                });

            modelBuilder.Entity("SKATDesarrollo.Models.HistTipoEquipo", b =>
                {
                    b.Property<int>("idTipoEquipo")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("idTipoEquipo"), 1L, 1);

                    b.Property<bool?>("activo")
                        .HasColumnType("bit")
                        .HasColumnName("activo");

                    b.Property<string>("tipoEquipo")
                        .HasColumnType("nvarchar(max)")
                        .HasColumnName("tipoEquipo");

                    b.HasKey("idTipoEquipo");

                    b.ToTable("HistTipoEquipo", (string)null);
                });

            modelBuilder.Entity("SKATDesarrollo.Models.HistTipoEquipoTipoMedida", b =>
                {
                    b.Property<int>("idTipoEquipoTipoMedida")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("idTipoEquipoTipoMedida"), 1L, 1);

                    b.Property<bool?>("activo")
                        .HasColumnType("bit")
                        .HasColumnName("activo");

                    b.Property<int?>("idTipoEquipo")
                        .HasColumnType("int")
                        .HasColumnName("idTipoEquipo");

                    b.Property<int?>("idTipoMedida")
                        .HasColumnType("int")
                        .HasColumnName("idTipoMedida");

                    b.HasKey("idTipoEquipoTipoMedida");

                    b.ToTable("HistTipoEquipoTipoMedida", (string)null);
                });

            modelBuilder.Entity("SKATDesarrollo.Models.HistTipoMedida", b =>
                {
                    b.Property<int>("idTipoMedida")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("idTipoMedida"), 1L, 1);

                    b.Property<bool?>("activo")
                        .HasColumnType("bit")
                        .HasColumnName("activo");

                    b.Property<string>("medida")
                        .HasColumnType("nvarchar(max)")
                        .HasColumnName("medida");

                    b.Property<string>("unidadMedida")
                        .HasColumnType("nvarchar(max)")
                        .HasColumnName("unidadMedida");

                    b.HasKey("idTipoMedida");

                    b.ToTable("HistTipoMedida", (string)null);
                });

            modelBuilder.Entity("SKATDesarrollo.Models.HistTipoMedidaCalculada", b =>
                {
                    b.Property<int?>("idTipoMCalculada")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int?>("idTipoMCalculada"), 1L, 1);

                    b.Property<bool?>("activo")
                        .HasColumnType("bit")
                        .HasColumnName("activo");

                    b.Property<string>("medidaCalculada")
                        .HasColumnType("nvarchar(max)")
                        .HasColumnName("medidaCalculada");

                    b.Property<string>("unidadMCalculada")
                        .HasColumnType("nvarchar(max)")
                        .HasColumnName("unidadMCalculada");

                    b.HasKey("idTipoMCalculada");

                    b.ToTable("HistTipoMedidaCalculada", (string)null);
                });

            modelBuilder.Entity("SKATDesarrollo.Models.SegLog", b =>
                {
                    b.Property<int>("IdLog")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasColumnName("idLog");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("IdLog"), 1L, 1);

                    b.Property<DateTime?>("TimeLog")
                        .HasColumnType("datetime")
                        .HasColumnName("timeLog");

                    b.Property<int>("TipoLogId")
                        .HasColumnType("int")
                        .HasColumnName("tipoLogID");

                    b.Property<int>("UserId")
                        .HasColumnType("int")
                        .HasColumnName("userID");

                    b.HasKey("IdLog");

                    b.ToTable("SegLog", (string)null);
                });

            modelBuilder.Entity("SKATDesarrollo.Models.SegPassword", b =>
                {
                    b.Property<int>("IdPass")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasColumnName("idPass");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("IdPass"), 1L, 1);

                    b.Property<bool?>("ActivoPass")
                        .HasColumnType("bit")
                        .HasColumnName("activoPass");

                    b.Property<DateTime?>("Desde")
                        .HasColumnType("datetime")
                        .HasColumnName("desde");

                    b.Property<string>("HashPassword")
                        .HasMaxLength(255)
                        .HasColumnType("nvarchar(255)")
                        .HasColumnName("hashPassword");

                    b.Property<DateTime?>("Hasta")
                        .HasColumnType("datetime")
                        .HasColumnName("hasta");

                    b.Property<int>("UserId")
                        .HasColumnType("int")
                        .HasColumnName("userID");

                    b.HasKey("IdPass");

                    b.ToTable("SegPassword", (string)null);
                });

            modelBuilder.Entity("SKATDesarrollo.Models.SegPermiso", b =>
                {
                    b.Property<int>("IdPermiso")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasColumnName("idPermiso");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("IdPermiso"), 1L, 1);

                    b.Property<bool?>("Activo")
                        .IsRequired()
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bit")
                        .HasColumnName("activo")
                        .HasDefaultValueSql("(CONVERT([bit],(0)))");

                    b.Property<string>("DescripcionPermiso")
                        .HasMaxLength(50)
                        .HasColumnType("nvarchar(50)")
                        .HasColumnName("descripcionPermiso");

                    b.HasKey("IdPermiso");

                    b.ToTable("SegPermiso", (string)null);
                });

            modelBuilder.Entity("SKATDesarrollo.Models.SegRol", b =>
                {
                    b.Property<int>("IdRol")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasColumnName("idRol");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("IdRol"), 1L, 1);

                    b.Property<bool?>("Activo")
                        .IsRequired()
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bit")
                        .HasColumnName("activo")
                        .HasDefaultValueSql("(CONVERT([bit],(0)))");

                    b.Property<string>("NombreRol")
                        .HasMaxLength(50)
                        .HasColumnType("nvarchar(50)")
                        .HasColumnName("nombreRol");

                    b.HasKey("IdRol");

                    b.ToTable("SegRol", (string)null);
                });

            modelBuilder.Entity("SKATDesarrollo.Models.SegRolPermiso", b =>
                {
                    b.Property<int>("IdPermisoRol")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasColumnName("idPermisoRol");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("IdPermisoRol"), 1L, 1);

                    b.Property<bool?>("Activo")
                        .IsRequired()
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bit")
                        .HasColumnName("activo")
                        .HasDefaultValueSql("(CONVERT([bit],(0)))");

                    b.Property<int>("PermisoId")
                        .HasColumnType("int")
                        .HasColumnName("permisoID");

                    b.Property<int>("RoleId")
                        .HasColumnType("int")
                        .HasColumnName("roleID");

                    b.HasKey("IdPermisoRol");

                    b.ToTable("SegRolPermiso", (string)null);
                });

            modelBuilder.Entity("SKATDesarrollo.Models.SegTipoLog", b =>
                {
                    b.Property<int>("IdTipoLog")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasColumnName("idTipoLog");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("IdTipoLog"), 1L, 1);

                    b.Property<string>("DescripcionLog")
                        .HasMaxLength(50)
                        .HasColumnType("nvarchar(50)")
                        .HasColumnName("descripcionLog");

                    b.HasKey("IdTipoLog");

                    b.ToTable("SegTipoLog", (string)null);
                });

            modelBuilder.Entity("SKATDesarrollo.Models.SegUser", b =>
                {
                    b.Property<int>("IdUser")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasColumnName("idUser");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("IdUser"), 1L, 1);

                    b.Property<bool?>("ActivoUser")
                        .HasColumnType("bit")
                        .HasColumnName("activoUser");

                    b.Property<string>("Email")
                        .HasMaxLength(50)
                        .HasColumnType("nvarchar(50)")
                        .HasColumnName("email");

                    b.Property<string>("ResetToken")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime?>("ResetTokenExpires")
                        .HasColumnType("datetime2");

                    b.HasKey("IdUser");

                    b.ToTable("SegUser", (string)null);
                });

            modelBuilder.Entity("SKATDesarrollo.Models.SegUserRol", b =>
                {
                    b.Property<int>("IdUserRol")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasColumnName("idUserRol");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("IdUserRol"), 1L, 1);

                    b.Property<bool?>("Activo")
                        .HasColumnType("bit")
                        .HasColumnName("activo");

                    b.Property<int>("RoleId")
                        .HasColumnType("int")
                        .HasColumnName("roleID");

                    b.Property<int>("UserId")
                        .HasColumnType("int")
                        .HasColumnName("userID");

                    b.HasKey("IdUserRol");

                    b.ToTable("SegUserRol", (string)null);
                });
#pragma warning restore 612, 618
        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace SKATDesarrollo.Migrations
{
    public partial class HistValorMedida : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "HistValorMedida",
                columns: table => new
                {
                    idValorMedida = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    calidadMedida = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    valorMedida = table.Column<double>(type: "float", nullable: true),
                    horaMedida = table.Column<DateTime>(type: "datetime2", nullable: true),
                    idTipoMedida = table.Column<int>(type: "int", nullable: true),
                    idEstadoTiempo = table.Column<int>(type: "int", nullable: true),
                    idRegistroMedida = table.Column<int>(type: "int", nullable: true),
                    activo = table.Column<bool>(type: "bit", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HistValorMedida", x => x.idValorMedida);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "HistValorMedida");
        }
    }
}

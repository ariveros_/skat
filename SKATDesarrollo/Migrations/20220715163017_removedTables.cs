﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace SKATDesarrollo.Migrations
{
    public partial class removedTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "HistTipoEquipoTipoMedida");

            migrationBuilder.DropTable(
                name: "HistTipoMedidaCalculada");

            migrationBuilder.DropTable(
                name: "HistTipoMedidaHistorica");

            migrationBuilder.DropTable(
                name: "HistValorCalculado");

            migrationBuilder.DropTable(
                name: "HistValorHistorico");

            migrationBuilder.DropColumn(
                name: "idEstadoTiempo",
                table: "HistValorMedida");

            migrationBuilder.AddColumn<int>(
                name: "idEstadoTiempo",
                table: "HistRegistroMedida",
                type: "int",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "idEstadoTiempo",
                table: "HistRegistroMedida");

            migrationBuilder.AddColumn<int>(
                name: "idEstadoTiempo",
                table: "HistValorMedida",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "HistTipoEquipoTipoMedida",
                columns: table => new
                {
                    idTipoEquipoTipoMedida = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    activo = table.Column<bool>(type: "bit", nullable: true),
                    idTipoEquipo = table.Column<int>(type: "int", nullable: true),
                    idTipoMedida = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HistTipoEquipoTipoMedida", x => x.idTipoEquipoTipoMedida);
                });

            migrationBuilder.CreateTable(
                name: "HistTipoMedidaCalculada",
                columns: table => new
                {
                    idTipoMCalculada = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    activo = table.Column<bool>(type: "bit", nullable: true),
                    medidaCalculada = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    unidadMCalculada = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HistTipoMedidaCalculada", x => x.idTipoMCalculada);
                });

            migrationBuilder.CreateTable(
                name: "HistTipoMedidaHistorica",
                columns: table => new
                {
                    idTipoMHistorica = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    activo = table.Column<bool>(type: "bit", nullable: true),
                    medidaHistorica = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    unidadMHistorica = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HistTipoMedidaHistorica", x => x.idTipoMHistorica);
                });

            migrationBuilder.CreateTable(
                name: "HistValorCalculado",
                columns: table => new
                {
                    idValorCalculado = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    activo = table.Column<bool>(type: "bit", nullable: true),
                    fechaValorCalculado = table.Column<DateTime>(type: "datetime2", nullable: true),
                    idRegistroMedida = table.Column<int>(type: "int", nullable: true),
                    idTipoMCalculada = table.Column<int>(type: "int", nullable: true),
                    valorCalculado = table.Column<double>(type: "float", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HistValorCalculado", x => x.idValorCalculado);
                });

            migrationBuilder.CreateTable(
                name: "HistValorHistorico",
                columns: table => new
                {
                    idValorHistorico = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    activo = table.Column<bool>(type: "bit", nullable: true),
                    fechaValorHistorico = table.Column<DateTime>(type: "datetime2", nullable: true),
                    idRegistroMedida = table.Column<int>(type: "int", nullable: true),
                    idTipoMHistorica = table.Column<int>(type: "int", nullable: true),
                    valorHistorico = table.Column<double>(type: "float", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HistValorHistorico", x => x.idValorHistorico);
                });
        }
    }
}

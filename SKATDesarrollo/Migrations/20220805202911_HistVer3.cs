﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace SKATDesarrollo.Migrations
{
    public partial class HistVer3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "idTipoMedida",
                table: "HistValorMedida");

            migrationBuilder.DropColumn(
                name: "zonaEstacion",
                table: "HistEstacion");

            migrationBuilder.AddColumn<bool>(
                name: "esHistorico",
                table: "HistValorMedida",
                type: "bit",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "fechaHistorico",
                table: "HistValorMedida",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "idTipoMedida",
                table: "HistRegistroMedida",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "idCentroControl",
                table: "HistEstacion",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "idTipoEstacion",
                table: "HistEstacion",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "idZonaEstacion",
                table: "HistEstacion",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "idPropiedad",
                table: "HistEquipo",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "HistCentroControl",
                columns: table => new
                {
                    idCentroControl = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    nombreCentroControl = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    idTipoCentroControl = table.Column<int>(type: "int", nullable: true),
                    activo = table.Column<bool>(type: "bit", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HistCentroControl", x => x.idCentroControl);
                });

            migrationBuilder.CreateTable(
                name: "HistPropiedad",
                columns: table => new
                {
                    idPropiedad = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    nombrePropiedad = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    activo = table.Column<bool>(type: "bit", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HistPropiedad", x => x.idPropiedad);
                });

            migrationBuilder.CreateTable(
                name: "HistTipoCenControl",
                columns: table => new
                {
                    idTipoCentroControl = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    nombreTipoCenControl = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    activo = table.Column<bool>(type: "bit", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HistTipoCenControl", x => x.idTipoCentroControl);
                });

            migrationBuilder.CreateTable(
                name: "HistTipoEquipoTipoMedida",
                columns: table => new
                {
                    idTipoEquipoTipoMedida = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    idTipoMedida = table.Column<int>(type: "int", nullable: true),
                    idTipoEquipo = table.Column<int>(type: "int", nullable: true),
                    activo = table.Column<bool>(type: "bit", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HistTipoEquipoTipoMedida", x => x.idTipoEquipoTipoMedida);
                });

            migrationBuilder.CreateTable(
                name: "HistTipoEstacion",
                columns: table => new
                {
                    idTipoEstacion = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    nombreTipoEstacion = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    activo = table.Column<bool>(type: "bit", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HistTipoEstacion", x => x.idTipoEstacion);
                });

            migrationBuilder.CreateTable(
                name: "HistZonaEstacion",
                columns: table => new
                {
                    idZonaEstacion = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    nombreZona = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    activo = table.Column<bool>(type: "bit", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HistZonaEstacion", x => x.idZonaEstacion);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "HistCentroControl");

            migrationBuilder.DropTable(
                name: "HistPropiedad");

            migrationBuilder.DropTable(
                name: "HistTipoCenControl");

            migrationBuilder.DropTable(
                name: "HistTipoEquipoTipoMedida");

            migrationBuilder.DropTable(
                name: "HistTipoEstacion");

            migrationBuilder.DropTable(
                name: "HistZonaEstacion");

            migrationBuilder.DropColumn(
                name: "esHistorico",
                table: "HistValorMedida");

            migrationBuilder.DropColumn(
                name: "fechaHistorico",
                table: "HistValorMedida");

            migrationBuilder.DropColumn(
                name: "idTipoMedida",
                table: "HistRegistroMedida");

            migrationBuilder.DropColumn(
                name: "idCentroControl",
                table: "HistEstacion");

            migrationBuilder.DropColumn(
                name: "idTipoEstacion",
                table: "HistEstacion");

            migrationBuilder.DropColumn(
                name: "idZonaEstacion",
                table: "HistEstacion");

            migrationBuilder.DropColumn(
                name: "idPropiedad",
                table: "HistEquipo");

            migrationBuilder.AddColumn<int>(
                name: "idTipoMedida",
                table: "HistValorMedida",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "zonaEstacion",
                table: "HistEstacion",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}

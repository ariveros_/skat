﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace SKATDesarrollo.Migrations
{
    public partial class HistRegistroMedida : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "HistRegistroMedida",
                columns: table => new
                {
                    idRegistroMedida = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    fInicio = table.Column<DateTime>(type: "datetime2", nullable: true),
                    fFin = table.Column<DateTime>(type: "datetime2", nullable: true),
                    idPuntoScada = table.Column<int>(type: "int", nullable: true),
                    activo = table.Column<bool>(type: "bit", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HistRegistroMedida", x => x.idRegistroMedida);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "HistRegistroMedida");
        }
    }
}

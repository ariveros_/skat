﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace SKATDesarrollo.Migrations
{
    public partial class SegMenu : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SegMenu",
                columns: table => new
                {
                    idMenu = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    nombreMenu = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    codigoMenu = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    urlMenu = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    iconoMenu = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    nombreMenuOpen = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    esPadre = table.Column<bool>(type: "bit", nullable: true),
                    posicionMenu = table.Column<int>(type: "int", nullable: true),
                    idPadre = table.Column<int>(type: "int", nullable: true),
                    activo = table.Column<bool>(type: "bit", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SegMenu", x => x.idMenu);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SegMenu");
        }
    }
}

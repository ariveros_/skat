﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace SKATDesarrollo.Migrations
{
    public partial class HistValorHistorico : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "HistValorHistorico",
                columns: table => new
                {
                    idValorHistorico = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    valorHistorico = table.Column<double>(type: "float", nullable: true),
                    fechaValorHistorico = table.Column<DateTime>(type: "datetime2", nullable: true),
                    idTipoMHistorica = table.Column<int>(type: "int", nullable: true),
                    idRegistroMedida = table.Column<int>(type: "int", nullable: true),
                    activo = table.Column<bool>(type: "bit", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HistValorHistorico", x => x.idValorHistorico);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "HistValorHistorico");
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace SKATDesarrollo.Migrations
{
    public partial class PotenciaNominal : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "maxPotencia",
                table: "HistEquipo",
                newName: "potenciaNominal");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "potenciaNominal",
                table: "HistEquipo",
                newName: "maxPotencia");
        }
    }
}

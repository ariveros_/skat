﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace SKATDesarrollo.Migrations
{
    public partial class RemoveSegPermisos : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SegPermiso");

            migrationBuilder.DropTable(
                name: "SegRolPermiso");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SegPermiso",
                columns: table => new
                {
                    idPermiso = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    activo = table.Column<bool>(type: "bit", nullable: false, defaultValueSql: "(CONVERT([bit],(0)))"),
                    descripcionPermiso = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SegPermiso", x => x.idPermiso);
                });

            migrationBuilder.CreateTable(
                name: "SegRolPermiso",
                columns: table => new
                {
                    idPermisoRol = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    activo = table.Column<bool>(type: "bit", nullable: false, defaultValueSql: "(CONVERT([bit],(0)))"),
                    permisoID = table.Column<int>(type: "int", nullable: false),
                    roleID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SegRolPermiso", x => x.idPermisoRol);
                });
        }
    }
}

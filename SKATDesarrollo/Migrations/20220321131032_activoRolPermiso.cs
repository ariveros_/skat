﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace SKATDesarrollo.Migrations
{
    public partial class activoRolPermiso : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "activo",
                table: "SegRolPermiso",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "activo",
                table: "SegRolPermiso");
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace SKATDesarrollo.Migrations
{
    public partial class columnaActivo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "activo",
                table: "SegRol",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "activo",
                table: "SegPermiso",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "activo",
                table: "SegRol");

            migrationBuilder.DropColumn(
                name: "activo",
                table: "SegPermiso");
        }
    }
}

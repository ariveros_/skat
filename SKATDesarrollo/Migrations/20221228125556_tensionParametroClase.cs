﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace SKATDesarrollo.Migrations
{
    public partial class tensionParametroClase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "tensionParametro",
                table: "HistPuntoScada");

            migrationBuilder.AddColumn<int>(
                name: "idTension",
                table: "HistPuntoScada",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "HistTension",
                columns: table => new
                {
                    idTension = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    valorTension = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HistTension", x => x.idTension);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "HistTension");

            migrationBuilder.DropColumn(
                name: "idTension",
                table: "HistPuntoScada");

            migrationBuilder.AddColumn<string>(
                name: "tensionParametro",
                table: "HistPuntoScada",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}

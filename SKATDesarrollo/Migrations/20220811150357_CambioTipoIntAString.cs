﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace SKATDesarrollo.Migrations
{
    public partial class CambioTipoIntAString : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
            name: "nroPuntoScada",
            table: "HistPuntoScada");
            migrationBuilder.AddColumn<string>(
                name: "nroPuntoScada",
                table: "HistPuntoScada",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "nroPuntoScada",
                table: "HistPuntoScada");
            migrationBuilder.AddColumn<int>(
                name: "nroPuntoScada",
                table: "HistPuntoScada",
                type: "int",
                nullable: true
                );

        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace SKATDesarrollo.Migrations
{
    public partial class HistValorCalculado : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "HistValorCalculado",
                columns: table => new
                {
                    idValorCalculado = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    valorCalculado = table.Column<double>(type: "float", nullable: true),
                    fechaValorCalculado = table.Column<DateTime>(type: "datetime2", nullable: true),
                    idTipoMCalculada = table.Column<int>(type: "int", nullable: true),
                    idRegistroMedida = table.Column<int>(type: "int", nullable: true),
                    activo = table.Column<bool>(type: "bit", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HistValorCalculado", x => x.idValorCalculado);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "HistValorCalculado");
        }
    }
}

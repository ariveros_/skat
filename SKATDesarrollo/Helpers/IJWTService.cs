﻿using System.IdentityModel.Tokens.Jwt;

namespace SKATDesarrollo.Helpers
{
    public interface IJWTService
    {
        string Generate(int id);
        JwtSecurityToken Verify(string jwt);
    }
}

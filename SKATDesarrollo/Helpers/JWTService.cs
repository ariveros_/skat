﻿using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.IdentityModel.Tokens;
using System.Text;
using SKATDesarrollo.Repository;
using SKATDesarrollo.Context;
using System.Security.Claims;

namespace SKATDesarrollo.Helpers
{
    public class JWTService : IJWTService
    {
        private readonly IUserRepository _userService;
        private readonly IConfiguration _configuration;
        public JWTService(IUserRepository userService, IConfiguration configuration)
        {
            _userService = userService;
            _configuration = configuration;
        }

        //Metodo para crear un JWT String
        public string Generate(int id)
        {
            //claims
            var user = _userService.GetByID(id); //me traigo la info del user

            var claims = new[]
            {
                new Claim(ClaimTypes.Email, user.email),
                new Claim(ClaimTypes.Role, user.rolName),

            };

            //Standar security data merged with secureKey
            var symmetricSecurityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration.GetSection("AppSettings:secureKey").Value));
            var credential = new SigningCredentials(symmetricSecurityKey, SecurityAlgorithms.HmacSha256Signature); //puede ser también HmacSha512Signature
            var header = new JwtHeader(credential);
            //Data that we want to encode
            var payload = new JwtPayload(id.ToString(), null, claims, null, DateTime.UtcNow.AddHours(8)); //expira después de 8 horas laborales
            //Combinamos el header con el payload para crear el token
            var securityToken = new JwtSecurityToken(header, payload);

            return new JwtSecurityTokenHandler().WriteToken(securityToken);
            //Otra forma de crear el token
            //var securityToken = new JwtSecurityToken(
            //    claims: claims,
            //    expires: DateTime.UtcNow.AddHours(8),
            //    signingCredentials: credential
            //    );

        }
        public JwtSecurityToken Verify (string jwt)
        {
            try
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.UTF8.GetBytes(_configuration.GetSection("AppSettings:secureKey").Value);
                tokenHandler.ValidateToken(jwt, new TokenValidationParameters
                {
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuerSigningKey = true,
                    ValidateIssuer = false,
                    ValidateAudience = false
                }, out SecurityToken validatedToken);

                return (JwtSecurityToken)validatedToken;
            }
            catch(Exception ex)
            {
                Console.WriteLine($"[Empty logout {ex}]");
                return null;
            }
        }
    }
}

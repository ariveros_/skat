﻿using MailKit.Net.Smtp;
using MailKit.Security;
using MimeKit;

namespace SKATDesarrollo.Utilities
{
    public class EnvioMails : IEnvioMails
    {
        private readonly IConfiguration _configuration;

        public EnvioMails(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void Send(string to, string subject, string body)
        {
            //create Email
            var email = new MimeMessage();
            email.From.Add(MailboxAddress.Parse(_configuration.GetSection("AppSettings:emailFrom").Value));
            email.To.Add(MailboxAddress.Parse(to));
            email.Subject = subject;
            email.Body = new TextPart(MimeKit.Text.TextFormat.Html)
            {
                Text = body
            };

            //send Email
            using var smtp = new SmtpClient();

            // office 365
            smtp.Connect("smtp.office365.com", 587, SecureSocketOptions.StartTls);

            smtp.Authenticate(_configuration.GetSection("AppSettings:emailFrom").Value, _configuration.GetSection("AppSettings:passwordFrom").Value); //Necesario si el SMTP requiere usuario y contraseña
            smtp.Send(email);
            smtp.Disconnect(true);
        }
    }
}

﻿namespace SKATDesarrollo.Utilities
{
    public interface IEnvioMails
    {
        void Send(string to, string subject, string body);
    }
}

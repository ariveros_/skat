﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SKATDesarrollo.Helpers;

namespace SKATDesarrollo.Controllers.MIddleware
{
    public class JWTHeaderMiddleware
    {
        private readonly RequestDelegate _next;

        public JWTHeaderMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                var authenticationCookieName = "jwt";
                var cookie = context.Request.Cookies[authenticationCookieName];
                if (cookie != null)
                {
                    //var token = JObject.Parse(cookie.ToString());
                    context.Request.Headers.Append("Authorization", "Bearer " + cookie);
                }

                await _next.Invoke(context);
            }
            catch (Exception ex)
            {
                throw new Exception("Ha ocurrido algo en la validación del token.");
            }
        }
    }
}

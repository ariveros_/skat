﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using SKATDesarrollo.Repository;
using SKATDesarrollo.Models.ViewModel;

namespace SKATDesarrollo.Controllers.API.Estacion
{
    [Route("api/[controller]")]
    [Authorize(Roles = "Full Admin, Admin, Usuario")]
    [ApiController]
    public class ReporteEstacionController : ControllerBase
    {
        private readonly  IServicioReportes _servicioReportes;
        public ReporteEstacionController(IServicioReportes servicioReportes)
        {
            _servicioReportes = servicioReportes;
        }
        [HttpGet]
        public IActionResult Get()
        {
            return Ok(new { message = "Algo" });
        }
        [HttpPost("Prueba")]
        public IActionResult Prueba()
        {
            var prueba = _servicioReportes.pruebaCargaValores();

            return (prueba) ? Ok(new { message = "Prueba" }) : NotFound();
        }
        #region Graficos
        [HttpPost("CargaTrafos")]
        public IActionResult CargaTrafos(Object jsonRoot)
        {
            try
            {
                //parsing json to JObject
                var Root = JObject.Parse(jsonRoot.ToString());

                var idEstacion = (int?)Root.GetValue("idEstacion");
                var from = (DateTime?)Root.GetValue("startDate");
                var to = (DateTime?)Root.GetValue("endDate");

                List<dataGraficoVM> registros = null;
                if (idEstacion != null && from != null && to != null)
                {
                    registros = _servicioReportes.cargaTrafosV2((int)idEstacion, (DateTime)from, (DateTime)to);
                }
                if (registros == null || !registros.Any()) { return NotFound(new { message = "No se ha incontrado la información pedida. Es probable que no exista." }); }
                return Ok(registros);
            }
            catch (Exception ex)
            {
                return Problem(detail: "An exception has occurred. Exception details: " + ex.Message, statusCode: 500);
            }
        }
        [HttpPost("TensionBarra")]
        public IActionResult TensionBarra(Object jsonRoot)
        {
            try
            {
                //parsing json to JObject
                var Root = JObject.Parse(jsonRoot.ToString());

                var idEstacion = (int?)Root.GetValue("idEstacion");
                var from = (DateTime?)Root.GetValue("startDate");
                var to = (DateTime?)Root.GetValue("endDate");

                List<dataGraficoVM> registros = new List<dataGraficoVM>();
                if (idEstacion != null && from != null && to != null)
                {
                    registros = _servicioReportes.tensionBarrasV2((int)idEstacion, (DateTime)from, (DateTime)to);
                }
                if (registros == null || !registros.Any()) { return NotFound(new { message = "No se pudo cargar el grafico" }); }
                return Ok(registros);
            }
            catch (Exception ex)
            {
                return Problem(detail: "An exception has occurred. Exception details: " + ex.Message, statusCode: 500);
            }
        }
        [HttpPost("PorcentajeCargaTR")]
        public IActionResult PorcentajeCargaTR(Object jsonRoot)
        {
            try
            {
                //parsing json to JObject
                var Root = JObject.Parse(jsonRoot.ToString());

                var idEstacion = (int?)Root.GetValue("idEstacion");
                var from = (DateTime?)Root.GetValue("startDate");
                var to = (DateTime?)Root.GetValue("endDate");


                List<dataGraficoVM> registros = new List<dataGraficoVM>();
                if (idEstacion != null && from != null && to != null)
                {
                    registros = _servicioReportes.porcentajeCargaTR((int)idEstacion, (DateTime)from, (DateTime)to);
                }
                if (registros == null || !registros.Any()) { return NotFound(new { message = "No se ha incontrado la información pedida. Es probable que no exista." }); }
                return Ok(registros);
            }
            catch (Exception ex)
            {
                return Problem(detail: "An exception has occurred. Exception details: " + ex.Message, statusCode: 500);
            }
        }
        [HttpPost("CargaATR")]
        public IActionResult CargaATR(Object jsonRoot)
        {
            try
            {
                //parsing json to JObject
                var Root = JObject.Parse(jsonRoot.ToString());

                var idEstacion = (int?)Root.GetValue("idEstacion");
                var from = (DateTime?)Root.GetValue("startDate");
                var to = (DateTime?)Root.GetValue("endDate");


                List<dataGraficoVM> registros = null;
                if (idEstacion != null && from != null && to != null)
                {
                    registros = _servicioReportes.cargaATRV2((int)idEstacion, (DateTime)from, (DateTime)to);
                }
                if (registros == null || !registros.Any()) { return NotFound(new { message = "No se ha incontrado la información pedida. Es probable que no exista." }); }
                return Ok(registros);
            }
            catch (Exception ex)
            {
                return Problem(detail: "An exception has occurred. Exception details: " + ex.Message, statusCode: 500);
            }
        }
        [HttpPost("PorcentajeCargaATR")]
        public IActionResult PorcentajeCargaATR(Object jsonRoot)
        {
            try
            {
                //parsing json to JObject
                var Root = JObject.Parse(jsonRoot.ToString());

                var idEstacion = (int?)Root.GetValue("idEstacion");
                var from = (DateTime?)Root.GetValue("startDate");
                var to = (DateTime?)Root.GetValue("endDate");

                List<dataGraficoVM> registros = new List<dataGraficoVM>();
                if (idEstacion != null && from != null && to != null)
                {
                    registros = _servicioReportes.porcentajeCargaATR((int)idEstacion, (DateTime)from, (DateTime)to);
                }
                if (registros == null || !registros.Any()) { return NotFound(new { message = "No se ha incontrado la información pedida. Es probable que no exista." }); }
                return Ok(registros);
            }
            catch (Exception ex)
            {
                return Problem(detail: "An exception has occurred. Exception details: " + ex.Message, statusCode: 500);
            }
        }

        [HttpPost("DemandaTotal")]
        public IActionResult DemandaTotal(Object jsonRoot)
        {
            try
            {
                //parsing json to JObject
                var Root = JObject.Parse(jsonRoot.ToString());

                var idEstacion = (int?)Root.GetValue("idEstacion");
                var from = (DateTime?)Root.GetValue("startDate");
                var to = (DateTime?)Root.GetValue("endDate");


                dataGraficoVM registros = null;
                if (idEstacion != null && from != null && to != null)
                {
                    registros = _servicioReportes.demandaTotal((int)idEstacion, (DateTime)from, (DateTime)to);
                }
                if (registros == null) { NotFound(new { message = "No se ha incontrado la información pedida. Es probable que no exista." }); }
                return Ok(registros);
            }
            catch (Exception ex)
            {
                return Problem(detail: "An exception has occurred. Exception details: " + ex.Message, statusCode: 500);
            }
        }
        [HttpPost("DemandaApilable")]
        public IActionResult DemandaApilable(Object jsonRoot)
        {
            try
            {
                //parsing json to JObject
                var Root = JObject.Parse(jsonRoot.ToString());

                var from = (DateTime?)Root.GetValue("startDate");
                var to = (DateTime?)Root.GetValue("endDate");
                var idEstacionPrimera = (int?)Root.GetValue("idEstacionPrimera");
                var idEstacionSegunda = (int?)Root.GetValue("idEstacionSegunda");


                List<dataGraficoVM> registros = new List<dataGraficoVM>();
                if (idEstacionPrimera != null && from != null && to != null)
                {
                    registros = _servicioReportes.demandaApilable((int)idEstacionPrimera, idEstacionSegunda, (DateTime)from, (DateTime)to);
                }
                if (registros == null || !registros.Any()) { return NotFound(new { message = "No se ha incontrado la información pedida. Es probable que no exista." }); }
                return Ok(registros);
            }
            catch (Exception ex)
            {

                return Problem(detail: "An exception has occurred. Exception details: " + ex.Message, statusCode: 500);
            }
        }
        [HttpPost("TemperaturaAmbiente")]
        public IActionResult TemperaturaAmbiente(Object jsonRoot)
        {
            try
            {
                //parsing json to JObject
                var Root = JObject.Parse(jsonRoot.ToString());

                var idEstacion = (int?)Root.GetValue("idEstacion");
                var from = (DateTime?)Root.GetValue("startDate");
                var to = (DateTime?)Root.GetValue("endDate");


                dataGraficoVM registros = null;
                if (idEstacion != null && from != null && to != null)
                {
                    registros = _servicioReportes.temperaturaEstacion((int)idEstacion, (DateTime)from, (DateTime)to);
                }
                if (registros == null) { NotFound(new { message = "No se ha incontrado la información pedida. Es probable que no exista." }); }
                return Ok(registros);
            }
            catch (Exception ex)
            {
                return Problem(detail: "An exception has occurred. Exception details: " + ex.Message, statusCode: 500);
            }
        }
        
        #endregion
        #region Tablas
        [HttpPost("TablaCargaTrafos")]
        public IActionResult TablaCargaTrafos(Object jsonRoot)
        {
            try
            {
                //parsing json to JObject
                var Root = JObject.Parse(jsonRoot.ToString());

                var idEstacion = (int?)Root.GetValue("idEstacion");
                var from = (DateTime?)Root.GetValue("startDate");
                var to = (DateTime?)Root.GetValue("endDate");
                var tension = (string?)Root.GetValue("tension");


                dataTablaVM tabla = new dataTablaVM();
                if (idEstacion != null && from != null && to != null && tension != null)
                {
                    tabla = _servicioReportes.cargaTrafosTabla((int)idEstacion, (DateTime)from, (DateTime)to, (string)tension);
                }
                if (tabla == null) { return NotFound(new { message = "No se ha incontrado la información pedida. Es probable que no exista." }); }
                return Ok(tabla);
            }
            catch (Exception ex)
            {
                return Problem(detail: "An exception has occurred. Exception details: " + ex.Message, statusCode: 500);
            }
        }
        [HttpPost("TablaCargaMinMax")]
        public IActionResult TablaCargaMinMax(Object jsonRoot)
        {
            try
            {
                //parsing json to JObject
                var Root = JObject.Parse(jsonRoot.ToString());

                var idEstacion = (int?)Root.GetValue("idEstacion");
                var from = (DateTime?)Root.GetValue("startDate");
                var to = (DateTime?)Root.GetValue("endDate");


                dataTablaVM tabla = new dataTablaVM();
                if (idEstacion != null && from != null && to != null)
                {
                    tabla = _servicioReportes.maximosminimosTRsATRs((int)idEstacion, (DateTime)from, (DateTime)to);
                }
                if (tabla == null) { return NotFound(new { message = "No se ha incontrado la información pedida. Es probable que no exista." }); }
                return Ok(tabla);
            }
            catch (Exception ex)
            {
                return Problem(detail: "An exception has occurred. Exception details: " + ex.Message, statusCode: 500);
            }
        }
        [HttpPost("TablaPorcentajeTrafos")]
        public IActionResult TablaPorcentajeTrafos(Object jsonRoot)
        {
            try
            {
                //parsing json to JObject
                var Root = JObject.Parse(jsonRoot.ToString());

                var idEstacion = (int?)Root.GetValue("idEstacion");
                var from = (DateTime?)Root.GetValue("startDate");
                var to = (DateTime?)Root.GetValue("endDate");
                var tension = (string?)Root.GetValue("tension");


                dataTablaVM tabla = new dataTablaVM();
                if (idEstacion != null && from != null && to != null && tension != null)
                {
                    tabla = _servicioReportes.porcentajeCargaTrafosTabla((int)idEstacion, (DateTime)from, (DateTime)to, (string)tension);
                }
                if (tabla == null) { return NotFound(new { message = "No se ha incontrado la información pedida. Es probable que no exista." }); }
                return Ok(tabla);
            }
            catch (Exception ex)
            {
                return Problem(detail: "An exception has occurred. Exception details: " + ex.Message, statusCode: 500);
            }
        }
        [HttpPost("TablaCargaATR")]
        public IActionResult TablaCargaATR(Object jsonRoot)
        {
            try
            {
                //parsing json to JObject
                var Root = JObject.Parse(jsonRoot.ToString());

                var idEstacion = (int?)Root.GetValue("idEstacion");
                var from = (DateTime?)Root.GetValue("startDate");
                var to = (DateTime?)Root.GetValue("endDate");
                var tension = (string?)Root.GetValue("tension");


                dataTablaVM tabla = new dataTablaVM();
                if (idEstacion != null && from != null && to != null && tension != null)
                {
                    tabla = _servicioReportes.cargaATRsTabla((int)idEstacion, (DateTime)from, (DateTime)to, (string)tension);
                }
                if (tabla == null) { return NotFound(new { message = "No se ha incontrado la información pedida. Es probable que no exista." }); }
                return Ok(tabla);
            }
            catch (Exception ex)
            {

                return Problem(detail: "An exception has occurred. Exception details: " + ex.Message, statusCode: 500);
            }
        }
        [HttpPost("TablaPorcentajeATR")]
        public IActionResult TablaPorcentajeATR(Object jsonRoot)
        {
            try
            {
                //parsing json to JObject
                var Root = JObject.Parse(jsonRoot.ToString());

                var idEstacion = (int?)Root.GetValue("idEstacion");
                var from = (DateTime?)Root.GetValue("startDate");
                var to = (DateTime?)Root.GetValue("endDate");
                var tension = (string?)Root.GetValue("tension");


                dataTablaVM tabla = new dataTablaVM();
                if (idEstacion != null && from != null && to != null && tension != null)
                {
                    tabla = _servicioReportes.porcentajeCargaATRsTabla((int)idEstacion, (DateTime)from, (DateTime)to, (string)tension);
                }
                if (tabla == null) { return NotFound(new { message = "No se ha incontrado la información pedida. Es probable que no exista." }); }
                return Ok(tabla);
            }
            catch (Exception ex)
            {
                return Problem(detail: "An exception has occurred. Exception details: " + ex.Message, statusCode: 500);
            }
        }
        
        [HttpPost("TablaDemanda")]
        public IActionResult TablaDemanda(Object jsonRoot)
        {
            try
            {
                //parsing json to JObject
                var Root = JObject.Parse(jsonRoot.ToString());

                var idEstacion = (int?)Root.GetValue("idEstacion");
                var from = (DateTime?)Root.GetValue("startDate");
                var to = (DateTime?)Root.GetValue("endDate");


                dataTablaVM tabla = new dataTablaVM();
                if (idEstacion != null && from != null && to != null)
                {
                    tabla = _servicioReportes.demandaTotalTabla((int)idEstacion, (DateTime)from, (DateTime)to);
                }
                if (tabla == null) { return NotFound(new { message = "No se ha incontrado la información pedida. Es probable que no exista." }); }
                return Ok(tabla);
            }
            catch (Exception ex)
            {
                return Problem(detail: "An exception has occurred. Exception details: " + ex.Message, statusCode: 500);
            }
        }
        [HttpPost("TablaTensionBarra")]
        public IActionResult TablaTensionBarra(Object jsonRoot)
        {
            try
            {
                //parsing json to JObject
                var Root = JObject.Parse(jsonRoot.ToString());

                var idEstacion = (int?)Root.GetValue("idEstacion");
                var from = (DateTime?)Root.GetValue("startDate");
                var to = (DateTime?)Root.GetValue("endDate");
                var tension = (string?)Root.GetValue("tension");


                dataTablaVM tabla = new dataTablaVM();
                if (idEstacion != null && from != null && to != null && tension != null)
                {
                    tabla = _servicioReportes.tensionBarrasTabla((int)idEstacion, (DateTime)from, (DateTime)to, (string)tension);
                }
                if (tabla == null) { return NotFound(new { message = "No se ha incontrado la información pedida. Es probable que no exista." }); }
                return Ok(tabla);
            }
            catch ( Exception ex)
            {

                return Problem(detail: "An exception has occurred. Exception details: " + ex.Message, statusCode: 500);
            }
        }
        [HttpPost("TablaTemperaturaAmbiente")]
        public IActionResult TablaTemperaturaAmbiente(Object jsonRoot)
        {
            try
            {
                //parsing json to JObject
                var Root = JObject.Parse(jsonRoot.ToString());

                var idEstacion = (int?)Root.GetValue("idEstacion");
                var from = (DateTime?)Root.GetValue("startDate");
                var to = (DateTime?)Root.GetValue("endDate");


                dataTablaVM tabla = new dataTablaVM();
                if (idEstacion != null && from != null && to != null)
                {
                    tabla = _servicioReportes.temperaturaAmbienteTabla((int)idEstacion, (DateTime)from, (DateTime)to);
                }
                if (tabla == null) { return NotFound(); }
                return Ok(tabla);
            }
            catch (Exception ex)
            {

                return Problem(detail: "An exception has occurred. Exception details: " + ex.Message, statusCode: 500);
            }
        }
        [HttpPost("TablaLineasBarrasPQ")]
        public IActionResult TablaLineasBarrasPQ(Object jsonRoot)
        {
            try
            {
                //parsing json to JObject
                var Root = JObject.Parse(jsonRoot.ToString());

                var idEstacion = (int?)Root.GetValue("idEstacion");
                var from = (DateTime?)Root.GetValue("startDate");
                var to = (DateTime?)Root.GetValue("endDate");


                List<dataTablaVM>? tabla = new List<dataTablaVM>();
                if (idEstacion != null && from != null && to != null)
                {
                    tabla = _servicioReportes.potenciasPQbarrasLineas((int)idEstacion, (DateTime)from, (DateTime)to);
                }
                if (tabla == null || !tabla.Any()) { return NotFound(new {message = "No se ha incontrado la información pedida. Es probable que no exista." });}
                return Ok(tabla);
            }
            catch(Exception ex)
            {
                return Problem(detail: "An exception has occurred. Exception details: "+ex.Message,statusCode: 500);
            }
        }
        [HttpPost("TablaTotalesPQLineas")]
        public IActionResult TablaTotalesPQLineas(Object jsonRoot)
        {
            try
            {
                //parsing json to JObject
                var Root = JObject.Parse(jsonRoot.ToString());

                var idEstacion = (int?)Root.GetValue("idEstacion");
                var from = (DateTime?)Root.GetValue("startDate");
                var to = (DateTime?)Root.GetValue("endDate");


                List<dataTablaVM>? tabla = new List<dataTablaVM>();
                if (idEstacion != null && from != null && to != null)
                {
                    tabla = _servicioReportes.totalesPQbarrsaLineas((int)idEstacion, (DateTime)from, (DateTime)to);
                }
                if (tabla == null || !tabla.Any()) { return NotFound(new { message = "No se ha incontrado la información pedida. Es probable que no exista." }); }
                return Ok(tabla);
            }
            catch(Exception ex)
            {
                return Problem(detail: "An exception has occurred. Exception details: " + ex.Message, statusCode: 500);
            }
        }
        [HttpPost("TablaCorrienteLineas")]
        public IActionResult TablaCorrienteLineas(Object jsonRoot)
        {
            try
            {
                //parsing json to JObject
                var Root = JObject.Parse(jsonRoot.ToString());

                var idEstacion = (int?)Root.GetValue("idEstacion");
                var from = (DateTime?)Root.GetValue("startDate");
                var to = (DateTime?)Root.GetValue("endDate");


                List<dataTablaVM>? tabla = new List<dataTablaVM>();
                if (idEstacion != null && from != null && to != null)
                {
                    tabla = _servicioReportes.corrienteBarrasLineas((int)idEstacion, (DateTime)from, (DateTime)to);
                }
                if (tabla == null || !tabla.Any()) { return NotFound(new { message = "No se ha incontrado la información pedida. Es probable que no exista." }); }
                return Ok(tabla);
            }
            catch (Exception ex)
            {
                return Problem(detail: "An exception has occurred. Exception details: " + ex.Message, statusCode: 500);
            }
        }
        [HttpPost("TablaTotalCorrienteLineas")]
        public IActionResult TablaTotalCorrienteLineas(Object jsonRoot)
        {
            try
            {
                //parsing json to JObject
                var Root = JObject.Parse(jsonRoot.ToString());

                var idEstacion = (int?)Root.GetValue("idEstacion");
                var from = (DateTime?)Root.GetValue("startDate");
                var to = (DateTime?)Root.GetValue("endDate");


                List<dataTablaVM>? tabla = new List<dataTablaVM>();
                if (idEstacion != null && from != null && to != null)
                {
                    tabla = _servicioReportes.totalesCorrienteBarrasLineas((int)idEstacion, (DateTime)from, (DateTime)to);
                }
                if (tabla == null || !tabla.Any()) { return NotFound(new { message = "No se ha incontrado la información pedida. Es probable que no exista." }); }
                return Ok(tabla);
            }
            catch (Exception ex)
            {
                return Problem(detail: "An exception has occurred. Exception details: " + ex.Message, statusCode: 500);
            }
        }

        [HttpPost("TablaAmperesLineas")]
        public IActionResult TablaAmperesLineaCDP(Object jsonRoot)
        {
            try
            {
                //parsing json to JObject
                var Root = JObject.Parse(jsonRoot.ToString());

                var idEstacion = (int?)Root.GetValue("idEstacion");
                var from = (DateTime?)Root.GetValue("startDate");
                var to = (DateTime?)Root.GetValue("endDate");
                var nombreEstacion = (string?)Root.GetValue("nombreEstacion");


                List<dataTablaVM>? tabla = new List<dataTablaVM>();
                if (idEstacion != null && from != null && to != null && nombreEstacion != null)
                {
                    switch (nombreEstacion)
                    {
                        case "E.T. Cruz de Piedra": 
                            tabla = _servicioReportes.amperesLineasCDP((int)idEstacion, (DateTime)from, (DateTime)to);
                            break;

                        case "E.T. Anchoris":
                            tabla = _servicioReportes.amperesLineasAnchoris((int)idEstacion, (DateTime)from, (DateTime)to);
                            break;
                    }
                    
                }
                if (tabla == null || !tabla.Any()) { return NotFound(new { message = "No se ha incontrado la información pedida. Es probable que no exista." }); }
                return Ok(tabla);
            }
            catch (Exception ex)
            {
                return Problem(detail: "An exception has occurred. Exception details: " + ex.Message, statusCode: 500);
            }
        }
        [HttpPost("TablaCosenoPhi")]
        public IActionResult TablaCosenoPhi(Object jsonRoot)
        {
            try
            {
                //parsing json to JObject
                var Root = JObject.Parse(jsonRoot.ToString());

                var idEstacion = (int?)Root.GetValue("idEstacion");
                var from = (DateTime?)Root.GetValue("startDate");
                var to = (DateTime?)Root.GetValue("endDate");
                var is5Min = (Boolean?)Root.GetValue("is5Min");


                List<dataTablaVM>? tabla = new List<dataTablaVM>();
                if (idEstacion != null && from != null && to != null)
                {
                    tabla = _servicioReportes.cosenoPhiLineas66ATR13((int)idEstacion, (DateTime)from, (DateTime)to,is5Min);
                }
                if (tabla == null || !tabla.Any()) { return NotFound(new { message = "No se ha incontrado la información pedida. Es probable que no exista." }); }
                return Ok(tabla);
            }
            catch (Exception ex)
            {
                return Problem(detail: "An exception has occurred. Exception details: " + ex.Message, statusCode: 500);
            }
        }

        #endregion

    }
}

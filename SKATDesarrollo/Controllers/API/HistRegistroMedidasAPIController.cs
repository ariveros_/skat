﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SKATDesarrollo.Context;
using SKATDesarrollo.Models;

namespace SKATDesarrollo.Controllers.API
{
    [Route("api/[controller]")]
    [ApiController]
    public class HistRegistroMedidasAPIController : ControllerBase
    {
        private readonly SegSkaContext _context;

        public HistRegistroMedidasAPIController(SegSkaContext context)
        {
            _context = context;
        }

        // GET: api/HistRegistroMedidasAPI
        [HttpGet]
        public async Task<ActionResult<IEnumerable<HistRegistroMedida>>> GetHistRegistroMedida()
        {
          if (_context.HistRegistroMedida == null)
          {
              return NotFound();
          }
            return await _context.HistRegistroMedida.ToListAsync();
        }

        // GET: api/HistRegistroMedidasAPI/5
        [HttpGet("{id}")]
        public async Task<ActionResult<HistRegistroMedida>> GetHistRegistroMedida(int id)
        {
          if (_context.HistRegistroMedida == null)
          {
              return NotFound();
          }
            var histRegistroMedida = await _context.HistRegistroMedida.FindAsync(id);

            if (histRegistroMedida == null)
            {
                return NotFound();
            }

            return histRegistroMedida;
        }

        // PUT: api/HistRegistroMedidasAPI/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutHistRegistroMedida(int id, HistRegistroMedida histRegistroMedida)
        {
            if (id != histRegistroMedida.idRegistroMedida)
            {
                return BadRequest();
            }

            _context.Entry(histRegistroMedida).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!HistRegistroMedidaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/HistRegistroMedidasAPI
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<HistRegistroMedida>> PostHistRegistroMedida(HistRegistroMedida histRegistroMedida)
        {
          if (_context.HistRegistroMedida == null)
          {
              return Problem("Entity set 'SegSkaContext.HistRegistroMedida'  is null.");
          }
            _context.HistRegistroMedida.Add(histRegistroMedida);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetHistRegistroMedida", new { id = histRegistroMedida.idRegistroMedida }, histRegistroMedida);
        }

        // DELETE: api/HistRegistroMedidasAPI/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteHistRegistroMedida(int id)
        {
            if (_context.HistRegistroMedida == null)
            {
                return NotFound();
            }
            var histRegistroMedida = await _context.HistRegistroMedida.FindAsync(id);
            if (histRegistroMedida == null)
            {
                return NotFound();
            }

            _context.HistRegistroMedida.Remove(histRegistroMedida);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool HistRegistroMedidaExists(int id)
        {
            return (_context.HistRegistroMedida?.Any(e => e.idRegistroMedida == id)).GetValueOrDefault();
        }
    }
}

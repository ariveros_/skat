﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using SKATDesarrollo.Context;
using SKATDesarrollo.Models;
using SKATDesarrollo.Repository;
using SKATDesarrollo.Models.ViewModel;

namespace SKATDesarrollo.Controllers.API
{
    [Route("api/[controller]")]
    [ApiController]
    public class HistEstacionesAPIController : ControllerBase
    {
        private readonly IServicioEstacion _servicioEstacion;

        public HistEstacionesAPIController(IServicioEstacion servicioEstacion)
        {
            _servicioEstacion = servicioEstacion;
        }

        // GET: api/HistEstacionesAPI
        [HttpGet]
        public ActionResult<List<HistEstacion>> GetStations()
        {
            return _servicioEstacion.AllStations();
        }

        //GET Estaciones activas
        [HttpGet("Estaciones")]
        public ActionResult<List<HistEstacion>> GetEstaciones()
        {
            return _servicioEstacion.AllActiveStations();
        }

        #region ABM
        [HttpGet("ObtenerEstaciones")]
        public ActionResult<Object> ObtenerEstaciones(bool activo)
        {
            object lista = _servicioEstacion.ObtenerEstaciones(activo);
            return lista;
        }
        [HttpGet("ConsultarEstacion")]
        public ActionResult ConsultarEstacion (int id)
        {
            HistEstacion estacion = _servicioEstacion.ObtenerEstacion(id);
            if (estacion != null) return Ok(estacion);
            else return NotFound(new { message = "Not found" });
        }
        //Alta
        [HttpPost("AltaEstacion")]
        public ActionResult AltaEstacion(HistEstacion estacion)
        {
            int codigo;
            codigo = _servicioEstacion.AltaEstacion(estacion);
            if (codigo == 200)
            {
                return Ok(new { message = "Success" });
            }
            else if (codigo == 422) return BadRequest();
            else return UnprocessableEntity();
        }
        //Baja/Recuperar
        [HttpPut("BajaRecuperarEstacion")]
        public ActionResult BajaRecuperarEstacion(int id)
        {
            int codigo;
            codigo = _servicioEstacion.BajaRecuperarEstacion(id);
            if (codigo == 200) { return Ok(new { message = "success" }); }
            else if (codigo == 404) { return NotFound(); }
            else return BadRequest();
        }
        //Modificar
        [HttpPut("ModificarEstacion")]
        public ActionResult ModificarEstacion(Object jsonRoot)
        {
            //parsing json to JObject
            var Root = JObject.Parse(jsonRoot.ToString());
            //id
            var idEstacion = (int)Root.GetValue("idEstacion");
            //nombre estacion
            var nombreE = (string)Root.GetValue("nombreEstacion");
            //zona
            var zona = (int)Root.GetValue("zonaEstacion");

            int codigo;
            codigo = _servicioEstacion.ModificarEstacion(idEstacion, nombreE, zona);
            if (codigo == 200) return Ok(codigo);
            else
            {
                if (codigo == 400) return BadRequest(new { message = "Error" });
                else return NotFound(new { message = "Error" });
            }
        }
        #endregion

    }
}

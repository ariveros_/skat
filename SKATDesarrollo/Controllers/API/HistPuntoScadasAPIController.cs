﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SKATDesarrollo.Context;
using SKATDesarrollo.Models;

namespace SKATDesarrollo.Controllers.API
{
    [Route("api/[controller]")]
    [ApiController]
    public class HistPuntoScadasAPIController : ControllerBase
    {
        private readonly SegSkaContext _context;

        public HistPuntoScadasAPIController(SegSkaContext context)
        {
            _context = context;
        }

        // GET: api/HistPuntoScadasAPI
        [HttpGet]
        public async Task<ActionResult<IEnumerable<HistPuntoScada>>> GetHistPuntoScada()
        {
          if (_context.HistPuntoScada == null)
          {
              return NotFound();
          }
            return await _context.HistPuntoScada.ToListAsync();
        }

        // GET: api/HistPuntoScadasAPI/5
        [HttpGet("{id}")]
        public async Task<ActionResult<HistPuntoScada>> GetHistPuntoScada(int id)
        {
          if (_context.HistPuntoScada == null)
          {
              return NotFound();
          }
            var histPuntoScada = await _context.HistPuntoScada.FindAsync(id);

            if (histPuntoScada == null)
            {
                return NotFound();
            }

            return histPuntoScada;
        }

        // PUT: api/HistPuntoScadasAPI/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutHistPuntoScada(int id, HistPuntoScada histPuntoScada)
        {
            if (id != histPuntoScada.idPuntoScada)
            {
                return BadRequest();
            }

            _context.Entry(histPuntoScada).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!HistPuntoScadaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/HistPuntoScadasAPI
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<HistPuntoScada>> PostHistPuntoScada(HistPuntoScada histPuntoScada)
        {
          if (_context.HistPuntoScada == null)
          {
              return Problem("Entity set 'SegSkaContext.HistPuntoScada'  is null.");
          }
            _context.HistPuntoScada.Add(histPuntoScada);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetHistPuntoScada", new { id = histPuntoScada.idPuntoScada }, histPuntoScada);
        }

        // DELETE: api/HistPuntoScadasAPI/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteHistPuntoScada(int id)
        {
            if (_context.HistPuntoScada == null)
            {
                return NotFound();
            }
            var histPuntoScada = await _context.HistPuntoScada.FindAsync(id);
            if (histPuntoScada == null)
            {
                return NotFound();
            }

            _context.HistPuntoScada.Remove(histPuntoScada);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool HistPuntoScadaExists(int id)
        {
            return (_context.HistPuntoScada?.Any(e => e.idPuntoScada == id)).GetValueOrDefault();
        }
    }
}

﻿#nullable disable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SKATDesarrollo.Context;
using SKATDesarrollo.Models;
using SKATDesarrollo.Models.ViewModel;
using SKATDesarrollo.Repository;
using System.Text.Json;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Authorization;

namespace SKATDesarrollo.Controllers.API
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Full Admin, Admin")]
    public class RolsAPIController : ControllerBase
    {
        
        private readonly IServicioRol _servicio;
 
        public RolsAPIController(IServicioRol servicio)
        {
            _servicio = servicio;
        }
       // GET: api/RolsAPI
        [HttpGet]
        public ActionResult<List<SegRol>> GetSegRols()
        {
            return _servicio.AllRoles();
        }
        //Get Roles activos
        [HttpGet("Roles")]
        public ActionResult<List<SegRol>> GetRoles()
        {
            return _servicio.AllRolesActivos();
        }
        #region Rol Permisos
        //OBTENER Rol
        [HttpGet("ObtenerRol")]
        public IActionResult ObtenerRol(bool activo)
        {
            object lista = _servicio.ObtenerRoles(activo);
            return Ok(lista);
        }

        //CONSULTAR Rol
        [HttpGet("ConsultarRol")]
        public IActionResult ConsultarRol (int rolID)
        {
            RolVM rol = _servicio.ConsultarRol(rolID);
            if (rol != null) return Ok(rol);
            else return NotFound(new { message = "No se encontro el rol" });
        }

        //ALTA Rol
        [HttpPost("AltaRol")]
        public IActionResult AltaRol(RolVM rol)
        {
            int codigo;
            codigo = _servicio.AltaRol(rol);
            if(codigo == 200) {
                return Ok(new
                {
                    message = "success"
                });
            }
            else { if (codigo == 400) return BadRequest(new { message = "Error" });
                else return UnprocessableEntity(new { message = "Este rol ya existe" }); //422
            }
            
        }
        //BAJA Rol
        [HttpPut("BajaRol")]
        public IActionResult BajaRol (Object jsonRoot)
        {
            //parsing json to JObject
            var Root = JObject.Parse(jsonRoot.ToString());
            //get idRol from JObject
            int? roleID = (int?)Root.GetValue("roleID");

            int codigo = 0;
            if (roleID != null)
            {
                codigo = _servicio.BajaRecuperarRol((int)roleID);
            }
            if (codigo == 200) { return Ok(new {message = "success"}); }
            else { return NotFound(new { message = "No se encontro el rol" }); }
        }

        //MODIFICAR Rol
        [HttpPut("ModificarRol")]
        public IActionResult ModificarRol(Object jsonRoot)
        {
            //parsing json to JObject
            var Root = JObject.Parse(jsonRoot.ToString());
            //string
            var nombreRol = (string)Root.GetValue("nuevoNombreRol");
            //obteniendo el id del rol a modificar
            var idRol = (int)Root["data"]["roleID"];


            int codigo = _servicio.ModificarRol(idRol, nombreRol);
            if (codigo == 200) return Ok(codigo);
            else
            {
                if (codigo == 400) return BadRequest(new { message = "Error" });
                else return NotFound(new { message = "Error" });
            }
        }
        //Agregar menu al rol
        [HttpPut("AgregarMenu")]
        public IActionResult AgregarMenu(Object jsonRoot)
        {
            //parsing json to JObject
            var Root = JObject.Parse(jsonRoot.ToString());
            //get idRol from JObject
            int idRol = (int)Root.GetValue("idRol");
            //get array "permisos"
            var menusArray = Root.GetValue("menus").ToArray();
            List<MenuVM> menus = new List<MenuVM>();
            
            for (int i = 0; i < menusArray.Length; i++)
            {
                MenuVM menu = new MenuVM();
                menu.idMenu = (int)Root["menus"][i];
                //permisoVM.permisoMenuNombre = (string)Root["permisos"][i]["permisoMenuNombre"];
                //permisoVM.permisoMenuActivo = (bool)Root["permisos"][i]["permisoMenuActivo"];
                
                menus.Add(menu);
            }

            int codigo = _servicio.agregarMenu(idRol, menus);
            if (codigo == 200) return Ok(codigo);
            else
            {
                if (codigo == 404) return BadRequest(new { message = "Error" });
                else return NotFound(new { message = "No se modifico el rol" });
            }

        }
        //Eliminar menu del rol
        [HttpPut("EliminarMenu")]
        public IActionResult EliminarMenu(Object jsonRoot)
        {
            //parsing json to JObject
            var Root = JObject.Parse(jsonRoot.ToString());
            //get idRol from JObject
            int idRol = (int)Root.GetValue("idRol");
            //get permiso from JObject
            int idMenu = (int)Root.GetValue("idMenu");

            int codigo = _servicio.eliminarMenu(idRol, idMenu);
            if (codigo == 200) return Ok(codigo);
            else
            {
                if (codigo == 404) return BadRequest(new { message = "Error" });
                else return NotFound(new { message = "No se modifico el rol" });
            }

        }
        

        [HttpPut("RecuperarRol")]
        public IActionResult RecuperarRol (Object jsonRoot)
        {
            //parsing json to JObject
            var Root = JObject.Parse(jsonRoot.ToString());
            //get idRol from JObject
            int? roleID = (int?)Root.GetValue("roleID");
            int codigo = 0;
            if(roleID != null)
            {
                codigo = _servicio.BajaRecuperarRol((int)roleID);
            }
            if (codigo == 200) { return Ok(new {message = "success"}); }
            else { return NotFound(new { message = "No se pudo recuperar el rol" }); }
        }
        #endregion
        #region UsuariosRoles
        //USUARIOS ROLES
        [HttpGet("ObtenerPersonasRol")]
        public ActionResult<Object> ObtenerPersonasRol(bool activo)
        {
            var lista = new List<UsuarioRolVM>();
            lista = _servicio.ObtenerPersonasRoles(activo);
            var json = JsonSerializer.Serialize(lista);
            return json;
        }
        //CONSULTA ROL USUARIO
        [HttpGet("ConsultarRoles")]
        public ActionResult ConsultarRoles(int id)
        {
            UsuarioRolVM rol = _servicio.ConsultaUnRol(id);
            if (rol != null) return Ok(rol);
            else return NotFound(new { message = "No se encontro el rol" });
        }

        //ALTA ROL USUARIO
        [HttpPost("AltaRoles")]
        public ActionResult AltaRoles(Object jsonRoot)
        {
            //parsing json to JObject
            var Root = JObject.Parse(jsonRoot.ToString());
            //get idRol from JObject
            int idRol = (int)Root.GetValue("idRol");
            //get permiso from JObject
            int idUser = (int)Root.GetValue("idUsuario");

            int codigo;
            codigo = _servicio.AltaRolUsuario(idRol, idUser);
            if (codigo == 200)
            {
                return Ok(new
                {
                    message = "success"
                });
            }
            else
            {
                if (codigo == 404) return BadRequest(new { message = "Error" });
                else return UnprocessableEntity(new { message = "Este rol ya existe" }); //422
            }
        }
        
        //MODIFICACIONES
        [HttpPut("ModificarRoles")]
        public ActionResult ModificarRoles(Object jsonRoot)
        {
            //parsing json to JObject
            var Root = JObject.Parse(jsonRoot.ToString());
            //idRol nuevo
            int idRol = (int)Root.GetValue("idNuevoRol");
            //id rol user
            int idUserRol = (int)Root.GetValue("idUsuario");

            int codigo = _servicio.ModificarRolUsuario(idRol, idUserRol);
            if (codigo == 200) return Ok(codigo);
            else return NotFound(new { message = "No se modifico el rol" });
        }

        //BAJA
        [HttpPut("BajaRoles")]
        public ActionResult BajaRoles(Object jsonRoot)
        {
            //parsing json to JObject
            var Root = JObject.Parse(jsonRoot.ToString());
            //get idRol from JObject
            int? idUserRol = (int?)Root.GetValue("IdUserRol");

            int codigo = 0;
            if (idUserRol != null)
            {
                codigo = _servicio.BajaRecuperarRolUsuario((int)idUserRol);
            }
            if (codigo == 200) { return Ok(new { message = "success" }); }
            else { return NotFound(new { message = "No se encontro el rol" }); }
        }
        
        //RECUPERAR
        [HttpPut("RecuperarRoles")]
        public ActionResult RecuperarRoles(Object jsonRoot)
        {
            //parsing json to JObject
            var Root = JObject.Parse(jsonRoot.ToString());
            //get idRol from JObject
            int? idUserRol = (int?)Root.GetValue("IdUserRol");

            int codigo = 0;
            if (idUserRol != null)
            {
                codigo = _servicio.BajaRecuperarRolUsuario((int)idUserRol);
            }
            if (codigo == 200) { return Ok(new { message = "success" }); }
            else { return NotFound(new { message = "No se encontro el rol" }); }
        }
        #endregion
    }
}

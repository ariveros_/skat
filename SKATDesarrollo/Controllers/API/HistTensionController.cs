﻿using Microsoft.AspNetCore.Mvc;
using SKATDesarrollo.Repository;
using SKATDesarrollo.Models.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json.Linq;

namespace SKATDesarrollo.Controllers.API
{
    [Route("api/[controller]")]
    [AllowAnonymous]
    [ApiController]
    public class HistTensionController : Controller
    {
        private readonly IServicioTension _servicioTension;
        public HistTensionController(IServicioTension servicioTension)
        {
            _servicioTension = servicioTension;
        }
        [HttpGet]
        public IActionResult Get()
        {
            var tensionesJson = _servicioTension.consultaTensiones();
            return (tensionesJson != null) ? Ok(tensionesJson) : NotFound();
        }
    }
}

﻿#nullable disable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SKATDesarrollo.Models;
using SKATDesarrollo.Models.ViewModel;
using SKATDesarrollo.Helpers;
using SKATDesarrollo.Context;
using SKATDesarrollo.Repository;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace SKATDesarrollo.Controllers.API
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Full Admin, Admin")]
    public class UsersAPIController : ControllerBase
    {
        private readonly IUserRepository _servUsuario;

        public UsersAPIController(IUserRepository servUsuario)
        {
            _servUsuario = servUsuario;
        }

        //GET: api/UsersAPI
       [HttpGet]
        public ActionResult<List<SegUser>> GetSegUsers()
        {
            return _servUsuario.GetSegUsers();
        }

        //Obtener Usuarios
        [HttpGet("ObtenerUsuarios")]
        public ActionResult<Object> ObtenerUsuarios(bool activo)
        {
            object lista = _servUsuario.ObtenerUsuarios(activo);
            return lista;
        }
        //Consultar user
        [HttpGet("ConsultarUser")]
        public ActionResult ConsultarUser(int id)
        {
            UsuarioVM usuario = _servUsuario.ConsultarUser(id);
            if (usuario != null) return Ok(usuario);
            else return NotFound(new { message = "No se encontro el rol" });
        }

        //Alta User
        [HttpPost("AltaUsuario")]
        public ActionResult AltaUsuario(UsuarioVM usuario) //necesitaremos json?
        {
            int codigo;
            codigo = _servUsuario.AltaUser(usuario);
            
            if (codigo == 200) return Ok(new {message = "success"});
            else
            {
                if (codigo == 400) return BadRequest(new { message = "Error" });
                else return UnprocessableEntity(new { message = "Este user ya existe" }); //422
            }
            //Console.WriteLine("algo");
            //return Ok();

        }
        //Modificar user
        [HttpPost("ModificarUser")]
        public ActionResult ModificarUser(UsuarioVM usuario)
        {

            int codigo = _servUsuario.ModificarUser(usuario);
            if (codigo == 200) return Ok(codigo);
            else
            {
                if (codigo == 404) return BadRequest(new { message = "Error" });
                else return NotFound(new { message = "No se modifico el usuario" });
            }
        }
       

        //Baja User
        [HttpPut("BajaUser")]
        public ActionResult BajaUser(UsuarioVM usuario)
        {
            int codigo = _servUsuario.BajaRecuperarUsuario(usuario);
            if (codigo == 200) { return Ok(new { message = "success" }); }
            else { return NotFound(new { message = "No se encontro el rol" }); }
        }
        //Recuperar User
        [HttpPut("RecuperarUser")]
        public ActionResult RecuperarUser(UsuarioVM usuario)
        {
            int codigo = _servUsuario.BajaRecuperarUsuario(usuario);
            if (codigo == 200) { return Ok(new { message = "success" }); }
            else { return NotFound(new { message = "No se pudo recuperar el rol" }); }
        }

        //Cambiar contraseña
        [Authorize(Roles = "Full Admin, Admin, Usuario")]
        [HttpPost("CambiarContraseña")]
        public ActionResult CambiarContraseña(Object jsonRoot)
        {
            //parsing json to JObject
            var Root = JObject.Parse(jsonRoot.ToString());
            //get id from JObject
            int idUser = (int)Root.GetValue("idUser");
            //get pass from JObject
            var newPass = (string)Root.GetValue("newPass");

            int codigo = _servUsuario.CambioContraseña(idUser, newPass);
            if (codigo == 200) return Ok(codigo);
            else
            {
                if (codigo == 404) return NotFound(new { message = "Error" });
                else return BadRequest(new { message = "Contraseña repetida" });
            }
        }
        //Recuperar Contraseña
        [AllowAnonymous]
        [HttpPost("RecuperarContraseña")]
        public ActionResult RecuperarContraseña(Object jsonRoot)
        {
            //parsing json to JObject
            var Root = JObject.Parse(jsonRoot.ToString());
            //get token from JObject
            string token = (string)Root.GetValue("token");
            //get pass from JObject
            var password = (string)Root.GetValue("password");

            int codigo = _servUsuario.ResetPassword(token,password);
            if (codigo == 200) return Ok(codigo);
            else if (codigo == 404) return NotFound(new { message = "No se encontró el usuario" });
            else if (codigo == 422) return UnprocessableEntity(new { message = "Contraseña repetida" });
            else return BadRequest();
           
        }

    }
}

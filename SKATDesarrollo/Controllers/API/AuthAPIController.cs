﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using SKATDesarrollo.Helpers;
using SKATDesarrollo.Models;
using SKATDesarrollo.Models.ViewModel;
using SKATDesarrollo.Repository;

namespace SKATDesarrollo.Controllers.API
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthAPIController : ControllerBase
    {
        private readonly IUserRepository _repository;
        private readonly IJWTService _jwtService;
        private readonly IServicioLog _servicioLog;
        

        public AuthAPIController(IUserRepository repository, IJWTService jwtService, IServicioLog servicioLog)
        {
            _repository = repository;
            _jwtService = jwtService;
            _servicioLog = servicioLog;

        }

        [HttpGet]
        public ActionResult Get()
        {
            return Ok(new {message = "Servidor conectado"});
        }

        [HttpPost("login")]
        public IActionResult Login(UsuarioVM usuario)
        {
            try
            {
                //Buscamos usuario
                var usuarioEncontrado = _repository.GetLogin(usuario.email, usuario.password);
                if (usuarioEncontrado == null) return BadRequest(new { message = "Credenciales incorrectas" }); //usuario y contraseña mal
                if (usuarioEncontrado.email == null) return BadRequest(new { message = "Credenciales incorrectas" }); //usuario mal
                                                                                                                      //if (usuarioEncontrado.userName != null) { if (usuarioEncontrado.password == null) return BadRequest(new { message = "Contraseña incorrecta" }); }
                                                                                                                      //Verificamos si está correcto 
                if (!BCrypt.Net.BCrypt.Verify(usuario.password, usuarioEncontrado.password))
                {
                    return BadRequest(new { message = "Credenciales incorrectas" });
                }

                var jwt = _jwtService.Generate(usuarioEncontrado.userID);
                Response.Cookies.Append("jwt", jwt, new CookieOptions
                {
                    HttpOnly = true,
                    SameSite = SameSiteMode.None,
                    Secure = true,
                    Expires = DateTime.UtcNow.AddHours(8)//AddHours(8)(4) no se
                });
                //Crear log de inicio de sesion
                _servicioLog.LogIn(usuarioEncontrado.userID);

                return Ok(new
                {
                    message = "success"
                });
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error en el método Login: {ex.Message}");
                return StatusCode(500, new { message = "Error interno del servidor" });
            }
        }

        [HttpGet("usuario")]
        public IActionResult Usuario()
        {
            try
            {

                var jwt = Request.Cookies["jwt"];
                if(jwt == null)
                {
                    return BadRequest(new { message = "Cookie no encontrada" });
                }
                var token = _jwtService.Verify(jwt);

                int userID = int.Parse(token.Issuer);
                var user = _repository.GetByID(userID);
                if(user != null) return Ok(user);
                else
                {
                    return BadRequest(new { message = "Cookie no encontrada" });
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { message = "Error interno del servidor", error = ex.Message });
            }

        }
        [HttpPost("logout")]
        public IActionResult Logout()
        {
            //Request cookie para obtener datos
            try
            {
                var jwt = Request.Cookies["jwt"];
                if(jwt == null)
                {
                    return BadRequest(new { message = "Cookie no encontrada" });
                }
                var token = _jwtService.Verify(jwt);
                int userID = int.Parse(token.Issuer);
                //Borrar cookie
                Response.Cookies.Delete("jwt", new CookieOptions
                {
                    HttpOnly = true,
                    SameSite = SameSiteMode.None,
                    Secure = true
                    //Expires = DateTime.Now.AddDays(-1) 
                });

                //Crear log de fin de sesion
                _servicioLog.LogOut(userID);

                return Ok(new
                {
                    message = "success"
                });
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { message = "Error interno del servidor" });
            }
            
        }
        [HttpPost("RecuperarPassword")]
        public ActionResult RecuperarPassword(Object jsonRoot)
        {
            //parsing json to JObject
            var Root = JObject.Parse(jsonRoot.ToString());
            //get email from JObject
            string email = (string)Root.GetValue("email");

            var user = _repository.GetByEmail(email);
            if(user != null)
            {
                _repository.enviarMailCambioContraseña(user.userID);
                return Ok();
            }
            else
            {
                return NotFound();
            }
            
        }
        [HttpPost("ValidarToken")]
        public ActionResult ValidarToken(Object jsonRoot)
        {
            //parsing json to JObject
            var Root = JObject.Parse(jsonRoot.ToString());
            //get token from JObject
            string token = (string)Root.GetValue("token");

            var user = _repository.GetByResetToken(token);
            if( user != null)
            {
                return Ok();
            }
            else
            {
                return NotFound();
            }
        }
    }
}

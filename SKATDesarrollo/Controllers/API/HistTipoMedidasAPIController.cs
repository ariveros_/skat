﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SKATDesarrollo.Context;
using SKATDesarrollo.Models;
using SKATDesarrollo.Repository;

namespace SKATDesarrollo.Controllers.API
{
    [Route("api/[controller]")]
    [AllowAnonymous]
    //[Authorize(Roles = "Full Admin, Admin")]
    [ApiController]
    public class HistTipoMedidasAPIController : ControllerBase
    {
        private readonly IServicioTipoMedida _servicio;

        public HistTipoMedidasAPIController(IServicioTipoMedida servicio)
        {
            _servicio = servicio;
        }
        [HttpGet]
        public IActionResult GetTipoMedidas()
        {
            var medidas = _servicio.GetHistTipoMedidas();
            return Ok(medidas);
        }
    }       
}

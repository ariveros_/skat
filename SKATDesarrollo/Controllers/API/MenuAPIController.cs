﻿#nullable disable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SKATDesarrollo.Context;
using SKATDesarrollo.Models;
using SKATDesarrollo.Models.ViewModel;
using SKATDesarrollo.Repository;
using System.Text.Json;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Authorization;

namespace SKATDesarrollo.Controllers.API
{
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    //[Authorize(Roles = "Full Admin, Admin")]
    public class MenuAPIController : ControllerBase
    {
        private readonly IServicioRol _servicio;

        public MenuAPIController(IServicioRol servicio)
        {
            _servicio = servicio;
        }
        [HttpGet("GetSidebar")]
        public IActionResult GetSidebar(int? idRol)
        {
            //List<string> list = new List<string>();
            SidebarVM sidebar = new SidebarVM();
            if (idRol != null)
            {
                sidebar = _servicio.GetSidebar((int)idRol);
            }

            return (sidebar != null) ? Ok(sidebar) : NotFound(new { message = "Error" });
        }

        [HttpGet("GetMenus")]
        public IActionResult GetMenus(int? idRol)
        {

            List<MenuVM> list = new List<MenuVM>();
            if (idRol != null)
            {
                list = _servicio.GetMenusByRol((int)idRol);
            }

            return (list != null) ? Ok(list) : NotFound(new { message = "Error" });
        }
        [HttpGet("GetOtrosMenus")]
        public IActionResult GetOtrosMenus(int? idRol)
        {

            List<MenuVM> list = new List<MenuVM>();
            if (idRol != null)
            {
                list = _servicio.GetOtrosMenus((int)idRol);
            }

            return (list != null) ? Ok(list) : NotFound(new { message = "Error" });
        }
    }
}

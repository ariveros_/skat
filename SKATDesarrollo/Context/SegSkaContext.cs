﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using SKATDesarrollo.Models;
using Microsoft.Extensions.Configuration;

namespace SKATDesarrollo.Context
{
    public partial class SegSkaContext : DbContext
    {
        protected readonly IConfiguration Configuration;

        //PARA LOCAL
        //public SegSkaContext()
        //{
        //}
        //public SegSkaContext(DbContextOptions<SegSkaContext> options)
        //    : base(options)
        //{
        //}

        //PARA PROD/TEST
        public SegSkaContext(IConfiguration configuration)
        {
            Configuration = configuration;
        }



        public virtual DbSet<SegLog> SegLogs { get; set; } = null!;
        public virtual DbSet<SegPassword> SegPasswords { get; set; } = null!;
        public virtual DbSet<SegRol> SegRols { get; set; } = null!;
        public virtual DbSet<SegTipoLog> SegTipoLogs { get; set; } = null!;
        public virtual DbSet<SegUser> SegUsers { get; set; } = null!;
        public virtual DbSet<SegUserRol> SegUserRols { get; set; } = null!;
        public virtual DbSet<SegMenu> SegMenus { get; set; } = null!;
        public virtual DbSet<SegPermisosMenu> SegPermisosMenus { get; set; } = null!;

        
        public virtual DbSet<HistCentroControl> HistCentroControl { get; set; } = null!;
        public virtual DbSet<HistEquipo> HistEquipo { get; set; } = null!;
        public virtual DbSet<HistEstacion> HistEstacion { get; set; } = null!;
        public virtual DbSet<HistEstadoTiempo> HistEstadoTiempo { get; set; } = null!;
        public virtual DbSet<HistPropiedad> HistPropiedad { get; set; } = null!;
        public virtual DbSet<HistPuntoScada> HistPuntoScada { get; set; } = null!;
        public virtual DbSet<HistRegistroMedida> HistRegistroMedida { get; set; } = null!;
        public virtual DbSet<HistTipoCenControl> HistTipoCenControl { get; set; } = null!;
        public virtual DbSet<HistTipoEquipo> HistTipoEquipo { get; set; } = null!;
        public virtual DbSet<HistTipoEquipoTipoMedida> HistTipoEquipoTipoMedida { get; set; } = null!;
        public virtual DbSet<HistTipoEstacion> HistTipoEstacion { get; set; } = null!;
        public virtual DbSet<HistTipoMedida> HistTipoMedida { get; set; } = null!;
        public virtual DbSet<HistValorMedida> HistValorMedida { get; set; } = null!;
        public virtual DbSet<HistZonaEstacion> HistZonaEstacion { get; set; } = null!;
        public virtual DbSet<HistTension> HistTension { get; set; } = null!;
       

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                //#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.

                /* -- LOCAL -- */
                optionsBuilder.UseSqlServer(Configuration.GetConnectionString("Local"));

                /* -- PROD -- */
                //optionsBuilder.UseSqlServer(Configuration.GetConnectionString("Prod"));
                /* -- TEST -- */
                //optionsBuilder.UseSqlServer(Configuration.GetConnectionString("Test"));

            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<SegLog>(entity =>
            {
                entity.HasKey(e => e.IdLog);

                entity.ToTable("SegLog");

                entity.Property(e => e.IdLog).HasColumnName("idLog");

                entity.Property(e => e.TimeLog)
                    .HasColumnType("datetime")
                    .HasColumnName("timeLog");

                entity.Property(e => e.TipoLogId).HasColumnName("tipoLogID");

                entity.Property(e => e.UserId).HasColumnName("userID");
            });

            modelBuilder.Entity<SegPassword>(entity =>
            {
                entity.HasKey(e => e.IdPass);

                entity.ToTable("SegPassword");

                entity.Property(e => e.IdPass).HasColumnName("idPass");

                entity.Property(e => e.ActivoPass).HasColumnName("activoPass");

                entity.Property(e => e.Desde)
                    .HasColumnType("datetime")
                    .HasColumnName("desde");

                entity.Property(e => e.HashPassword)
                    .HasMaxLength(255)
                    .HasColumnName("hashPassword");

                entity.Property(e => e.Hasta)
                    .HasColumnType("datetime")
                    .HasColumnName("hasta");

                entity.Property(e => e.UserId).HasColumnName("userID");
            });


            modelBuilder.Entity<SegRol>(entity =>
            {
                entity.HasKey(e => e.IdRol);

                entity.ToTable("SegRol");

                entity.Property(e => e.IdRol).HasColumnName("idRol");

                entity.Property(e => e.Activo)
                    .IsRequired()
                    .HasColumnName("activo")
                    .HasDefaultValueSql("(CONVERT([bit],(0)))");

                entity.Property(e => e.NombreRol)
                    .HasMaxLength(50)
                    .HasColumnName("nombreRol");
            });


            modelBuilder.Entity<SegTipoLog>(entity =>
            {
                entity.HasKey(e => e.IdTipoLog);

                entity.ToTable("SegTipoLog");

                entity.Property(e => e.IdTipoLog).HasColumnName("idTipoLog");

                entity.Property(e => e.DescripcionLog)
                    .HasMaxLength(50)
                    .HasColumnName("descripcionLog");
            });

            modelBuilder.Entity<SegUser>(entity =>
            {
                entity.HasKey(e => e.IdUser);

                entity.ToTable("SegUser");

                entity.Property(e => e.IdUser).HasColumnName("idUser");

                entity.Property(e => e.ActivoUser).HasColumnName("activoUser");

                entity.Property(e => e.Email)
                    .HasMaxLength(50)
                    .HasColumnName("email");
            });

            modelBuilder.Entity<SegUserRol>(entity =>
            {
                entity.HasKey(e => e.IdUserRol);

                entity.ToTable("SegUserRol");

                entity.Property(e => e.IdUserRol).HasColumnName("idUserRol");

                entity.Property(e => e.Activo).HasColumnName("activo");

                entity.Property(e => e.RoleId).HasColumnName("roleID");

                entity.Property(e => e.UserId).HasColumnName("userID");
            });
            modelBuilder.Entity<SegMenu>(entity =>
            {
                entity.HasKey(e => e.idMenu);
                entity.ToTable("SegMenu");
                entity.Property(e => e.idMenu).HasColumnName("idMenu");
                entity.Property(e => e.nombreMenu).HasColumnName("nombreMenu");
                entity.Property(e => e.codigoMenu).HasColumnName("codigoMenu");
                entity.Property(e => e.urlMenu).HasColumnName("urlMenu");
                entity.Property(e => e.iconoMenu).HasColumnName("iconoMenu");
                entity.Property(e => e.esPadre).HasColumnName("esPadre");
                entity.Property(e => e.nombreMenuOpen).HasColumnName("nombreMenuOpen");
                entity.Property(e => e.posicionMenu).HasColumnName("posicionMenu");
                entity.Property(e => e.idPadre).HasColumnName("idPadre");
                entity.Property(e => e.activo).HasColumnName("activo");

            });
            modelBuilder.Entity<SegPermisosMenu>(entity =>
            {
                entity.HasKey(e => e.idPermisoMenu);
                entity.ToTable("SegPermisosMenu");
                entity.Property(e => e.idPermisoMenu).HasColumnName("idPermisoMenu");
                entity.Property(e => e.idMenu).HasColumnName("idMenu");
                entity.Property(e => e.idRol).HasColumnName("idRol");
                entity.Property(e => e.activo).HasColumnName("activo");
            });

            modelBuilder.Entity<HistEquipo>(entity =>
            {
                entity.HasKey(e => e.idEquipo);
                entity.ToTable("HistEquipo");
                entity.Property(e => e.nombreEquipo).HasColumnName("nombreEquipo");
                entity.Property(e => e.potenciaNominal).HasColumnName("potenciaNominal");
                entity.Property(e => e.idTipoEquipo).HasColumnName("idTipoEquipo");
                entity.Property(e => e.idEstacion).HasColumnName("idEstacion");
                entity.Property(e => e.idPropiedad).HasColumnName("idPropiedad");
                entity.Property(e => e.activo).HasColumnName("activo");


            });

            modelBuilder.Entity<HistEstacion>(entity =>
            {
                entity.HasKey(e=> e.idEstacion);
                entity.ToTable("HistEstacion");
                entity.Property(e => e.nombreEstacion).HasColumnName("nombreEstacion");
                entity.Property(e => e.idZonaEstacion).HasColumnName("idZonaEstacion");
                entity.Property(e => e.idTipoEstacion).HasColumnName("idTipoEstacion");
                entity.Property(e => e.idCentroControl).HasColumnName("idCentroControl");
                entity.Property(e => e.activo).HasColumnName("activo");
            });

            modelBuilder.Entity<HistEstadoTiempo>(entity =>
            {
                entity.HasKey(e=>e.idEstadoTiempo);
                entity.ToTable("HistEstadoTiempo");
                entity.Property(e => e.estadoTiempo).HasColumnName("estadoTiempo");
                entity.Property(e => e.activo).HasColumnName("activo");
            });

            modelBuilder.Entity<HistPuntoScada>(entity =>
            {
                entity.HasKey(e=>e.idPuntoScada);
                entity.ToTable("HistPuntoScada");
                entity.Property(e => e.nroPuntoScada).HasColumnName("nroPuntoScada");
                entity.Property(e => e.fechaCreacionPunto).HasColumnName("fechaCreacionPunto");
                entity.Property(e => e.fechaFinPunto).HasColumnName("fechaFinPunto");
                entity.Property(e => e.idTension).HasColumnName("idTension");
                entity.Property(e => e.activo).HasColumnName("activo");
                entity.Property(e => e.idEquipo).HasColumnName("idEquipo");
                entity.Property(e => e.lado).HasColumnName("lado");
                entity.Property(e => e.descripcionPunto).HasColumnName("descripcionPunto");
            });

            modelBuilder.Entity<HistTension>(entity =>
            {
                entity.HasKey(e => e.idTension);
                entity.ToTable("HistTension");
                entity.Property(e => e.valorTension).HasColumnName("valorTension");
                entity.Property(e => e.activo).HasColumnName("activo");
            });

            modelBuilder.Entity<HistRegistroMedida>(entity =>
            {
                entity.HasKey(e => e.idRegistroMedida);
                entity.ToTable("HistRegistroMedida");
                entity.Property(e => e.fInicio).HasColumnName("fInicio");
                entity.Property(e => e.fFin).HasColumnName("fFin");
                entity.Property(e => e.idPuntoScada).HasColumnName("idPuntoScada");
                entity.Property(e => e.activo).HasColumnName("activo");
                entity.Property(e => e.idEstadoTiempo).HasColumnName("idEstadoTiempo");
                entity.Property(e => e.idTipoMedida).HasColumnName("idTipoMedida");
                entity.Property(e => e.createdByMichibotDate).HasColumnName("createdByMichibotDate");
            });
            modelBuilder.Entity<HistTipoEquipo>(entity =>
            {
                entity.HasKey(e=>e.idTipoEquipo);
                entity.ToTable("HistTipoEquipo");
                entity.Property(e => e.tipoEquipo).HasColumnName("tipoEquipo");
                entity.Property(e => e.activo).HasColumnName("activo");
            });

            modelBuilder.Entity<HistTipoMedida>(entity =>
            {
                entity.HasKey(e => e.idTipoMedida);
                entity.ToTable("HistTipoMedida");
                entity.Property(e => e.medida).HasColumnName("medida");
                entity.Property(e => e.unidadMedida).HasColumnName("unidadMedida");
                entity.Property(e => e.siglaMedida).HasColumnName("siglaMedida");
                entity.Property(e => e.activo).HasColumnName("activo");
            });

            modelBuilder.Entity<HistValorMedida>(entity => 
            {
                entity.HasKey(e => e.idValorMedida);
                entity.ToTable("HistValorMedida");
                entity.Property(e=>e.calidadMedida).HasColumnName("calidadMedida");
                entity.Property(e => e.valorMedida).HasColumnName("valorMedida");
                entity.Property(e=>e.horaMedida).HasColumnName("horaMedida");
                entity.Property(e => e.idRegistroMedida).HasColumnName("idRegistroMedida");
                entity.Property(e => e.activo).HasColumnName("activo");
                entity.Property(e => e.esHistorico).HasColumnName("esHistorico");
                entity.Property(e => e.fechaHistorico).HasColumnName("fechaHistorico");
                entity.Property(e => e.createdByMichibotDate).HasColumnName("createdByMichibotDate");
            });

            modelBuilder.Entity<HistTipoEstacion>(entity =>
            {
                entity.HasKey(e => e.idTipoEstacion);
                entity.ToTable("HistTipoEstacion");
                entity.Property(e => e.nombreTipoEstacion).HasColumnName("nombreTipoEstacion");
                entity.Property(e => e.siglaEstacion).HasColumnName("siglaEstacion");
                entity.Property(e => e.activo).HasColumnName("activo");
            });

            modelBuilder.Entity<HistTipoEquipoTipoMedida>(entity =>
            {
                entity.HasKey(e => e.idTipoEquipoTipoMedida);
                entity.ToTable("HistTipoEquipoTipoMedida");
                entity.Property(e => e.activo).HasColumnName("activo");
                entity.Property(e => e.idTipoMedida).HasColumnName("idTipoMedida");
                entity.Property(e => e.idTipoEquipo).HasColumnName("idTipoEquipo");
            });

            modelBuilder.Entity<HistPropiedad>(entity =>
            {
                entity.HasKey(e=>e.idPropiedad);
                entity.ToTable("HistPropiedad");
                entity.Property(e => e.nombrePropiedad).HasColumnName("nombrePropiedad");
                entity.Property(e => e.activo).HasColumnName("activo");
            });

            modelBuilder.Entity<HistZonaEstacion>(entity =>
            {
                entity.HasKey(e => e.idZonaEstacion);
                entity.ToTable("HistZonaEstacion");
                entity.Property(e => e.nombreZona).HasColumnName("nombreZona");
                entity.Property(e => e.activo).HasColumnName("activo");
            });

            modelBuilder.Entity<HistCentroControl>(entity =>
            {
                entity.HasKey(e => e.idCentroControl);
                entity.ToTable("HistCentroControl");
                entity.Property(e => e.nombreCentroControl).HasColumnName("nombreCentroControl");
                entity.Property(e => e.idTipoCentroControl).HasColumnName("idTipoCentroControl");
                entity.Property(e => e.activo).HasColumnName("activo");
            });

            modelBuilder.Entity<HistTipoCenControl>(entity =>
            {
                entity.HasKey(e => e.idTipoCentroControl);
                entity.ToTable("HistTipoCenControl");
                entity.Property(e => e.nombreTipoCenControl).HasColumnName("nombreTipoCenControl");
                entity.Property(e => e.activo).HasColumnName("activo");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}

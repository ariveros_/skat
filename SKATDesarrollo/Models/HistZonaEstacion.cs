﻿using System.ComponentModel.DataAnnotations;
namespace SKATDesarrollo.Models
{
    public partial class HistZonaEstacion
    {
        [Key]
        public int idZonaEstacion { get; set; }
        public string? nombreZona { get; set; }
        public bool? activo { get; set; }
    }
}

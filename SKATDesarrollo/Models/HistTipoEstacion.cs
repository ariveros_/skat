﻿using System.ComponentModel.DataAnnotations;
namespace SKATDesarrollo.Models
{
    public partial class HistTipoEstacion
    {
        [Key]
        public int idTipoEstacion { get; set; }
        public string? nombreTipoEstacion { set; get; }
        public string? siglaEstacion { set; get; }
        public bool? activo { get; set; }
    }
}

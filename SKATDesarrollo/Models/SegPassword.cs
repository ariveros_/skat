﻿using System;
using System.Collections.Generic;

namespace SKATDesarrollo.Models
{
    public partial class SegPassword
    {
        public int IdPass { get; set; }
        public DateTime? Desde { get; set; }
        public DateTime? Hasta { get; set; }
        public string? HashPassword { get; set; }
        public bool? ActivoPass { get; set; }
        public int UserId { get; set; }
    }
}

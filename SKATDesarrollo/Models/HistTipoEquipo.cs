﻿    using System.ComponentModel.DataAnnotations;
namespace SKATDesarrollo.Models
{
    public partial class HistTipoEquipo
    {
        [Key]
        public int idTipoEquipo { get; set; }
        public string? tipoEquipo { get; set; }
        public bool? activo { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations;
namespace SKATDesarrollo.Models
{
    public partial class HistValorMedida
    {
        [Key]
        public int idValorMedida { get; set; }
        public string? calidadMedida { get; set; }
        public double? valorMedida { get; set;}
        public DateTime? horaMedida { get; set;}
        public int? idRegistroMedida { get; set ; }
        public bool? activo { get; set; }
        public bool? esHistorico { get; set; }
        public DateTime? fechaHistorico { get; set;}
        public DateTime? createdByMichibotDate { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace SKATDesarrollo.Models
{
    public partial class SegUserRol
    {
        public int IdUserRol { get; set; }
        public int UserId { get; set; }
        public int RoleId { get; set; }
        public bool? Activo { get; set; }
    }
}

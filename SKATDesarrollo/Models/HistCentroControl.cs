﻿using System.ComponentModel.DataAnnotations;
namespace SKATDesarrollo.Models
{
    public partial class HistCentroControl
    {
        [Key]
        public int idCentroControl { get; set; }
        public string? nombreCentroControl { get; set; }
        public int? idTipoCentroControl { get; set; }
        public bool? activo { get; set; }
    }
}

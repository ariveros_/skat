﻿using System;
using System.Collections.Generic;

namespace SKATDesarrollo.Models
{
    public partial class SegRol
    {
        public int IdRol { get; set; }
        public string? NombreRol { get; set; }
        public bool? Activo { get; set; }
    }
}

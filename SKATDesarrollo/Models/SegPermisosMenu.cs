﻿using System.ComponentModel.DataAnnotations;
namespace SKATDesarrollo.Models
{
    public partial class SegPermisosMenu
    {
        [Key]
        public int idPermisoMenu { get; set; }
        public int? idMenu { get; set; }
        public int? idRol { get; set; }
        public bool? activo { get; set; }
    }
}

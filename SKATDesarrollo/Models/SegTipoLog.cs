﻿using System;
using System.Collections.Generic;

namespace SKATDesarrollo.Models
{
    public partial class SegTipoLog
    {
        public int IdTipoLog { get; set; }
        public string? DescripcionLog { get; set; }
    }
}

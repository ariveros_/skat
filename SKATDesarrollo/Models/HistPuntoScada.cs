﻿using System.ComponentModel.DataAnnotations;
namespace SKATDesarrollo.Models
{
    public partial class HistPuntoScada
    {
        [Key]
        public int idPuntoScada { get; set; }
        public string? nroPuntoScada { get; set; }
        public DateTime? fechaCreacionPunto { get; set; }
        public DateTime? fechaFinPunto { get; set; }
        public int? idTension { get; set; }
        public bool? activo { get; set; }
        public int? idEquipo { get; set; }
        public string? lado { get; set; }
        public string? descripcionPunto { get; set; }
    }
}

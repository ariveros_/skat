﻿using System.ComponentModel.DataAnnotations;
namespace SKATDesarrollo.Models
{
    public partial class HistRegistroMedida
    {
        [Key]
        public int idRegistroMedida { get; set; }
        public DateTime? fInicio { get; set; }
        public DateTime? fFin { get; set; }
        public int? idPuntoScada { get; set; }
        public bool? activo { get; set; }
        public int? idEstadoTiempo { get; set; }
        public int? idTipoMedida { get; set; }
        public DateTime? createdByMichibotDate { get; set; }

    }
}

﻿using System.ComponentModel.DataAnnotations;
namespace SKATDesarrollo.Models
{
    public partial class HistTipoEquipoTipoMedida
    {
        [Key]
        public int idTipoEquipoTipoMedida { get; set; }
        public int? idTipoMedida { get; set; }
        public int? idTipoEquipo { get; set; }
        public bool? activo { get; set; }
    }
}

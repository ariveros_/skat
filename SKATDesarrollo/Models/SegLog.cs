﻿using System;
using System.Collections.Generic;

namespace SKATDesarrollo.Models
{
    public partial class SegLog
    {
        public int IdLog { get; set; }
        public DateTime? TimeLog { get; set; }
        public int TipoLogId { get; set; }
        public int UserId { get; set; }
    }
}

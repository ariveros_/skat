﻿namespace SKATDesarrollo.Models.ViewModel
{
    public class ValorVM
    {
        public int idValorMedida { get; set; }
        public string? calidadMedida { get; set; }
        public double? valorMedida { get; set; }
        public DateTime? horaMedida { get; set; }
        public int? idRegistroMedida { get; set; }
        public int? idTrafo { get; set; }
        public int? idTipoMedida { get; set; }
        public string? nombreMedida { get; set; }
        public bool? activo { get; set; }
        public bool? esHistorico { get; set; }
        public DateTime? fechaHistorico { get; set; }
    }
}

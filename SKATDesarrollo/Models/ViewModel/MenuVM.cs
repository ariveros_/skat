﻿namespace SKATDesarrollo.Models.ViewModel
{
    public class MenuVM
    {
        public int? idMenu { get; set; }
        public string? nombreMenu { get; set; }
        public string? codigoMenu { get; set; }
        public string? urlMenu { get; set; }
        public string? iconoMenu { get; set; }
        public string? nombreMenuOpen { get; set; }
        public bool? esPadre { get; set; }
        public int? posicionMenu { get; set; }
        public int? idPadre { get; set; }
        public bool? activo { get; set; }
        public List<MenuVM>? submenus { get; set; }
    }
}

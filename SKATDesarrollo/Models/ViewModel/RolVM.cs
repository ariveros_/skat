﻿namespace SKATDesarrollo.Models.ViewModel
{
    public class RolVM
    {
        public int roleID { get; set; }
        public string? nombreRol { get; set; }
        public bool? activo { get; set; }
        public List<MenuVM> menuVMs { get; set; }
        
    }
}

﻿namespace SKATDesarrollo.Models.ViewModel
{
    public class dataTablaVM
    {
        public int? idTable { get; set; }
        public string? titleTable { get; set; }
        public List<HeaderTable>? headersTable { get; set; }
        public List<HeaderTable>? subheaderTables { get; set; }
        public List<RowBodyTable>? bodyTable { get; set; }

    }
    public class HeaderTable
    {
        public int? idHeader { get; set; }
        public string? headerTitle { get; set; }
        public int? idEquipo { get; set; }
        public bool? hasSide { get; set; }
        public List<HeaderTable>? subheaders { get; set; }
    }
    public class RowBodyTable
    {
        public int? idHour { get; set; }
        //public DateTime? dateHour { get; set; }
        public String? dateHour { get; set; }
        public List<RowValue>? rowValues { get; set; }

    }
    public class RowValue
    {
        public int? orderValue { get; set; }
        public int? idValorMedida { get; set; }
        public double? valorMedida { get; set; }
        public int? idHeader { get; set; }
        public int? idSubheader { get; set; }
        public String? dateHour { get; set; } //la uso solo para la tabla de maximos y mínimos de Carga de trafos
    }
    //viejo
    [Obsolete]
    public class TablaVM
    {
        public string? labelTabla { get; set; }
        public List<string?> labelEncabezado { get; set; }
        public List<DataTablaVM>? dataTablaVMs { get; set; }
    }
    [Obsolete]
    public class DataTablaVM
    {
        public int? idHora { get; set; }
        public string? hora { get; set; }
        public List<double?> Valores { get; set; }

    }
    [Obsolete]
    public class DatosTablaVM
    {

        public int? id { get; set; }
        public double? valor { get; set; }

    }

}

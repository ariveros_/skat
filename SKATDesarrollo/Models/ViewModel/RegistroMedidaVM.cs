﻿namespace SKATDesarrollo.Models.ViewModel
{
    public class RegistroMedidaVM
    {
        public int idRegistroMedida { get; set; }
        public DateTime? fInicio { get; set; }
        public DateTime? fFin { get; set; }
        public int? idPuntoScada { get; set; }
        public string? nroScada { get; set; }
        public string? lado { get; set; }
        public int? idTension { get; set; }
        public string? nivelTension { get; set; }
        public bool? activo { get; set; }
        public int? idEstadoTiempo { get; set; }
        public int? idEquipo { get; set; }
        public string? nombreEquipo { get; set; }
        public int? idTipoMedida { get; set; }
        public int? idtipoEquipo { get; set; }
        public string? tipoEquipo { get; set; }
        public string? descripcionMedida { get; set; }
        public string? unidadMedida { get; set; }
        public string? siglaMedida { get; set; }
        public double? minVal { get; set; }
        public double? maxVal { get; set; }
        public List<ValorVM>? Valores { get; set; }

    }
}

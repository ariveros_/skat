﻿

using System.Text.Json.Serialization;

namespace SKATDesarrollo.Models.ViewModel
{
    public class UsuarioVM
    {
        public int userID { get; set; }
        public string? email { get; set; }
        public string? password { get; set; }
        public bool? activo { get; set; }
        public int idRol { get; set; }
        public string? rolName { get; set; }
    }
}

﻿namespace SKATDesarrollo.Models.ViewModel
{
    public class UsuarioRolVM
    {
        public int IdUserRol { get; set; }
        public int IdUser{ get; set; }
        public string? email { get; set; }
        public int IdRol { get; set; }
        public string? nombreRol { get; set; }
        public bool? activo { get; set; }

    }
}

﻿namespace SKATDesarrollo.Models.ViewModel
{
    public class dataGraficoVM
    {
        public int? contGraficos { get; set; }
        public double? minValue { get; set; }
        public double? maxValue { get; set; }
        public List<string>? labels { get; set; }
        public List<DatasetVM>? datasets { get; set; }
        public string? labelGrafico { get; set; }
    }
    public class DatasetVM
    {
        public string? label { get; set; }
        public List<double?> data { get; set; }
    }
    

}

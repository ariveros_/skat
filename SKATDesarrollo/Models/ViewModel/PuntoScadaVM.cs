﻿namespace SKATDesarrollo.Models.ViewModel
{
    public class PuntoScadaVM
    {
        public int? idPuntoScada { get; set; }
        public string? nroScada { get; set; }
        public DateTime? fechaCreacionPunto { get; set;}
        public DateTime? fechaFinPunto { get; set;}
        public bool? activo { get; set; }
        public int? idEquipo { get; set; }
        public int? idEstacion { get; set; }
        public string? nombreEstacion { get; set; }
        public int? idTension { get; set; }
        public string? lado { get; set; }
    }
}

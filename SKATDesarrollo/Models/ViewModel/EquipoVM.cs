﻿namespace SKATDesarrollo.Models.ViewModel
{
    public class EquipoVM
    {
        public int idEquipo { get; set; }
        public string? nombreEquipo { get; set; }
        public bool? activo { get; set; }
        public int? idEstacion { get; set; }
        public string? nombreEstacion { get; set; }
        public int? idTipoEquipo { get; set; }
        public string? nombreTipoEquipo { get; set; }
       public List<PuntoScadaVM> puntoScadas { get; set; }
    }
}

﻿namespace SKATDesarrollo.Models.ViewModel
{
    public class MedidaVM
    {
        public int idTipoMedida { get; set; }
        public string? medida { get; set; }
        public string? unidadMedida { get; set; }
        public string? siglaMedida { set; get; }
        public bool? activo { get; set; }
    }
}

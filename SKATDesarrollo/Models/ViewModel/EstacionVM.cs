﻿namespace SKATDesarrollo.Models.ViewModel
{
    public class EstacionVM
    {
        public int idEstacion { get; set; }
        public string? nombreEstacion { get; set; }
        public int? idZonaEstacion { get; set; }
        public string? nombreZonaEstacion { get; set; }
        public int? idTipoEstacion { get; set; }
        public string? nombreTipoEstacion { get; set; }
        public int? idCentroControl { get; set; }
        public string? nombreCentroControl { get; set; }
        public bool? activo { get; set; }
    }
}

﻿namespace SKATDesarrollo.Models.ViewModel
{
    public class SidebarVM
    {
        public int? idRol { get; set; }
        public string nombreRol { get; set; }
        public string sidebar { get; set; }
    }
}

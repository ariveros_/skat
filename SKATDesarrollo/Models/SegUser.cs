﻿using System;
using System.Collections.Generic;

namespace SKATDesarrollo.Models
{
    public partial class SegUser
    {
        public int IdUser { get; set; }
        public string? Email { get; set; }
        public bool? ActivoUser { get; set; }
        public string? ResetToken { get; set; }
        public DateTime? ResetTokenExpires { get; set; }
    }
}

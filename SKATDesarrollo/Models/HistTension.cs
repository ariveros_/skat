﻿using System.ComponentModel.DataAnnotations;
namespace SKATDesarrollo.Models
{
    public partial class HistTension
    {
        [Key]
        public int idTension { get; set; }
        public string? valorTension { get; set; }
        public bool? activo { get; set; }
    }
}

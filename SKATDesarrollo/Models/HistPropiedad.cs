﻿using System.ComponentModel.DataAnnotations;
namespace SKATDesarrollo.Models
{
    public partial class HistPropiedad
    {
        [Key]
        public int idPropiedad { get; set; }
        public string? nombrePropiedad { get; set; }
        public bool? activo { get; set; }
    }
}

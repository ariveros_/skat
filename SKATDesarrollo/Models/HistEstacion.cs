﻿using System.ComponentModel.DataAnnotations;

namespace SKATDesarrollo.Models

{
    public partial class HistEstacion
    {
        [Key]
        public int idEstacion { get; set; }
        public string? nombreEstacion { get; set; }
        public int? idZonaEstacion { get; set; }
        public int? idTipoEstacion { get; set; }
        public int? idCentroControl { get; set; }
        public bool? activo { get; set; }

    }
}

﻿using System.ComponentModel.DataAnnotations;
namespace SKATDesarrollo.Models
{
    public partial class HistTipoMedida
    {
        [Key]
        public int idTipoMedida { get; set; }
        public string? medida { get; set; }
        public string? unidadMedida { get; set; }
        public string? siglaMedida { set; get; }
        public bool? activo { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations;
namespace SKATDesarrollo.Models
{
    public partial class HistEstadoTiempo
    {
        [Key]
        public int idEstadoTiempo { get; set; }
        public string? estadoTiempo { get; set; }
        public bool? activo { get; set; }
    }
}

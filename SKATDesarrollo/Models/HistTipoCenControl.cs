﻿
using System.ComponentModel.DataAnnotations;
namespace SKATDesarrollo.Models
{
    public partial class HistTipoCenControl
    {
        [Key]
        public int idTipoCentroControl { get; set; }
        public string? nombreTipoCenControl { get; set; }
        public bool? activo { get; set; }
    }
}

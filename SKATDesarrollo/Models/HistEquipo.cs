﻿using System.ComponentModel.DataAnnotations;
namespace SKATDesarrollo.Models
{
    public partial class HistEquipo
    {
        [Key]
        public int idEquipo { get; set; }
        public string? nombreEquipo { get; set; }
        public double? potenciaNominal { get; set; }
        public bool? activo { get; set; }
        public int? idEstacion { get; set; }
        public int? idTipoEquipo { get; set; }
        public int? idPropiedad { get; set; }
    }
}

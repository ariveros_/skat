﻿using SKATDesarrollo.Context;
using SKATDesarrollo.Models;
using SKATDesarrollo.Models.ViewModel;
using System.Globalization;
using System.Text.Json;

namespace SKATDesarrollo.Repository
{
    public class ServicioTension : IServicioTension
    {
        private readonly SegSkaContext _context;
        public ServicioTension(SegSkaContext context)
        {
            _context = context;
        }
        public Object consultaTensiones()
        {
            var tensiones = _context.HistTension.Where(x=> x.activo == true).ToList();
            return JsonSerializer.Serialize(tensiones);
        }
    }
}

﻿namespace SKATDesarrollo.Repository
{
    public interface IServicioLog
    {
        void LogIn(int userID);
        void LogOut(int userID);
    }
}

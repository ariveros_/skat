﻿using Microsoft.Win32;
using SKATDesarrollo.Context;
using SKATDesarrollo.Models;
using SKATDesarrollo.Models.ViewModel;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text.Json;
using System.Transactions;

namespace SKATDesarrollo.Repository
{
    public class ServicioReportes : IServicioReportes
    {
        private readonly SegSkaContext _context;
        public ServicioReportes(SegSkaContext context)
        {
            _context = context;
        }

        #region pruebas para carga de datos
        public Boolean duplica()
        {
            try
            {
                DateTime fechaInicial = new DateTime(2022, 07, 05, 0, 0, 0);
                DateTime fechaFinal = new DateTime(2022, 07, 05, 23, 55, 0);

                var tmpRegistros = registros().Where(x=> x.fInicio >= fechaInicial && x.fFin <= fechaFinal).ToList();
                foreach (var registro in tmpRegistros)
                {
                    HistRegistroMedida newRegistro = new HistRegistroMedida
                    {
                        fInicio = registro.fInicio.Value.AddDays(2),
                        fFin = registro.fFin.Value.AddDays(2),
                        idEstadoTiempo = registro.idEstadoTiempo,
                        idPuntoScada = registro.idPuntoScada,
                        activo = registro.activo,
                        idTipoMedida = registro.idTipoMedida
                    };
                    _context.HistRegistroMedida.Add(newRegistro);
                    _context.SaveChanges();

                    int idNewReg = registros().OrderByDescending(x => x.idRegistroMedida).FirstOrDefault().idRegistroMedida;
                    var tmpValores = valores(registro.idRegistroMedida).ToList();
                    foreach (var valor in tmpValores)
                    {
                        HistValorMedida newValor = new HistValorMedida
                        {
                            idRegistroMedida = idNewReg,
                            calidadMedida = valor.calidadMedida,
                            valorMedida=valor.valorMedida,
                            horaMedida = valor.horaMedida.Value.AddDays(2),
                            activo = valor.activo,
                            esHistorico = valor.esHistorico,
                            fechaHistorico = valor.fechaHistorico
                        };
                        _context.HistValorMedida.Add(newValor);
                    }
                        _context.SaveChanges();
                }

                return true;
            }
            catch(Exception e)
            {
                return false;
            }
        }
        public Boolean pruebaCalculoPorcentaje()
        {
            /**** ESTO SE MODIFICA ****/
            var idEquipo = 1008; //id del equipo
            var idRegistroPA = 12137; // id registro de potencia aparente previamente cargado
            
            var equipo = _context.HistEquipo.Where(x => x.idEquipo == idEquipo).FirstOrDefault();
            HistPuntoScada puntoScada = new HistPuntoScada
            {
                fechaCreacionPunto = DateTime.Now,
                fechaFinPunto = null,
                activo = true,
                idEquipo = idEquipo,
                nroPuntoScada = null,
                idTension=4 // recordar modificar
            };
            /**** fin de modificaciones ***/
            #region calculo de porcentaje
            _context.HistPuntoScada.Add(puntoScada);
            _context.SaveChanges();

            int idNuevoPunto = _context.HistPuntoScada.OrderByDescending(x => x.idPuntoScada).Where(x => x.activo == true).FirstOrDefault().idPuntoScada;
            HistRegistroMedida histRegistroMedida = new HistRegistroMedida
            {
                fInicio = DateTime.Parse("05-07-2022 00:00:00"),
                fFin = DateTime.Parse("05-07-2022 23:55:00"),
                activo = true,
                idEstadoTiempo = 1,
                idPuntoScada = idNuevoPunto,
                idTipoMedida = 10
            };
            _context.HistRegistroMedida.Add(histRegistroMedida);
            _context.SaveChanges();

            int idRegistroM = _context.HistRegistroMedida.OrderByDescending(x => x.idRegistroMedida).Where(x => x.activo == true).FirstOrDefault().idRegistroMedida;

            var potenciaNominal = equipo.potenciaNominal;
            var fInicio = DateTime.Parse("05-07-2022 00:00:00");
            var fFin = DateTime.Parse("05-07-2022 23:55:00");

            var potAparente = (valoresQuery(idRegistroPA, fInicio, fFin, idEquipo)).ToList();
            List<HistValorMedida> valorPorcentaje = new List<HistValorMedida>();
            foreach (var valorPA in potAparente)
            {
                double valorcito = (double)valorPA.valorMedida;
                var porcentaje = (valorcito * 100) / potenciaNominal;

                HistValorMedida valorCalculado = new HistValorMedida
                {
                    calidadMedida = null,
                    valorMedida = porcentaje,
                    horaMedida = valorPA.horaMedida,
                    idRegistroMedida = idRegistroM,
                    activo = true,
                    esHistorico = false,
                    fechaHistorico = null
                };
                valorPorcentaje.Add(valorCalculado);

            }
            _context.HistValorMedida.AddRange(valorPorcentaje);
            _context.SaveChanges();
            #endregion
            return true;
        }
        public Boolean pruebaCalculoPotenciaAparenteS()
        {
            /**** ESTO SE MODIFICA ****/
            int idEquipo = 1008; //id del equipo
            int idRegistroP= 12135; //id registro de Potencia Activa (P)
            int idRegistroQ = 12136; //id registro de Potencia Reactiva (Q)
 
            
            HistPuntoScada puntoScada = new HistPuntoScada
            {
                fechaCreacionPunto = DateTime.Now,
                fechaFinPunto = null,
                activo = true,
                idEquipo = idEquipo,
                nroPuntoScada = null,
                idTension=4 //recordar cambiar
            };
            /**** fin de modificaciones ***/
            #region calculo de pot aparente
            _context.HistPuntoScada.Add(puntoScada);
            _context.SaveChanges();
            int idNuevoPunto = _context.HistPuntoScada.OrderByDescending(x => x.idPuntoScada).Where(x => x.activo == true).FirstOrDefault().idPuntoScada;
            HistRegistroMedida histRegistroMedida = new HistRegistroMedida
            {
                fInicio = DateTime.Parse("05-07-2022 00:00:00"),
                fFin = DateTime.Parse("05-07-2022 23:55:00"),
                activo = true,
                idEstadoTiempo = 1,
                idPuntoScada = idNuevoPunto,
                idTipoMedida = 3 
            };
            _context.HistRegistroMedida.Add(histRegistroMedida);
            _context.SaveChanges();

            int idRegistroM = _context.HistRegistroMedida.OrderByDescending(x => x.idRegistroMedida).Where(x => x.activo == true).FirstOrDefault().idRegistroMedida;

            DateTime fInicio = DateTime.Parse("05-07-2022 00:00:00");
            DateTime fFin = DateTime.Parse("05-07-2022 23:55:00");

            var potActiva = (valoresQuery(idRegistroP, fInicio, fFin, idEquipo)).ToList();
            var potReactiva = (valoresQuery(idRegistroQ, fInicio, fFin, idEquipo)).ToList();

            List<HistValorMedida> valorPA = new List<HistValorMedida>();
            for (var i = 0; i < potReactiva.Count; i++)
            {
                double pa = (double)potActiva[i].valorMedida;
                double pq = (double)potReactiva[i].valorMedida;

                var valorcito = Math.Sqrt(Math.Pow(pa, 2) + Math.Pow(pq, 2));


                HistValorMedida valorCalculado = new HistValorMedida
                {
                    calidadMedida = null,
                    valorMedida = valorcito,
                    horaMedida = potActiva[i].horaMedida,
                    idRegistroMedida = idRegistroM,
                    activo = true,
                    esHistorico = false,
                    fechaHistorico = null
                };
                valorPA.Add(valorCalculado);
            }
            _context.HistValorMedida.AddRange(valorPA);
            _context.SaveChanges();
            #endregion
            return true;
        }
        public Boolean pruebaEquipo()
        {
            var idEstacion = 6;
            HistEquipo equipo1 = new HistEquipo
            {
                nombreEquipo = "LAT 1 Gran Mendoza", 
                potenciaNominal = null,
                activo = true,
                idEstacion = idEstacion,
                idTipoEquipo = 2, 
                idPropiedad = 1
            };
            _context.HistEquipo.Add(equipo1);

            HistEquipo equipo2 = new HistEquipo
            {
                nombreEquipo = "LAT 2 Gran Mendoza",
                potenciaNominal = null,
                activo = true,
                idEstacion = idEstacion,
                idTipoEquipo = 2,
                idPropiedad = 1
            };
            _context.HistEquipo.Add(equipo2);

            HistEquipo equipo3 = new HistEquipo
            {
                nombreEquipo = "LAT 1 Lujan de Cuyo",
                potenciaNominal = null,
                activo = true,
                idEstacion = idEstacion,
                idTipoEquipo = 2,
                idPropiedad = 1
            };
            _context.HistEquipo.Add(equipo3);

            HistEquipo equipo4 = new HistEquipo
            {
                nombreEquipo = "LAT 2 Lujan de Cuyo",
                potenciaNominal = null,
                activo = true,
                idEstacion = idEstacion,
                idTipoEquipo = 2,
                idPropiedad = 1
            };
            _context.HistEquipo.Add(equipo4);

            HistEquipo equipo5 = new HistEquipo
            {
                nombreEquipo = "LAT Anchoris",
                potenciaNominal = null,
                activo = true,
                idEstacion = idEstacion,
                idTipoEquipo = 2,
                idPropiedad = 1
            };
            _context.HistEquipo.Add(equipo5);

            //HistEquipo equipo6 = new HistEquipo
            //{
            //    nombreEquipo = "LAT Junin",
            //    potenciaNominal = null,
            //    activo = true,
            //    idEstacion = idEstacion,
            //    idTipoEquipo = 2,
            //    idPropiedad = 1
            //};
            //_context.HistEquipo.Add(equipo6);

            //HistEquipo equipo7 = new HistEquipo
            //{
            //    nombreEquipo = "LAT Alto Verde",
            //    potenciaNominal = null,
            //    activo = true,
            //    idEstacion = idEstacion,
            //    idTipoEquipo = 2,
            //    idPropiedad = 1
            //};
            //_context.HistEquipo.Add(equipo7);

            //HistEquipo equipo8 = new HistEquipo
            //{
            //    nombreEquipo = "LAT Anchoris",
            //    potenciaNominal = null,
            //    activo = true,
            //    idEstacion = idEstacion,
            //    idTipoEquipo = 2,
            //    idPropiedad = 1
            //};
            //_context.HistEquipo.Add(equipo8);

            //HistEquipo equipo9 = new HistEquipo
            //{
            //    nombreEquipo = "LAT COOP.POPULAR RIVADAVIA",
            //    potenciaNominal = null,
            //    activo = true,
            //    idEstacion = idEstacion,
            //    idTipoEquipo = 2,
            //    idPropiedad = 1
            //};
            //_context.HistEquipo.Add(equipo9);

            //HistEquipo equipo10 = new HistEquipo
            //{
            //    nombreEquipo = "COMP. CAPACITIVA",
            //    potenciaNominal = null,
            //    activo = true,
            //    idEstacion = idEstacion,
            //    idTipoEquipo = 5,
            //    idPropiedad = 1
            //};
            //_context.HistEquipo.Add(equipo10);


            //HistEquipo equipo11 = new HistEquipo
            //{
            //    nombreEquipo = "LAT COOP.SUBRIO TUNUYAN",
            //    potenciaNominal = null,
            //    activo = true,
            //    idEstacion = idEstacion,
            //    idTipoEquipo = 2,
            //    idPropiedad = 1
            //};
            //_context.HistEquipo.Add(equipo11);
            _context.SaveChanges();
            
            return true;
        }
        public Boolean pruebaCargaValores()
        {
            /**** ESTO SE MODIFICA ****/
            int idEquipo = 2016; /* Linea 2 Argentina */
            string numeroPtoScada = "241020";
            string valoresTxt = "241020;00;-3.791;----;-3.791;----;-3.791;----;-3.756;----;-3.756;----;-3.791;----;-3.791;----;-3.894;----;-3.825;----;-3.756;----;-3.756;----;-3.756;----; 241020;01;-3.756;----;-3.756;----;-3.721;----;-3.721;----;-3.721;----;-3.687;----;-3.687;----;-3.687;----;-3.687;----;-3.721;----;-3.721;----;-3.756;----; 241020;02;-3.756;----;-3.721;----;-3.791;----;-3.791;----;-4.102;----;-4.102;----;-4.102;----;-4.102;----;-4.102;----;-4.102;----;-4.171;----;-3.963;----; 241020;03;-4.102;----;-4.102;----;-4.067;----;-4.067;----;-4.067;----;-4.102;----;-4.102;----;-4.102;----;-4.102;----;-4.067;----;-4.067;----;-4.102;----; 241020;04;-4.205;----;-4.067;----;-4.067;----;-4.067;----;-4.067;----;-4.205;----;-4.205;----;-4.205;----;-4.102;----;-4.067;----;-4.067;----;-4.102;----; 241020;05;-4.102;----;-4.102;----;-4.102;----;-4.102;----;-4.102;----;-4.102;----;-4.102;----;-4.102;----;-4.136;----;-4.136;----;-4.102;----;-4.102;----; 241020;06;-4.136;----;-4.136;----;-4.102;----;-4.136;----;-4.136;----;-4.136;----;-4.136;----;-4.136;----;-4.102;----;-4.102;----;-4.102;----;-4.102;----; 241020;07;-4.102;----;-4.102;----;-4.136;----;-4.136;----;-4.136;----;-4.136;----;-4.136;----;-4.136;----;-4.136;----;-4.136;----;-4.136;----;-4.102;----; 241020;08;-4.136;----;-4.136;----;-4.136;----;-4.136;----;-4.136;----;-4.102;----;-4.102;----;-4.102;----;-4.136;----;-4.136;----;-4.136;----;-4.102;----; 241020;09;-4.102;----;-4.102;----;-4.136;----;-4.240;----;-4.102;----;-4.136;----;-4.136;----;-4.032;----;-4.136;----;-4.102;----;-4.102;----;-4.136;----; 241020;10;-4.102;----;-4.102;----;-4.171;----;-4.171;----;-4.205;----;-4.205;----;-4.205;----;-4.205;----;-4.240;----;-4.240;----;-4.240;----;-4.240;----; 241020;11;-4.240;----;-4.240;----;-4.240;----;-4.240;----;-4.240;----;-4.240;----;-4.205;----;-4.205;----;-4.240;----;-4.240;----;-4.274;----;-4.240;----; 241020;12;-4.240;----;-4.240;----;-4.240;----;-4.240;----;-4.240;----;-4.240;----;-4.240;----;-4.240;----;-4.240;----;-4.240;----;-4.240;----;-4.205;----; 241020;13;-4.205;----;-4.205;----;-4.240;----;-4.240;----;-4.240;----;-4.240;----;-4.240;----;-4.240;----;-4.205;----;-4.240;----;-4.240;----;-4.205;----; 241020;14;-4.240;----;-4.240;----;-4.205;----;-4.171;----;-4.171;----;-4.205;----;-4.205;----;-4.205;----;-4.171;----;-4.171;----;-4.171;----;-4.205;----; 241020;15;-4.205;----;-4.205;----;-4.171;----;-4.171;----;-4.171;----;-4.136;----;-4.136;----;-4.171;----;-4.171;----;-4.171;----;-4.171;----;-4.136;----; 241020;16;-4.136;----;-4.136;----;-4.136;----;-4.136;----;-4.136;----;-4.171;----;-4.171;----;-4.171;----;-4.171;----;-4.171;----;-4.136;----;-4.171;----; 241020;17;-4.136;----;-4.136;----;-4.171;----;-4.171;----;-4.171;----;-4.205;----;-4.205;----;-4.205;----;-4.171;----;-4.171;----;-4.171;----;-4.171;----; 241020;18;-4.171;----;-4.171;----;-4.171;----;-4.171;----;-4.171;----;-4.171;----;-4.171;----;-4.171;----;-4.205;----;-4.205;----;-4.205;----;-4.171;----; 241020;19;-4.171;----;-4.171;----;-4.171;----;-4.171;----;-4.171;----;-4.205;----;-4.205;----;-4.309;----;-3.445;----;-3.445;----;-3.445;----;-3.410;----; 241020;20;-3.410;----;-3.445;----;-3.410;----;-3.410;----;-3.410;----;-3.445;----;-3.445;----;-3.445;----;-3.480;----;-3.445;----;-3.445;----;-3.480;----; 241020;21;-3.480;----;-3.480;----;-3.445;----;-3.445;----;-3.445;----;-3.480;----;-3.480;----;-3.480;----;-3.480;----;-3.480;----;-3.480;----;-3.480;----; 241020;22;-3.480;----;-3.480;----;-3.480;----;-3.480;----;-3.480;----;-3.445;----;-3.480;----;-3.480;----;-3.480;----;-3.480;----;-3.480;----;-3.480;----; 241020;23;-3.480;----;-3.480;----;-3.480;----;-3.480;----;-4.205;----;-3.894;----;-3.894;----;-4.032;----;-4.171;----;-4.171;----;-4.205;----;-4.274;----;";
            string calidadMedida = "----";
            DateTime startDate = DateTime.Parse("06-07-2022 00:00:00");
            DateTime endDate = DateTime.Parse("06-07-2022 23:55:00");

            //HistPuntoScada histPuntoScada = new HistPuntoScada
            //{
            //    fechaCreacionPunto = null,
            //    fechaFinPunto = null,
            //    activo = true,
            //    idEquipo = idEquipo,
            //    nroPuntoScada = numeroPtoScada,
            //    idTension = 3
            //};
            //#region Tabla de tensiones (ayuda memoria)
            ///* =================================================
            // *        ID             TENSIÓN (kV)
            // * =================================================         
            // *        1             13,2
            // *        2             33
            // *        3             66
            // *        4             132
            // *        5             220
            // *        6             500
            // * =================================================
            //*/
            //#endregion
            //_context.HistPuntoScada.Add(histPuntoScada);
            //_context.SaveChanges();

            //int idNuevoPunto = _context.HistPuntoScada.OrderByDescending(x => x.idPuntoScada).Where(x => x.activo == true).FirstOrDefault().idPuntoScada;
            int idNuevoPunto = 13815;
            HistRegistroMedida histRegistroMedida = new HistRegistroMedida
            {
                fInicio = startDate,
                fFin = endDate,
                activo = true,
                idEstadoTiempo = 1,
                idPuntoScada = idNuevoPunto,
                idTipoMedida = 2
            };
            #region Tabla de variables (ayuda memoria)
            /* =================================================
             *        ID             VARIABLE (SIGLA)
             * =================================================         
             *        1             POT. ACTIVA (P)
             *        2             POT. REACTIVA (Q)
             *        3             POT. APARENTE (S)
             *        4             TENSIÓN (V)
             *        5             CORRIENTE (I)
             *        6             FRECUENCIA (F)
             *        7             TEMPERATURA (T)
             *        8             DEMANDA TOTAL (DT)
             *        10            PORCENTAJE DE CARGA (%)
             *        11            COSENO PHI (COSF)
             * =================================================
            */
            #endregion

            _context.HistRegistroMedida.Add(histRegistroMedida);
            _context.SaveChanges();
            /**** fin de modificaciones ***/

            int idRegistroAGuardar = _context.HistRegistroMedida.OrderByDescending(x => x.idRegistroMedida).Where(x => x.activo == true).FirstOrDefault().idRegistroMedida;

            
            string nroPunto = numeroPtoScada + ";";
            var valoresSeparados = valoresTxt.Split(new string[] { nroPunto }, StringSplitOptions.None);

            #region declaración de variables para procesamiento
            string phrase0 = "";
            string phrase1 = "";
            string phrase2 = "";
            string phrase3 = "";
            string phrase4 = "";
            string phrase5 = "";
            string phrase6 = "";
            string phrase7 = "";
            string phrase8 = "";
            string phrase9 = "";
            string phrase10 = "";
            string phrase11 = "";
            string phrase12 = "";
            string phrase13 = "";
            string phrase14 = "";
            string phrase15 = "";
            string phrase16 = "";
            string phrase17 = "";
            string phrase18 = "";
            string phrase19 = "";
            string phrase20 = "";
            string phrase21 = "";
            string phrase22 = "";
            string phrase23 = "";
            #endregion
            #region procesamiento de strings
            for (int i=1;i < valoresSeparados.Length; i++)
            {
                var frases = valoresSeparados[i].Split(new char[] {';'}, 2);
                switch (frases[0])
                {
                    case "00":
                        phrase0= frases[1];
                        break;
                    case "01":
                        phrase1 = frases[1];
                        break ;
                    case "02":
                        phrase2 = frases[1];
                        break;
                    case "03":
                        phrase3 = frases[1];

                        break;
                    case "04":
                        phrase4 = frases[1];
                        break;
                    case "05":
                        phrase5 = frases[1];
                        break;
                    case "06":
                         phrase6= frases[1];
                        break;
                    case "07":
                        phrase7 = frases[1];
                        break;
                    case "08":
                         phrase8 = frases[1];
                        break;
                    case "09":
                        phrase9 = frases[1];
                        break;
                    case "10":
                        phrase10 = frases[1];
                        break;
                    case "11":
                         phrase11= frases[1];
                        break;
                    case "12":
                        phrase12 = frases[1];
                        break;
                    case "13":
                        phrase13 = frases[1];
                        break;
                    case "14":
                        phrase14 = frases[1];
                        break;
                    case "15":
                        phrase15 = frases[1];
                        break;
                    case "16":
                        phrase16 = frases[1];
                        break;
                    case "17":
                        phrase17 = frases[1];
                        break;
                    case "18":
                        phrase18 = frases[1];
                        break;
                    case "19":
                        phrase19 = frases[1];
                        break;
                    case "20":
                        phrase20 = frases[1];
                        break;
                    case "21":
                        phrase21 = frases[1];
                        break;
                    case "22":
                        phrase22 = frases[1];
                        break;
                    case "23":
                        phrase23 = frases[1];
                        break;
                    
                    default:
                        break;
                }

            }

            //00
            string[] words0 = phrase0.Split(';');
            List<decimal> data0 = new List<decimal>();
            for (var i = 0; i <= words0.Length - 2; i++)
            {
                if (i % 2 == 0)
                {
                    decimal valoresHora;
                    valoresHora = decimal.Parse(words0[i], CultureInfo.InvariantCulture);
                    data0.Add(valoresHora);
                }
            }
            DateTime inicialHora0 = startDate; //ACTUALIZAR HORA
            foreach (var valor in data0)
            {

                HistValorMedida valorcito = new HistValorMedida
                {
                    calidadMedida = calidadMedida,
                    valorMedida = (double)valor,
                    horaMedida = inicialHora0,
                    idRegistroMedida = idRegistroAGuardar,
                    activo = true,
                    esHistorico = false,
                    fechaHistorico = null
                };

                _context.HistValorMedida.Add(valorcito);
                _context.SaveChanges();
                inicialHora0 = inicialHora0.AddMinutes(5);
            }

            //01
            string[] words1 = phrase1.Split(';');
            List<decimal> data1 = new List<decimal>();
            for (var i = 0; i <= words1.Length - 2; i++)
            {
                if (i % 2 == 0)
                {
                    decimal valoresHora;
                    valoresHora = decimal.Parse(words1[i], CultureInfo.InvariantCulture);
                    data1.Add(valoresHora);
                }
            }
            DateTime inicialHora1 = startDate.AddHours(1); //ACTUALIZAR HORA
            foreach (var valor in data1)
            {

                HistValorMedida valorcito = new HistValorMedida
                {
                    calidadMedida = calidadMedida,
                    valorMedida = (double)valor,
                    horaMedida = inicialHora1,
                    idRegistroMedida = idRegistroAGuardar,
                    activo = true,
                    esHistorico = false,
                    fechaHistorico = null
                };

                _context.HistValorMedida.Add(valorcito);
                _context.SaveChanges();
                inicialHora1 = inicialHora1.AddMinutes(5);
            }

            //02
           
            string[] words2 = phrase2.Split(';');
            List<decimal> data2 = new List<decimal>();
            for (var i = 0; i <= words2.Length - 2; i++)
            {
                if (i % 2 == 0)
                {
                    decimal valoresHora;
                    valoresHora = decimal.Parse(words2[i], CultureInfo.InvariantCulture);
                    data2.Add(valoresHora);
                }
            }
            DateTime inicialHora2 = startDate.AddHours(2); //ACTUALIZAR HORA
            foreach (var valor in data2)
            {

                HistValorMedida valorcito = new HistValorMedida
                {
                    calidadMedida = calidadMedida,
                    valorMedida = (double)valor,
                    horaMedida = inicialHora2,
                    idRegistroMedida = idRegistroAGuardar,
                    activo = true,
                    esHistorico = false,
                    fechaHistorico = null
                };

                _context.HistValorMedida.Add(valorcito);
                _context.SaveChanges();
                inicialHora2 = inicialHora2.AddMinutes(5);
            }

            //03
            string[] words3 = phrase3.Split(';');
            List<decimal> data3 = new List<decimal>();
            for (var i = 0; i <= words3.Length - 2; i++)
            {
                if (i % 2 == 0)
                {
                    decimal valoresHora;
                    valoresHora = decimal.Parse(words3[i], CultureInfo.InvariantCulture);
                    data3.Add(valoresHora);
                }
            }
            DateTime inicialHora3 = startDate.AddHours(3); //ACTUALIZAR HORA
            foreach (var valor in data3)
            {

                HistValorMedida valorcito = new HistValorMedida
                {
                    calidadMedida = calidadMedida,
                    valorMedida = (double)valor,
                    horaMedida = inicialHora3,
                    idRegistroMedida = idRegistroAGuardar,
                    activo = true,
                    esHistorico = false,
                    fechaHistorico = null
                };

                _context.HistValorMedida.Add(valorcito);
                _context.SaveChanges();
                inicialHora3 = inicialHora3.AddMinutes(5);
            }

            //04
            string[] words4 = phrase4.Split(';');
            List<decimal> data4 = new List<decimal>();
            for (var i = 0; i <= words4.Length - 2; i++)
            {
                if (i % 2 == 0)
                {
                    decimal valoresHora;
                    valoresHora = decimal.Parse(words4[i], CultureInfo.InvariantCulture);
                    data4.Add(valoresHora);
                }
            }
            DateTime inicialHora4 = startDate.AddHours(4); //ACTUALIZAR HORA
            foreach (var valor in data4)
            {

                HistValorMedida valorcito = new HistValorMedida
                {
                    calidadMedida = calidadMedida,
                    valorMedida = (double)valor,
                    horaMedida = inicialHora4,
                    idRegistroMedida = idRegistroAGuardar,
                    activo = true,
                    esHistorico = false,
                    fechaHistorico = null
                };

                _context.HistValorMedida.Add(valorcito);
                _context.SaveChanges();
                inicialHora4 = inicialHora4.AddMinutes(5);
            }

            //05
            string[] words5 = phrase5.Split(';');
            List<decimal> data5 = new List<decimal>();
            for (var i = 0; i <= words5.Length - 2; i++)
            {
                if (i % 2 == 0)
                {
                    decimal valoresHora;
                    valoresHora = decimal.Parse(words5[i], CultureInfo.InvariantCulture);
                    data5.Add(valoresHora);
                }
            }
            DateTime inicialHora5 = startDate.AddHours(5); //ACTUALIZAR HORA
            foreach (var valor in data5)
            {

                HistValorMedida valorcito = new HistValorMedida
                {
                    calidadMedida = calidadMedida,
                    valorMedida = (double)valor,
                    horaMedida = inicialHora5,
                    idRegistroMedida = idRegistroAGuardar,
                    activo = true,
                    esHistorico = false,
                    fechaHistorico = null
                };

                _context.HistValorMedida.Add(valorcito);
                _context.SaveChanges();
                inicialHora5 = inicialHora5.AddMinutes(5);
            }

            //06
            string[] words6 = phrase6.Split(';');
            List<decimal> data6 = new List<decimal>();
            for (var i = 0; i <= words6.Length - 2; i++)
            {
                if (i % 2 == 0)
                {
                    decimal valoresHora;
                    valoresHora = decimal.Parse(words6[i], CultureInfo.InvariantCulture);
                    data6.Add(valoresHora);
                }
            }
            DateTime inicialHora6 = startDate.AddHours(6); //ACTUALIZAR HORA
            foreach (var valor in data6)
            {

                HistValorMedida valorcito = new HistValorMedida
                {
                    calidadMedida = calidadMedida,
                    valorMedida = (double)valor,
                    horaMedida = inicialHora6,
                    idRegistroMedida = idRegistroAGuardar,
                    activo = true,
                    esHistorico = false,
                    fechaHistorico = null
                };

                _context.HistValorMedida.Add(valorcito);
                _context.SaveChanges();
                inicialHora6 = inicialHora6.AddMinutes(5);
            }

            //07
            string[] words7 = phrase7.Split(';');
            List<decimal> data7 = new List<decimal>();
            for (var i = 0; i <= words7.Length - 2; i++)
            {
                if (i % 2 == 0)
                {
                    decimal valoresHora;
                    valoresHora = decimal.Parse(words7[i], CultureInfo.InvariantCulture);
                    data7.Add(valoresHora);
                }
            }
            DateTime inicialHora7 = startDate.AddHours(7); //ACTUALIZAR HORA
            foreach (var valor in data7)
            {

                HistValorMedida valorcito = new HistValorMedida
                {
                    calidadMedida = calidadMedida,
                    valorMedida = (double)valor,
                    horaMedida = inicialHora7,
                    idRegistroMedida = idRegistroAGuardar,
                    activo = true,
                    esHistorico = false,
                    fechaHistorico = null
                };

                _context.HistValorMedida.Add(valorcito);
                _context.SaveChanges();
                inicialHora7 = inicialHora7.AddMinutes(5);
            }

            //08
           
            string[] words8 = phrase8.Split(';');
            List<decimal> data8 = new List<decimal>();
            for (var i = 0; i <= words8.Length - 2; i++)
            {
                if (i % 2 == 0)
                {
                    decimal valoresHora;
                    valoresHora = decimal.Parse(words8[i], CultureInfo.InvariantCulture);
                    data8.Add(valoresHora);
                }
            }
            DateTime inicialHora8 = startDate.AddHours(8); //ACTUALIZAR HORA
            foreach (var valor in data8)
            {

                HistValorMedida valorcito = new HistValorMedida
                {
                    calidadMedida = calidadMedida,
                    valorMedida = (double)valor,
                    horaMedida = inicialHora8,
                    idRegistroMedida = idRegistroAGuardar,
                    activo = true,
                    esHistorico = false,
                    fechaHistorico = null
                };

                _context.HistValorMedida.Add(valorcito);
                _context.SaveChanges();
                inicialHora8 = inicialHora8.AddMinutes(5);
            }

            //09

            string[] words9 = phrase9.Split(';');
            List<decimal> data9 = new List<decimal>();
            for (var i = 0; i <= words9.Length - 2; i++)
            {
                if (i % 2 == 0)
                {
                    decimal valoresHora;
                    valoresHora = decimal.Parse(words9[i], CultureInfo.InvariantCulture);
                    data9.Add(valoresHora);
                }
            }
            DateTime inicialHora9 = startDate.AddHours(9); //ACTUALIZAR HORA
            foreach (var valor in data9)
            {

                HistValorMedida valorcito = new HistValorMedida
                {
                    calidadMedida = calidadMedida,
                    valorMedida = (double)valor,
                    horaMedida = inicialHora9,
                    idRegistroMedida = idRegistroAGuardar,
                    activo = true,
                    esHistorico = false,
                    fechaHistorico = null
                };

                _context.HistValorMedida.Add(valorcito);
                _context.SaveChanges();
                inicialHora9 = inicialHora9.AddMinutes(5);
            }

            //10

            string[] words10 = phrase10.Split(';');
            List<decimal> data10 = new List<decimal>();
            for (var i = 0; i <= words10.Length - 2; i++)
            {
                if (i % 2 == 0)
                {
                    decimal valoresHora;
                    valoresHora = decimal.Parse(words10[i], CultureInfo.InvariantCulture);
                    data10.Add(valoresHora);
                }
            }
            DateTime inicialHora10 = startDate.AddHours(10); //ACTUALIZAR HORA
            foreach (var valor in data10)
            {

                HistValorMedida valorcito = new HistValorMedida
                {
                    calidadMedida = calidadMedida,
                    valorMedida = (double)valor,
                    horaMedida = inicialHora10,
                    idRegistroMedida = idRegistroAGuardar,
                    activo = true,
                    esHistorico = false,
                    fechaHistorico = null
                };

                _context.HistValorMedida.Add(valorcito);
                _context.SaveChanges();
                inicialHora10 = inicialHora10.AddMinutes(5);
            }

            //11
           
            string[] words11 = phrase11.Split(';');
            List<decimal> data11 = new List<decimal>();
            for (var i = 0; i <= words11.Length - 2; i++)
            {
                if (i % 2 == 0)
                {
                    decimal valoresHora;
                    valoresHora = decimal.Parse(words11[i], CultureInfo.InvariantCulture);
                    data11.Add(valoresHora);
                }
            }
            DateTime inicialHora11 = startDate.AddHours(11); //ACTUALIZAR HORA
            foreach (var valor in data11)
            {

                HistValorMedida valorcito = new HistValorMedida
                {
                    calidadMedida = calidadMedida,
                    valorMedida = (double)valor,
                    horaMedida = inicialHora11,
                    idRegistroMedida = idRegistroAGuardar,
                    activo = true,
                    esHistorico = false,
                    fechaHistorico = null
                };

                _context.HistValorMedida.Add(valorcito);
                _context.SaveChanges();
                inicialHora11 = inicialHora11.AddMinutes(5);
            }

            //12

            string[] words12 = phrase12.Split(';');
            List<decimal> data12 = new List<decimal>();
            for (var i = 0; i <= words12.Length - 2; i++)
            {
                if (i % 2 == 0)
                {
                    decimal valoresHora;
                    valoresHora = decimal.Parse(words12[i], CultureInfo.InvariantCulture);
                    data12.Add(valoresHora);
                }
            }
            DateTime inicialHora12 = startDate.AddHours(12); //ACTUALIZAR HORA
            foreach (var valor in data12)
            {

                HistValorMedida valorcito = new HistValorMedida
                {
                    calidadMedida = calidadMedida,
                    valorMedida = (double)valor,
                    horaMedida = inicialHora12,
                    idRegistroMedida = idRegistroAGuardar,
                    activo = true,
                    esHistorico = false,
                    fechaHistorico = null
                };

                _context.HistValorMedida.Add(valorcito);
                _context.SaveChanges();
                inicialHora12 = inicialHora12.AddMinutes(5);
            }

            //13
            
            string[] words13 = phrase13.Split(';');
            List<decimal> data13 = new List<decimal>();
            for (var i = 0; i <= words13.Length - 2; i++)
            {
                if (i % 2 == 0)
                {
                    decimal valoresHora;
                    valoresHora = decimal.Parse(words13[i], CultureInfo.InvariantCulture);
                    data13.Add(valoresHora);
                }
            }
            DateTime inicialHora13 = startDate.AddHours(13); //ACTUALIZAR HORA
            foreach (var valor in data13)
            {

                HistValorMedida valorcito = new HistValorMedida
                {
                    calidadMedida = calidadMedida,
                    valorMedida = (double)valor,
                    horaMedida = inicialHora13,
                    idRegistroMedida = idRegistroAGuardar,
                    activo = true,
                    esHistorico = false,
                    fechaHistorico = null
                };

                _context.HistValorMedida.Add(valorcito);
                _context.SaveChanges();
                inicialHora13 = inicialHora13.AddMinutes(5);
            }

            //14
           
            string[] words14 = phrase14.Split(';');
            List<decimal> data14 = new List<decimal>();
            for (var i = 0; i <= words14.Length - 2; i++)
            {
                if (i % 2 == 0)
                {
                    decimal valoresHora;
                    valoresHora = decimal.Parse(words14[i], CultureInfo.InvariantCulture);
                    data14.Add(valoresHora);
                }
            }
            DateTime inicialHora14 = startDate.AddHours(14); //ACTUALIZAR HORA
            foreach (var valor in data14)
            {

                HistValorMedida valorcito = new HistValorMedida
                {
                    calidadMedida = calidadMedida,
                    valorMedida = (double)valor,
                    horaMedida = inicialHora14,
                    idRegistroMedida = idRegistroAGuardar,
                    activo = true,
                    esHistorico = false,
                    fechaHistorico = null
                };

                _context.HistValorMedida.Add(valorcito);
                _context.SaveChanges();
                inicialHora14 = inicialHora14.AddMinutes(5);
            }

            //15
           
            string[] words15 = phrase15.Split(';');
            List<decimal> data15 = new List<decimal>();
            for (var i = 0; i <= words15.Length - 2; i++)
            {
                if (i % 2 == 0)
                {
                    decimal valoresHora;
                    valoresHora = decimal.Parse(words15[i], CultureInfo.InvariantCulture);
                    data15.Add(valoresHora);
                }
            }
            DateTime inicialHora15 = startDate.AddHours(15); //ACTUALIZAR HORA
            foreach (var valor in data15)
            {

                HistValorMedida valorcito = new HistValorMedida
                {
                    calidadMedida = calidadMedida,
                    valorMedida = (double)valor,
                    horaMedida = inicialHora15,
                    idRegistroMedida = idRegistroAGuardar,
                    activo = true,
                    esHistorico = false,
                    fechaHistorico = null
                };

                _context.HistValorMedida.Add(valorcito);
                _context.SaveChanges();
                inicialHora15 = inicialHora15.AddMinutes(5);
            }

            //16
           
            string[] words16 = phrase16.Split(';');
            List<decimal> data16 = new List<decimal>();
            for (var i = 0; i <= words16.Length - 2; i++)
            {
                if (i % 2 == 0)
                {
                    decimal valoresHora;
                    valoresHora = decimal.Parse(words16[i], CultureInfo.InvariantCulture);
                    data16.Add(valoresHora);
                }
            }
            DateTime inicialHora16 = startDate.AddHours(16); //ACTUALIZAR HORA
            foreach (var valor in data16)
            {

                HistValorMedida valorcito = new HistValorMedida
                {
                    calidadMedida = calidadMedida,
                    valorMedida = (double)valor,
                    horaMedida = inicialHora16,
                    idRegistroMedida = idRegistroAGuardar,
                    activo = true,
                    esHistorico = false,
                    fechaHistorico = null
                };

                _context.HistValorMedida.Add(valorcito);
                _context.SaveChanges();
                inicialHora16 = inicialHora16.AddMinutes(5);
            }

            //17
            
            string[] words17 = phrase17.Split(';');
            List<decimal> data17 = new List<decimal>();
            for (var i = 0; i <= words17.Length - 2; i++)
            {
                if (i % 2 == 0)
                {
                    decimal valoresHora;
                    valoresHora = decimal.Parse(words17[i], CultureInfo.InvariantCulture);
                    data17.Add(valoresHora);
                }
            }
            DateTime inicialHora17 = startDate.AddHours(17); //ACTUALIZAR HORA
            foreach (var valor in data17)
            {

                HistValorMedida valorcito = new HistValorMedida
                {
                    calidadMedida = calidadMedida,
                    valorMedida = (double)valor,
                    horaMedida = inicialHora17,
                    idRegistroMedida = idRegistroAGuardar,
                    activo = true,
                    esHistorico = false,
                    fechaHistorico = null
                };

                _context.HistValorMedida.Add(valorcito);
                _context.SaveChanges();
                inicialHora17 = inicialHora17.AddMinutes(5);
            }

            //18
           
            string[] words18 = phrase18.Split(';');
            List<decimal> data18 = new List<decimal>();
            for (var i = 0; i <= words18.Length - 2; i++)
            {
                if (i % 2 == 0)
                {
                    decimal valoresHora;
                    valoresHora = decimal.Parse(words18[i], CultureInfo.InvariantCulture);
                    data18.Add(valoresHora);
                }
            }
            DateTime inicialHora18 = startDate.AddHours(18); //ACTUALIZAR HORA
            foreach (var valor in data18)
            {

                HistValorMedida valorcito = new HistValorMedida
                {
                    calidadMedida = calidadMedida,
                    valorMedida = (double)valor,
                    horaMedida = inicialHora18,
                    idRegistroMedida = idRegistroAGuardar,
                    activo = true,
                    esHistorico = false,
                    fechaHistorico = null
                };

                _context.HistValorMedida.Add(valorcito);
                _context.SaveChanges();
                inicialHora18 = inicialHora18.AddMinutes(5);
            }

            //19
           
            string[] words19 = phrase19.Split(';');
            List<decimal> data19 = new List<decimal>();
            for (var i = 0; i <= words19.Length - 2; i++)
            {
                if (i % 2 == 0)
                {
                    decimal valoresHora;
                    valoresHora = decimal.Parse(words19[i], CultureInfo.InvariantCulture);
                    data19.Add(valoresHora);
                }
            }
            DateTime inicialHora19 = startDate.AddHours(19); //ACTUALIZAR HORA
            foreach (var valor in data19)
            {

                HistValorMedida valorcito = new HistValorMedida
                {
                    calidadMedida = calidadMedida,
                    valorMedida = (double)valor,
                    horaMedida = inicialHora19,
                    idRegistroMedida = idRegistroAGuardar,
                    activo = true,
                    esHistorico = false,
                    fechaHistorico = null
                };

                _context.HistValorMedida.Add(valorcito);
                _context.SaveChanges();
                inicialHora19 = inicialHora19.AddMinutes(5);
            }

            //20
           
            string[] words20 = phrase20.Split(';');
            List<decimal> data20 = new List<decimal>();
            for (var i = 0; i <= words20.Length - 2; i++)
            {
                if (i % 2 == 0)
                {
                    decimal valoresHora;
                    valoresHora = decimal.Parse(words20[i], CultureInfo.InvariantCulture);
                    data20.Add(valoresHora);
                }
            }
            DateTime inicialHora20 = startDate.AddHours(20); //ACTUALIZAR HORA
            foreach (var valor in data20)
            {

                HistValorMedida valorcito = new HistValorMedida
                {
                    calidadMedida = calidadMedida,
                    valorMedida = (double)valor,
                    horaMedida = inicialHora20,
                    idRegistroMedida = idRegistroAGuardar,
                    activo = true,
                    esHistorico = false,
                    fechaHistorico = null
                };

                _context.HistValorMedida.Add(valorcito);
                _context.SaveChanges();
                inicialHora20 = inicialHora20.AddMinutes(5);
            }

            //21
            
            string[] words21 = phrase21.Split(';');
            List<decimal> data21 = new List<decimal>();
            for (var i = 0; i <= words21.Length - 2; i++)
            {
                if (i % 2 == 0)
                {
                    decimal valoresHora;
                    valoresHora = decimal.Parse(words21[i], CultureInfo.InvariantCulture);
                    data21.Add(valoresHora);
                }
            }
            DateTime inicialHora21 = startDate.AddHours(21); //ACTUALIZAR HORA
            foreach (var valor in data21)
            {

                HistValorMedida valorcito = new HistValorMedida
                {
                    calidadMedida = calidadMedida,
                    valorMedida = (double)valor,
                    horaMedida = inicialHora21,
                    idRegistroMedida = idRegistroAGuardar,
                    activo = true,
                    esHistorico = false,
                    fechaHistorico = null
                };

                _context.HistValorMedida.Add(valorcito);
                _context.SaveChanges();
                inicialHora21 = inicialHora21.AddMinutes(5);
            }

            //22
            
            string[] words22 = phrase22.Split(';');
            List<decimal> data22 = new List<decimal>();
            for (var i = 0; i <= words22.Length - 2; i++)
            {
                if (i % 2 == 0)
                {
                    decimal valoresHora;
                    valoresHora = decimal.Parse(words22[i], CultureInfo.InvariantCulture);
                    data22.Add(valoresHora);
                }
            }
            DateTime inicialHora22 = startDate.AddHours(22); //ACTUALIZAR HORA
            foreach (var valor in data22)
            {

                HistValorMedida valorcito = new HistValorMedida
                {
                    calidadMedida = calidadMedida,
                    valorMedida = (double)valor,
                    horaMedida = inicialHora22,
                    idRegistroMedida = idRegistroAGuardar,
                    activo = true,
                    esHistorico = false,
                    fechaHistorico = null
                };

                _context.HistValorMedida.Add(valorcito);
                _context.SaveChanges();
                inicialHora22 = inicialHora22.AddMinutes(5);
            }

            //23
            
            string[] words23 = phrase23.Split(';');
            List<decimal> data23 = new List<decimal>();
            for (var i = 0; i <= words23.Length - 2; i++)
            {
                if (i % 2 == 0)
                {
                    decimal valoresHora;
                    valoresHora = decimal.Parse(words23[i], CultureInfo.InvariantCulture);
                    data23.Add(valoresHora);
                }
            }
            DateTime inicialHora23 = startDate.AddHours(23); //ACTUALIZAR HORA
            foreach (var valor in data23)
            {

                HistValorMedida valorcito = new HistValorMedida
                {
                    calidadMedida = calidadMedida,
                    valorMedida = (double)valor,
                    horaMedida = inicialHora23,
                    idRegistroMedida = idRegistroAGuardar,
                    activo = true,
                    esHistorico = false,
                    fechaHistorico = null
                };

                _context.HistValorMedida.Add(valorcito);
                _context.SaveChanges();
                inicialHora23 = inicialHora23.AddMinutes(5);
            }
            #endregion
            return true;
        }
        #endregion
        #region IQueryables
        private IQueryable<EquipoVM> consultaEquipo(int idEquipo)
        {
            IQueryable<EquipoVM> equipVM = (from equipo in _context.HistEquipo
                                            join estacion in _context.HistEstacion
                                                 on equipo.idEstacion equals estacion.idEstacion
                                            where equipo.idEquipo == idEquipo && equipo.activo == true
                                            select new EquipoVM
                                            {
                                                idEquipo = equipo.idEquipo,
                                                idEstacion = equipo.idEstacion,
                                                nombreEquipo = equipo.nombreEquipo,
                                                nombreEstacion = estacion.nombreEstacion,
                                                activo = true,
                                                idTipoEquipo = equipo.idTipoEquipo,
                                            });
            return equipVM;
        }

        private IQueryable<EquipoVM> consultaEquipo(string nombreEquipo, int idEstacion)
        {
            IQueryable<EquipoVM> equipVM = (from equipo in _context.HistEquipo
                                            join estacion in _context.HistEstacion
                                                 on equipo.idEstacion equals estacion.idEstacion
                                            where equipo.nombreEquipo == nombreEquipo && equipo.activo == true && equipo.idEstacion == idEstacion
                                            select new EquipoVM
                                            {
                                                idEquipo = equipo.idEquipo,
                                                idEstacion = equipo.idEstacion,
                                                nombreEquipo = equipo.nombreEquipo,
                                                nombreEstacion = estacion.nombreEstacion,
                                                activo = true,
                                                idTipoEquipo = equipo.idTipoEquipo,
                                            });
            return equipVM;
        }
        private IQueryable<HistRegistroMedida> registros()
        {
            IQueryable<HistRegistroMedida> tmpRegistros = _context.HistRegistroMedida.Where(x => x.activo == true);

            return tmpRegistros.Distinct();
        }
        private IQueryable<HistValorMedida> valores(int idRegistro)
        {
            IQueryable<HistValorMedida> tmpValores = _context.HistValorMedida.Where(x => x.activo == true && x.idRegistroMedida == idRegistro);
            return tmpValores.Distinct();
        }
        private IQueryable<EquipoVM> equipoQuery (int idEstacion, int idTipoEquipo)
        {
            var consultaEquipos = (from equipo in _context.HistEquipo
                                   join tipoequipo in _context.HistTipoEquipo
                                     on equipo.idTipoEquipo equals tipoequipo.idTipoEquipo
                                   join estacion in _context.HistEstacion
                                     on equipo.idEstacion equals estacion.idEstacion
                                   where equipo.idEstacion == idEstacion && equipo.activo == true && equipo.idTipoEquipo == idTipoEquipo && estacion.activo == true
                                   select new EquipoVM
                                   {
                                       idEquipo = equipo.idEquipo,
                                       idEstacion = equipo.idEstacion,
                                       nombreEquipo = equipo.nombreEquipo,
                                       nombreEstacion = estacion.nombreEstacion,
                                       activo = true,
                                       idTipoEquipo = equipo.idTipoEquipo,

                                   });
            return consultaEquipos.Distinct();
        }
        private IQueryable<EquipoVM> equipoTensionQuery(int idEstacion, int idTipoEquipo, int idTension)
        {
            var consultaEquipos = (from equipo in _context.HistEquipo
                                   join tipoequipo in _context.HistTipoEquipo
                                     on equipo.idTipoEquipo equals tipoequipo.idTipoEquipo
                                   join estacion in _context.HistEstacion
                                     on equipo.idEstacion equals estacion.idEstacion
                                   join puntoscadas in _context.HistPuntoScada
                                     on equipo.idEquipo equals puntoscadas.idEquipo
                                   where equipo.idEstacion == idEstacion && equipo.activo == true
                                        && equipo.idTipoEquipo == idTipoEquipo && estacion.activo == true
                                        && puntoscadas.activo == true && puntoscadas.idTension == idTension
                                   select new EquipoVM
                                   {
                                       idEquipo = equipo.idEquipo,
                                       idEstacion = equipo.idEstacion,
                                       nombreEquipo = equipo.nombreEquipo,
                                       nombreEstacion = estacion.nombreEstacion,
                                       activo = true,
                                       idTipoEquipo = equipo.idTipoEquipo,
                                   });
            return consultaEquipos.Distinct();
        }
        private IQueryable<EquipoVM> equipoTensionMedidaQuery(int idEstacion, int idTipoEquipo, int idTension, int idTipoMedida)
        {
            var consultaEquipos = (from equipo in _context.HistEquipo
                                   join tipoequipo in _context.HistTipoEquipo
                                     on equipo.idTipoEquipo equals tipoequipo.idTipoEquipo
                                   join estacion in _context.HistEstacion
                                     on equipo.idEstacion equals estacion.idEstacion
                                   join puntoscadas in _context.HistPuntoScada
                                     on equipo.idEquipo equals puntoscadas.idEquipo
                                   join registrosmedidas in _context.HistRegistroMedida
                                     on puntoscadas.idPuntoScada equals registrosmedidas.idPuntoScada
                                   where equipo.idEstacion == idEstacion && equipo.activo == true
                                        && equipo.idTipoEquipo == idTipoEquipo && estacion.activo == true
                                        && puntoscadas.activo == true && puntoscadas.idTension == idTension
                                        && registrosmedidas.activo == true && registrosmedidas.idTipoMedida == idTipoMedida
                                   select new EquipoVM
                                   {
                                       idEquipo = equipo.idEquipo,
                                       idEstacion = equipo.idEstacion,
                                       nombreEquipo = equipo.nombreEquipo,
                                       nombreEstacion = estacion.nombreEstacion,
                                       activo = true,
                                       idTipoEquipo = equipo.idTipoEquipo,
                                   });
            return consultaEquipos.Distinct();
        }
        private IQueryable<RegistroMedidaVM> registroQuery(int idTipoMedida, DateTime from, DateTime to, int idEquipo)
        {
            var consultaRegistros = (from equipo in _context.HistEquipo
                                        join tipoequipo in _context.HistTipoEquipo
                                            on equipo.idTipoEquipo equals tipoequipo.idTipoEquipo
                                        join estacion in _context.HistEstacion
                                            on equipo.idEstacion equals estacion.idEstacion
                                        join puntoscada in _context.HistPuntoScada
                                            on equipo.idEquipo equals puntoscada.idEquipo into puntoequipo
                                        from pq in puntoequipo.DefaultIfEmpty()
                                        join registros in _context.HistRegistroMedida
                                            on pq.idPuntoScada equals registros.idPuntoScada
                                        join tipomedida in _context.HistTipoMedida
                                            on registros.idTipoMedida equals tipomedida.idTipoMedida
                                        where registros.idTipoMedida == idTipoMedida
                                            && registros.fInicio >= @from && registros.fFin <= @to && registros.activo == true
                                            && equipo.idEquipo == idEquipo
                                        select new RegistroMedidaVM
                                        {
                                            idRegistroMedida = registros.idRegistroMedida,
                                            fInicio = @from,
                                            fFin = @to,
                                            idPuntoScada = registros.idPuntoScada,
                                            nroScada = pq.nroPuntoScada,
                                            lado=pq.lado,
                                            idEquipo = equipo.idEquipo,
                                            idTension = pq.idTension,
                                            idEstadoTiempo = registros.idEstadoTiempo,
                                            idTipoMedida = tipomedida.idTipoMedida,
                                            descripcionMedida = tipomedida.medida,
                                            unidadMedida = tipomedida.unidadMedida,
                                            siglaMedida = tipomedida.siglaMedida,
                                            activo = true,
                                            Valores = new List<ValorVM>() { }
                                        });
            return consultaRegistros.Distinct();
        }
        private IQueryable<RegistroMedidaVM> registroQueryTension(int idTipoMedida, DateTime from, DateTime to, int idEquipo, int tension)
        {
            var consultaRegistros = (from registro in _context.HistRegistroMedida
                                     join puntoS in _context.HistPuntoScada
                                         on registro.idPuntoScada equals puntoS.idPuntoScada
                                     join tipomedida in _context.HistTipoMedida
                                      on registro.idTipoMedida equals tipomedida.idTipoMedida
                                     where registro.fInicio >= @from && registro.fFin <= @to
                                     && registro.activo == true && puntoS.idTension == tension
                                     && puntoS.idEquipo == idEquipo && registro.idTipoMedida == idTipoMedida
                                     select new RegistroMedidaVM
                                     {
                                         idRegistroMedida = registro.idRegistroMedida,
                                         fInicio = @from,
                                         fFin = @to,
                                         idPuntoScada = registro.idPuntoScada,
                                         idEquipo = puntoS.idEquipo,
                                         idTension = puntoS.idTension,
                                         idEstadoTiempo = registro.idEstadoTiempo,
                                         idTipoMedida = tipomedida.idTipoMedida,
                                         descripcionMedida = tipomedida.medida,
                                         unidadMedida = tipomedida.unidadMedida,
                                         siglaMedida = tipomedida.siglaMedida,
                                         activo = true,
                                         Valores = new List<ValorVM>() { }
                                     });

            return consultaRegistros.Distinct();

        }
        private IQueryable<ValorVM> valoresQuery(int idRegistro, DateTime? fInicio, DateTime? fFin, int? idequipo)
        {
            var consultaValores = (from valor in _context.HistValorMedida
                           where valor.idRegistroMedida == idRegistro &&
                           valor.horaMedida >= fInicio &&
                            valor.horaMedida <= fFin && valor.activo ==true
                           select new ValorVM
                           {
                               valorMedida = valor.valorMedida,
                               horaMedida = valor.horaMedida,
                               idValorMedida = valor.idValorMedida,
                               idTrafo = idequipo,
                               activo = valor.activo,
                               calidadMedida = valor.calidadMedida
                           });
            return consultaValores.Distinct();

        }
        private IQueryable<ValorVM> valoresTimeQuery(int idRegistro, DateTime? time, int? idEquipo)
        {
            var consultaValores = (from valor in _context.HistValorMedida
                                   where valor.idRegistroMedida == idRegistro &&
                                   valor.horaMedida == time && valor.activo == true
                                   select new ValorVM
                                   {
                                       valorMedida = valor.valorMedida,
                                       horaMedida = valor.horaMedida,
                                       idValorMedida = valor.idValorMedida,
                                       idTrafo = idEquipo,
                                       activo = valor.activo,
                                       calidadMedida = valor.calidadMedida
                                   });
            return consultaValores.Distinct();

        }
        private IQueryable<ValorVM> valoresQuery(DateTime? fInicio, DateTime? fFin, int? idTipoEquipo, int? idTipoMedida, int? idEstacion)
        {
            IQueryable<ValorVM> listadoValores = (from estacion in _context.HistEstacion
                                                  join equipo in _context.HistEquipo on estacion.idEstacion equals equipo.idEstacion
                                                  join tipoequipo in _context.HistTipoEquipo on equipo.idTipoEquipo equals tipoequipo.idTipoEquipo
                                                  join puntoscada in _context.HistPuntoScada
                                                        on equipo.idEquipo equals puntoscada.idEquipo into puntoequipo
                                                  from pq in puntoequipo.DefaultIfEmpty()
                                                  join registros in _context.HistRegistroMedida
                                                      on pq.idPuntoScada equals registros.idPuntoScada
                                                  join valoreshist in _context.HistValorMedida on registros.idRegistroMedida equals valoreshist.idRegistroMedida
                                                  where registros.idTipoMedida == idTipoMedida
                                                      && registros.fInicio >= fInicio && registros.fFin <= fFin && registros.activo == true
                                                      && equipo.idTipoEquipo == idTipoEquipo && valoreshist.activo == true && estacion.idEstacion == idEstacion
                                                  select new ValorVM
                                                  {
                                                      idValorMedida = valoreshist.idValorMedida,
                                                      valorMedida = valoreshist.valorMedida,
                                                      horaMedida = valoreshist.horaMedida,
                                                      activo = true,
                                                  }).Distinct();
            return listadoValores;
        }
        private IQueryable<ValorVM> valoresQuery(DateTime? fInicio, DateTime? fFin, int? idEquipo, int? idTipoMedida)
        {
            IQueryable<ValorVM> listadoValores = (from estacion in _context.HistEstacion
                                                  join equipo in _context.HistEquipo on estacion.idEstacion equals equipo.idEstacion
                                                  join tipoequipo in _context.HistTipoEquipo on equipo.idTipoEquipo equals tipoequipo.idTipoEquipo
                                                  join puntoscada in _context.HistPuntoScada
                                                        on equipo.idEquipo equals puntoscada.idEquipo into puntoequipo
                                                  from pq in puntoequipo.DefaultIfEmpty()
                                                  join registros in _context.HistRegistroMedida
                                                      on pq.idPuntoScada equals registros.idPuntoScada
                                                  join valoreshist in _context.HistValorMedida on registros.idRegistroMedida equals valoreshist.idRegistroMedida
                                                  where registros.idTipoMedida == idTipoMedida
                                                      && registros.fInicio >= fInicio && registros.fFin <= fFin && registros.activo == true
                                                      && equipo.idEquipo == idEquipo && valoreshist.activo == true
                                                  select new ValorVM
                                                  {
                                                      idValorMedida = valoreshist.idValorMedida,
                                                      valorMedida = valoreshist.valorMedida,
                                                      horaMedida = valoreshist.horaMedida,
                                                      activo = true,
                                                  }).Distinct();
            return listadoValores;
        }
        private IQueryable<MedidaVM> MedidaInfo (int idMedida)
        {
            var medidaInfo = (from medida in _context.HistTipoMedida
                              where medida.idTipoMedida == idMedida && medida.activo == true
                              select new MedidaVM
                              {
                                  idTipoMedida = medida.idTipoMedida,
                                  medida = medida.medida,
                                  unidadMedida = medida.unidadMedida,
                                  siglaMedida = medida.siglaMedida,
                                  activo = medida.activo
                              }).Distinct();

            return medidaInfo;
        }
        private IQueryable<RegistroMedidaVM> registrosListQuery(int idEquipo, int idTension, int idMedida, DateTime from, DateTime to)
        {
            var registrosList = (from punto in _context.HistPuntoScada
                                 join registros in _context.HistRegistroMedida on punto.idPuntoScada equals registros.idPuntoScada
                                 where punto.idEquipo == idEquipo && punto.activo == true
                                   && punto.idTension == idTension && registros.idTipoMedida == idMedida
                                   && registros.activo == true && registros.fInicio >= @from && registros.fFin <= @to
                                 select new RegistroMedidaVM
                                 {
                                     idRegistroMedida = registros.idRegistroMedida,
                                     fFin = registros.fFin,
                                     fInicio = registros.fInicio,
                                     idPuntoScada = punto.idPuntoScada,
                                     nroScada = punto.nroPuntoScada,
                                     activo = registros.activo,
                                     idEquipo = punto.idEquipo,
                                     idTension = punto.idTension,
                                     lado = punto.lado,
                                 });
            return registrosList.Distinct();
        }
        #endregion
        #region Métodos Generales
        //Filtros de tiempo
        private List<double?> filterTimes(DateTime from, DateTime to,int idEquipo, int idRegistro)
        {
            var tmpValores = (valoresQuery(idRegistro, from, to, idEquipo)).ToList();
            List<double?> valoresEncontrados = new List<double?>();
            {
                TimeSpan interval = to - from;
                if (interval.TotalDays <4) //1 a 3 días
                {
                    for (DateTime i = from; i <= to; i = i.AddMinutes(5))
                    {
                        var exist = tmpValores.Where(y => y.horaMedida == i).FirstOrDefault();
                        double? valor = exist?.valorMedida;
                        if (valor != null)
                        {
                            valoresEncontrados.Add(Math.Round((double)valor, 2, MidpointRounding.AwayFromZero));
                        }
                        else
                        {
                            valoresEncontrados.Add(null);
                        }
                    }
                }
                
            };
                
            return valoresEncontrados;
        }
        private List<double?> filterTimes(DateTime from, DateTime to, int idEquipo, List<RegistroMedidaVM> registros)
        {
            List<double?> valoresEncontrados = new List<double?>();
            foreach(var registro in registros)
            {
                var tmpValores = (valoresQuery(registro.idRegistroMedida, from, to, idEquipo)).ToList();
                {
                    TimeSpan interval = to - from;
                    if (interval.TotalDays < 4) //1 a 3 días
                    {

                        for (DateTime i = (DateTime)registro.fInicio.Value; i <= (DateTime)registro.fFin.Value; i = i.AddMinutes(5))
                        {
                            var exist = tmpValores.Where(y => y.horaMedida == i).FirstOrDefault();
                            double? valor = exist?.valorMedida;
                            if (valor != null)
                            {
                                valoresEncontrados.Add(Math.Round((double)valor, 2, MidpointRounding.AwayFromZero));
                            }
                            else
                            {
                                valoresEncontrados.Add(null);
                            }
                        }
                    }
                };

            }

            return valoresEncontrados;
        }
        private List<string> filterTimes(DateTime from, DateTime to)
        {
            List<string> labelTime = new List<string>();
            {
                TimeSpan interval = to - from;
                if (interval.TotalDays <= 3) //1 día a 3 días
                {
                    //cada 5 min
                    labelTime = label(from,to, (double) 5);

                }
               
            };
            return labelTime;
        }
        //label: este método trae las etiquetas horarias del gráfico
        private List<string> label(DateTime min, DateTime max, double interval) 
        {
            List<string> hours = new List<string>();

            var horaCero = (DateTime.Parse("00:00:00"));
            for (DateTime i = min; i < max; i = i.AddMinutes(interval))
            {

                if (i.TimeOfDay.Ticks == 0)
                {
                    var fechaHora = (i.ToString("g", CultureInfo.GetCultureInfo("es-AR"))).Split(" "); //g Format Specifier   es-ES Culture  31/10/2008 17:04
                    var fecha = fechaHora[0].Split("/");
                    var hora = fechaHora[1];
                    var stringHora = fecha[0] + '/' + fecha[1] + ' ' + hora; //quedaría 31/10 17:04

                    hours.Add(stringHora);
                }
                else
                {
                    var fechaHora = (i.ToString("g", CultureInfo.GetCultureInfo("es-AR"))).Split(" "); //g Format Specifier   es-ES Culture  31/10/2008 17:04
                    var hora = fechaHora[1];
                    var stringHora = hora; //quedaría sólo 17:04

                    hours.Add(stringHora);
                }

            }

            return hours;
        }
        [Obsolete]
        private DatasetVM GetDatasetV2(DateTime from, DateTime to, int idRegistro, int idEquipo)
        {

            var equipoInfo = _context.HistEquipo.Where(x => x.idEquipo == idEquipo).FirstOrDefault();
            var idPuntoScada = _context.HistRegistroMedida.Where(x => x.idRegistroMedida == idRegistro).Select(x => x.idPuntoScada).FirstOrDefault();
            var puntoScadaInfo = _context.HistPuntoScada.Where(x => x.idEquipo == idEquipo && x.idPuntoScada == idPuntoScada).FirstOrDefault();
            var tensionInfo = _context.HistTension.Where(x => x.idTension == puntoScadaInfo.idTension).FirstOrDefault();
            string label;
            if (equipoInfo.idTipoEquipo == 2 || equipoInfo.idTipoEquipo == 4)
            {
                label = equipoInfo.nombreEquipo + " " + tensionInfo.valorTension + " kv";
            }
            else
            {
                label = (puntoScadaInfo.lado != null) ? (equipoInfo.nombreEquipo + " " + "LADO " + puntoScadaInfo.lado) : (equipoInfo.nombreEquipo);
            }

            List<double?> valores = filterTimes(from, to, idEquipo, idRegistro);

            DatasetVM dataset = new DatasetVM
            {
                label = label,
                data = valores
            };

            return dataset;
        }
        private DatasetVM GetDatasetV3(DateTime from, DateTime to, int idEquipo, List<RegistroMedidaVM> registrosList)
        {
            var equipoInfo = _context.HistEquipo.Where(x => x.idEquipo == idEquipo).FirstOrDefault();

            //voy a suponer que siempre viene el mismo punto en los registros, porque los dividi por si tienen lado o no
            var idPuntoScada = registrosList.Select(x => x.idPuntoScada).FirstOrDefault();
            var puntoScadaInfo = _context.HistPuntoScada.Where(x => x.idEquipo == idEquipo && x.idPuntoScada == idPuntoScada).FirstOrDefault();

            var tensionInfo = _context.HistTension.Where(x => x.idTension == puntoScadaInfo.idTension).FirstOrDefault();
            string label;
            if (equipoInfo.idTipoEquipo == 2 || equipoInfo.idTipoEquipo == 4)
            {
                label = equipoInfo.nombreEquipo + " " + tensionInfo.valorTension + " kv";
            }
            else
            {
                label = (puntoScadaInfo.lado != null && puntoScadaInfo.lado != string.Empty) ? (equipoInfo.nombreEquipo + " " + "LADO " + puntoScadaInfo.lado) : (equipoInfo.nombreEquipo);
            }

            List<double?> valores = filterTimes(from, to, idEquipo, registrosList);

            DatasetVM dataset = new DatasetVM
            {
                label = label,
                data = valores
            };

            return dataset;
        }
        private List<dataGraficoVM> graficosEquiposXTension(int idEstacion, DateTime from, DateTime to, int idTipoEquipo, int idMedida)
        {
            TimeSpan interval = to - from;
            if(interval.TotalDays > 3) //si son más de 3 días
            {
                to = from.AddDays(2).AddHours(23).AddMinutes(59);
            }
            var tensionList = _context.HistTension.Where(x => x.activo == true).Distinct().ToList();

            List<dataGraficoVM> dataGraficoVMs = new List<dataGraficoVM>();
            int contGraficos = 1;

            var medidaInfo = MedidaInfo(idMedida).FirstOrDefault();

            foreach (var tension in tensionList)
            {
                List<double?> minMaxValues = new List<double?>();

                var tmpEquipos = (equipoQuery(idEstacion, idTipoEquipo)).ToList();
                if (tmpEquipos != null && tmpEquipos.Count > 0)
                {
                    List<DatasetVM> datasets = new List<DatasetVM>();
                    List<DateTime> minMaxDates = new List<DateTime>();
                    foreach (var equipo in tmpEquipos)
                    {
                        var registrosList = registrosListQuery(equipo.idEquipo, tension.idTension, idMedida, from, to).ToList();
                        if (registrosList.Any())
                        {
                            #region min y max dates
                            minMaxDates.Add(registrosList.Min(x => x.fInicio).Value);
                            minMaxDates.Add(registrosList.Max(x => x.fFin).Value);
                            #endregion
                            //Hay equipos que tienen puntos llamados "lados triángulo" por ejemplo:
                            var registrosLado = registrosList.Where(x=>x.lado != null).Distinct().ToList();
                            var registrosSinLado = registrosList.Where(x=>x.lado == null).Distinct().ToList();
                            if (registrosLado.Any())
                            {
                                DatasetVM dataset = GetDatasetV3(from, to, equipo.idEquipo, registrosLado);
                                datasets.Add(dataset);
                            }
                            if (registrosSinLado.Any())
                            {
                                DatasetVM dataset = GetDatasetV3(from, to, equipo.idEquipo, registrosSinLado);
                                datasets.Add(dataset);
                            }
                            foreach (var registro in registrosList)
                            {
                                #region minimos y maximos val
                                var tmpValores = valoresQuery(registro.idRegistroMedida, registro.fInicio, registro.fFin, equipo.idEquipo).ToList();
                                //Busco minimos y maximos
                                if (tmpValores != null && tmpValores.Count > 0)
                                {
                                    if(tmpValores.Min(x => x.valorMedida).HasValue) minMaxValues.Add(Math.Round(tmpValores.Min(x => x.valorMedida).Value, 2, MidpointRounding.AwayFromZero));
                                    if(tmpValores.Max(x => x.valorMedida).HasValue) minMaxValues.Add(Math.Round(tmpValores.Max(x => x.valorMedida).Value, 2, MidpointRounding.AwayFromZero));

                                }
                                #endregion
                            }
                            
                        }
                    }
                    if (datasets.Any())
                    {
                        List<string> labelHours = filterTimes(from, to);
                        dataGraficoVM dataGrafico = new dataGraficoVM
                        {
                            contGraficos = contGraficos,
                            labels = labelHours,
                            datasets = datasets,
                            maxValue = minMaxValues.Max(),
                            minValue = minMaxValues.Min(),
                            labelGrafico = tension.valorTension + " kV",
                        };
                        dataGraficoVMs.Add(dataGrafico);
                        contGraficos++;
                    }
                }
            }
            return dataGraficoVMs;
        }
        private dataGraficoVM graficosLATBarrasXTension(int idEstacion, DateTime from, DateTime to, int idTipoEquipo, int idMedida, int idTension)
        {
            TimeSpan interval = to - from;
            if (interval.TotalDays > 3) //si son más de 3 días
            {
                to = from.AddDays(2).AddHours(23).AddMinutes(59);
            }

            dataGraficoVM dataGraficoVM = new dataGraficoVM();
            List<double?> minMaxValues = new List<double?>();

            var tmpEquipos = (equipoQuery(idEstacion, idTipoEquipo)).ToList();
            if (tmpEquipos != null && tmpEquipos.Count > 0)
            {
                List<DatasetVM> datasets = new List<DatasetVM>();
                List<DateTime> minMaxDates = new List<DateTime>();
                foreach (var equipo in tmpEquipos)
                {
                    var registrosList = registrosListQuery(equipo.idEquipo, idTension, idMedida, from, to).ToList();
                    if (registrosList.Any())
                    {
                        #region min y max dates
                        minMaxDates.Add(registrosList.Min(x => x.fInicio).Value);
                        minMaxDates.Add(registrosList.Max(x => x.fFin).Value);
                        #endregion

                        DatasetVM dataset = GetDatasetV3(from, to, equipo.idEquipo, registrosList);
                        datasets.Add(dataset);
                        foreach (var registro in registrosList)
                        {
                            //DatasetVM dataset = GetDatasetV2(from,to,registro.idRegistroMedida,equipo.idEquipo);
                            //datasets.Add(dataset);
                            #region minimos y maximos val
                            var tmpValores = valoresQuery(registro.idRegistroMedida, registro.fInicio, registro.fFin, equipo.idEquipo).ToList();
                            //Busco minimos y maximos
                            if (tmpValores != null && tmpValores.Count > 0)
                            {
                                minMaxValues.Add(Math.Round((double)(tmpValores.Min(x => x.valorMedida).Value), 2, MidpointRounding.AwayFromZero));
                                minMaxValues.Add(Math.Round((double)(tmpValores.Max(x => x.valorMedida).Value), 2, MidpointRounding.AwayFromZero));
                            }
                            #endregion
                        }
                    }
                }
                if (datasets != null && datasets.Count > 0)
                {
                    dataGraficoVM dataGrafico = new dataGraficoVM
                    {
                        maxValue = minMaxValues.Max(),
                        minValue = minMaxValues.Min(),
                        datasets = datasets,
                    };

                    return dataGrafico;
                }
            }
            return null;
        }
        #endregion
        #region MetodosGraficadores
        //carga de trafos en MVA
        public List<dataGraficoVM> cargaTrafosV2(int idEstacion, DateTime from, DateTime to)
        {
            List<dataGraficoVM> dataGraficoVMs = graficosEquiposXTension(idEstacion, from, to, 1, 3);

            return dataGraficoVMs;
        }
        //carga ATR en MVA
        public List<dataGraficoVM> cargaATRV2(int idEstacion, DateTime from, DateTime to)
        {
            List<dataGraficoVM> dataGraficoVMs = graficosEquiposXTension(idEstacion, from, to, 2002, 3);

            return dataGraficoVMs;
        }
        //tension barras en kV
        public List<dataGraficoVM> tensionBarrasV2(int idEstacion, DateTime from, DateTime to)
        {
            var idMedida = 4;
            var idTipoLATs = 2;
            var idTipoBarras = 4;

            List<dataGraficoVM> dataGraficoVMs = new List<dataGraficoVM>();
            var tensionList = _context.HistTension.Where(x => x.activo == true).Distinct().ToList();
            var medidaInfo = MedidaInfo(idMedida).FirstOrDefault();
            int contGraficos = 1;

            foreach (var tension in tensionList)
            {
                var graficoBarra = graficosLATBarrasXTension(idEstacion, from, to, idTipoBarras, idMedida, tension.idTension);
                var graficoLinea = graficosLATBarrasXTension(idEstacion, from, to, idTipoLATs, idMedida, tension.idTension);
                if (graficoBarra != null || graficoLinea != null)
                {
                    List<DatasetVM> datasets = new List<DatasetVM>();
                    List<double?> minMaxValues = new List<double?>();
                    List<DateTime> minMaxDates = new List<DateTime>();

                    if (graficoBarra != null)
                    {
                        datasets.AddRange(graficoBarra.datasets);
                        minMaxValues.Add(graficoBarra.maxValue);
                        minMaxValues.Add(graficoBarra.minValue);

                    }
                    if (graficoLinea != null)
                    {
                        datasets.AddRange(graficoLinea.datasets);
                        minMaxValues.Add(graficoLinea.minValue);
                        minMaxValues.Add(graficoLinea.maxValue);
                    }

                    List<string> labelHours = filterTimes(from, to);
                    dataGraficoVM dataGrafico = new dataGraficoVM
                    {
                        contGraficos = contGraficos,
                        labels = labelHours,
                        datasets = datasets,
                        maxValue = minMaxValues.Max(),
                        minValue = minMaxValues.Min(),
                        labelGrafico = tension.valorTension + " kV",
                    };
                    dataGraficoVMs.Add(dataGrafico);
                    contGraficos++;

                }
            }
            return dataGraficoVMs;
        }
        //porcentaje carga trafos
        public List<dataGraficoVM> porcentajeCargaTR(int idEstacion, DateTime from, DateTime to)
        {
            List<dataGraficoVM> dataGraficoVMs = graficosEquiposXTension(idEstacion, from, to, 1, 10);

            return dataGraficoVMs;
        }
        //porcentaje carga de autotrafos
        public List<dataGraficoVM> porcentajeCargaATR(int idEstacion, DateTime from, DateTime to)
        {
            List<dataGraficoVM> dataGraficoVMs = graficosEquiposXTension(idEstacion, from, to, 2002, 10);

            return dataGraficoVMs;
        }
        //demanda apilable entre estaciones
        public List<dataGraficoVM> demandaApilable(int idEstPrimera, int? idEstSegunda, DateTime from, DateTime to)
        {
            List<dataGraficoVM> dataGraficoVMs = new List<dataGraficoVM>();
            
            dataGraficoVM dataGrafico1 = demandaTotal(idEstPrimera, from, to);
            dataGraficoVMs.Add(dataGrafico1);
            if (idEstSegunda.HasValue)
            {
                dataGraficoVM dataGrafico2 = demandaTotal((int)idEstSegunda, from, to);
                dataGrafico2.contGraficos += 1;
                dataGraficoVMs.Add(dataGrafico2);
            }

            return dataGraficoVMs;
        }
        //demanda total de una estación
        public dataGraficoVM demandaTotal (int idEstacion, DateTime from, DateTime to)
        {
            var infoEstacion = _context.HistEstacion.Where(x => x.idEstacion == idEstacion && x.activo == true).FirstOrDefault();
            if (infoEstacion != null)
            {
                TimeSpan interval = to - from;
                if (interval.TotalDays > 3) //si son más de 3 días
                {
                    to = from.AddDays(2).AddHours(23).AddMinutes(59);
                }
                int idTipoEq = 0;
                var idTipoMedida = 3;
                var countTrafos = (equipoQuery(idEstacion, 1)).Count();
                var countATR = (equipoQuery(idEstacion, 2002)).Count();
                if (countTrafos != 0)
                {
                    idTipoEq = 1;
                }
                else if (countATR != 0)
                {
                    idTipoEq = 2002;
                }

                if (idTipoEq != 0)
                {
                    var tmpTrafos = (equipoQuery(idEstacion, idTipoEq)).ToList();
                    List<ValorVM>? tmpValues = new List<ValorVM>();
                    List<double?> valores = new List<double?>();

                    foreach(var equipo in tmpTrafos)
                    {
                        var puntoScada = (from eq in _context.HistEquipo
                                          join puntoscada in _context.HistPuntoScada
                                          on equipo.idEquipo equals puntoscada.idEquipo
                                          join registromedida in _context.HistRegistroMedida
                                          on puntoscada.idPuntoScada equals registromedida.idPuntoScada
                                          where eq.idEquipo == equipo.idEquipo && eq.activo == true
                                          && puntoscada.activo == true && registromedida.idTipoMedida == 3
                                          && registromedida.fInicio >= @from && registromedida.fFin <= @to && registromedida.activo == true
                                          select new PuntoScadaVM
                                          {
                                              idPuntoScada = puntoscada.idPuntoScada,
                                              nroScada = puntoscada.nroPuntoScada,
                                              fechaCreacionPunto = puntoscada.fechaCreacionPunto,
                                              fechaFinPunto = puntoscada.fechaFinPunto,
                                              activo = puntoscada.activo,
                                              idEquipo = puntoscada.idEquipo,
                                              idEstacion = eq.idEstacion,
                                              idTension = puntoscada.idTension,
                                              lado = puntoscada.lado,
                                              //nombreEstacion
                                          }).Distinct().OrderByDescending(x => x.idTension).FirstOrDefault();
                        
                        if(puntoScada!= null)
                        {
                            var values = (from valor in _context.HistValorMedida
                                          join registrosmedida in _context.HistRegistroMedida
                                            on valor.idRegistroMedida equals registrosmedida.idRegistroMedida
                                          join puntoscada in _context.HistPuntoScada
                                            on registrosmedida.idPuntoScada equals puntoscada.idPuntoScada

                                        where registrosmedida.idTipoMedida == idTipoMedida
                                            && registrosmedida.fInicio >= @from && registrosmedida.fFin <= @to && registrosmedida.activo == true
                                            && registrosmedida.idPuntoScada == puntoScada.idPuntoScada
                                        select new ValorVM
                                        {
                                            idValorMedida = valor.idValorMedida,
                                            valorMedida = valor.valorMedida,
                                            horaMedida = valor.horaMedida,
                                            activo = true,
                                        }).Distinct().ToList();
                            
                            tmpValues.AddRange(values);
                        }
                        

                                                     
                    }

                    for (DateTime i = from; i <= to; i = i.AddMinutes(5))
                    {
                        var exist = tmpValues.Where(y => y.horaMedida == i).ToList();
                        double? valor = exist?.Sum(x => x.valorMedida).Value;

                        if (valor != null)
                        {
                            valores.Add(Math.Round((double)valor, 2, MidpointRounding.AwayFromZero));
                        }
                        else
                        {
                            valores.Add(null);
                        }
                    }
                    var min = valores.Min();
                    var max = valores.Max();

                    DatasetVM dataset = new DatasetVM
                    {
                        label = "Demanda total E.T. "+infoEstacion.nombreEstacion,
                        data = valores
                    };

                    List<DatasetVM>? data = new List<DatasetVM>();
                    data.Add(dataset);
                    var labelHours = filterTimes(from, to);

                    dataGraficoVM demandaTot = new dataGraficoVM
                    {
                        contGraficos = 1,
                        labels = labelHours,
                        datasets = data,
                        maxValue = max,
                        minValue = min,
                        labelGrafico = "Demanda Total"
                    };

                    return demandaTot;
                }
            }
            return null;
        }
        public dataGraficoVM temperaturaEstacion(int idEstacion, DateTime from, DateTime to)
        {
            var infoEstacion = _context.HistEstacion.Where(x => x.idEstacion == idEstacion && x.activo == true).FirstOrDefault();
            if(infoEstacion != null)
            {
                TimeSpan interval = to - from;
                if (interval.TotalDays > 3) //si son más de 3 días
                {
                    to = from.AddDays(2).AddHours(23).AddMinutes(59);
                }
                int idTipoEq = 1002; //termómetro
                int idTipoMed = 7; //temperatura
                var consultaEquipo = equipoQuery(idEstacion,idTipoEq).FirstOrDefault();
                List<DatasetVM> datasets = new List<DatasetVM>();
                if (consultaEquipo != null)
                {
                    List<DateTime> minMaxDates = new List<DateTime>();
                    List<double?> minMaxValues = new List<double?>();

                    var registrosList = registroQuery(idTipoMed, from, to, consultaEquipo.idEquipo).ToList();
                    if (registrosList.Any())
                    {
                        #region min y max dates
                        minMaxDates.Add(registrosList.Min(x => x.fInicio).Value);
                        minMaxDates.Add(registrosList.Max(x => x.fFin).Value);
                        #endregion
                        DatasetVM dataset = GetDatasetV3(from, to, consultaEquipo.idEquipo, registrosList);
                        datasets.Add(dataset);
                        foreach (var registro in registrosList)
                        {
                            #region minimos y maximos val
                            var tmpValores = valoresQuery(registro.idRegistroMedida, registro.fInicio, registro.fFin, consultaEquipo.idEquipo).ToList();
                            //Busco minimos y maximos
                            if (tmpValores != null && tmpValores.Count > 0)
                            {
                                minMaxValues.Add(Math.Round((double)(tmpValores.Min(x => x.valorMedida).Value), 2, MidpointRounding.AwayFromZero));
                                minMaxValues.Add(Math.Round((double)(tmpValores.Max(x => x.valorMedida).Value), 2, MidpointRounding.AwayFromZero));
                            }
                            #endregion
                        }
                    }
                    //if (datasets.Any())
                    //{
                        List<string> labelHours = filterTimes(from, to);
                        dataGraficoVM dataGrafico = new dataGraficoVM
                        {
                            labels = labelHours,
                            datasets = datasets,
                            maxValue = minMaxValues.Max(),
                            minValue = minMaxValues.Min(),
                            labelGrafico = "Temperatura ambiente E.T. "+ infoEstacion.nombreEstacion,
                        };
                        return dataGrafico;
                    //}
                }
                
            }
            return null;
        }
        #endregion
        #region Tablas
        public dataTablaVM cargaTrafosTabla(int idEstacion, DateTime from, DateTime to, string tension)
        {
            int idTipoEquipo = 1;
            return getTableTrAtr(idEstacion,from,to,tension,idTipoEquipo);
        }
        public dataTablaVM cargaATRsTabla(int idEstacion, DateTime from, DateTime to, string tension)
        {
            int idTipoEquipo = 2002;
            return getTableTrAtr(idEstacion, from, to, tension, idTipoEquipo);
        }
        public List<dataTablaVM> potenciasPQbarrasLineas(int idEstacion, DateTime from, DateTime to)
        {
            //  **-- TABLA GROUPED --** //
            List<dataTablaVM> tablas = new List<dataTablaVM>();
            var tensions = _context.HistTension.Where(x => x.activo == true).Distinct().ToList();
            if (tensions.Any())
            {
                var idTipoLATs = 2;
                var idTipoBarras = 4;
                var idMedidaP = 1;
                var idMedidaQ = 2;
                int tableCount = 1;

                foreach (var tension in tensions)
                {
                    var tmpLATs = equipoTensionMedidaQuery(idEstacion, idTipoLATs, tension.idTension, idMedidaP).ToList();
                    var tmpBarras = equipoTensionMedidaQuery(idEstacion, idTipoBarras, tension.idTension, idMedidaP).ToList();

                    if (tmpLATs.Any() || tmpBarras.Any())
                    {
                        List<HeaderTable>? headersTable = new List<HeaderTable>();

                        int idHeaderCount = 1;
                        HeaderTable headerHora = new HeaderTable
                        {
                            idHeader = idHeaderCount++,
                            headerTitle = "Hora"
                        };
                        headersTable.Add(headerHora);

                        if (tmpLATs.Any())
                        {
                            foreach (var lat in tmpLATs)
                            {

                                HeaderTable headerP = new HeaderTable
                                {
                                    idHeader = 1,
                                    headerTitle = "P (MW)"
                                };
                                HeaderTable headerQ = new HeaderTable
                                {
                                    idHeader = 2,
                                    headerTitle = "Q (Mvar)"
                                };
                                List<HeaderTable>? subheaders = new List<HeaderTable> { headerP, headerQ };

                                HeaderTable headerLAT = new HeaderTable
                                {
                                    idHeader = idHeaderCount++,
                                    idEquipo = lat.idEquipo,
                                    headerTitle = lat.nombreEquipo,
                                    subheaders = subheaders
                                };
                                headersTable.Add(headerLAT);
                            }
                        }
                        if (tmpBarras.Any())
                        {
                            foreach (var barra in tmpBarras)
                            {
                                HeaderTable headerP = new HeaderTable
                                {
                                    idHeader = 1,
                                    headerTitle = "P (MW)"
                                };
                                HeaderTable headerQ = new HeaderTable
                                {
                                    idHeader = 2,
                                    headerTitle = "Q (Mvar)"
                                };
                                List<HeaderTable>? subheaders = new List<HeaderTable> { headerP, headerQ };

                                HeaderTable headerBarra = new HeaderTable
                                {
                                    idHeader = idHeaderCount++,
                                    idEquipo = barra.idEquipo,
                                    headerTitle = barra.nombreEquipo,
                                    subheaders = subheaders
                                };
                                headersTable.Add(headerBarra);

                            }
                        }

                        List<RowBodyTable>? bodyTable = new List<RowBodyTable>();

                        if (headersTable.Count > 1)
                        {
                            int countHour = 1;
                            for (DateTime i = from; i <= to; i = i.AddMinutes(5))
                            {
                                List<RowValue>? rowValues = new List<RowValue>();
                                int orderValue = 1;

                                for (int j = 1; j < (headersTable.Count); j++)
                                {
                                    int? idEquipo = headersTable[j].idEquipo;
                                    int? idHeader = headersTable[j].idHeader;

                                    var registrosP = registrosListQuery((int)idEquipo, tension.idTension, idMedidaP, from, to).ToList();
                                    var registrosQ = registrosListQuery((int)idEquipo, tension.idTension, idMedidaQ, from, to).ToList();
                                    if (registrosP.Any() || registrosQ.Any())
                                    {
                                        if (registrosP.Any())
                                        {
                                            foreach (var registro in registrosP)
                                            {
                                                var valor = (valoresTimeQuery(registro.idRegistroMedida, i, idEquipo)).FirstOrDefault();
                                                if (valor != null)
                                                {
                                                    RowValue rowValue = new RowValue
                                                    {
                                                        orderValue = orderValue++,
                                                        idHeader = idHeader,
                                                        idSubheader = headersTable[j].subheaders[0].idHeader,
                                                        idValorMedida = (valor != null) ? valor.idValorMedida : null,
                                                        valorMedida = (valor != null) ? Math.Round((double)valor.valorMedida, 2, MidpointRounding.AwayFromZero) : null
                                                    };
                                                    rowValues.Add(rowValue);
                                                }


                                            }
                                        }
                                        if (registrosQ.Any())
                                        {
                                            foreach (var registro in registrosQ)
                                            {
                                                var valor = (valoresTimeQuery(registro.idRegistroMedida, i, idEquipo)).FirstOrDefault();

                                                if (valor != null)
                                                {
                                                    RowValue rowValue = new RowValue
                                                    {
                                                        orderValue = orderValue++,
                                                        idHeader = idHeader,
                                                        idSubheader = headersTable[j].subheaders[1].idHeader,
                                                        idValorMedida = (valor != null) ? valor.idValorMedida : null,
                                                        valorMedida = (valor != null) ? Math.Round((double)valor.valorMedida, 2, MidpointRounding.AwayFromZero) : null
                                                    };
                                                    rowValues.Add(rowValue);
                                                }


                                            }
                                        }
                                    }
                                    else
                                    {
                                        headersTable.RemoveAt(j);
                                    }

                                }
                                if (rowValues.Any())
                                {
                                    RowBodyTable rowBodyTable = new RowBodyTable
                                    {
                                        idHour = countHour++,
                                        dateHour = (i.ToString("g", CultureInfo.GetCultureInfo("es-AR"))),
                                        rowValues = rowValues
                                    };
                                    bodyTable.Add(rowBodyTable);
                                }

                            }

                            if (headersTable.Count > 1)
                            {
                                List<RowValue>? avgRow = new List<RowValue>();
                                for (int j = 1; j < headersTable.Count; j++)
                                {
                                    int? idEquipo = headersTable[j].idEquipo;
                                    int? idHeader = headersTable[j].idHeader;

                                    var registrosP = registrosListQuery((int)idEquipo, tension.idTension, idMedidaP, from, to).ToList();
                                    var registrosQ = registrosListQuery((int)idEquipo, tension.idTension, idMedidaQ, from, to).ToList();
                                    if (registrosP.Any())
                                    {
                                        double? promP = 0;
                                        List<ValorVM>? valores = new List<ValorVM>();
                                        foreach (var registro in registrosP)
                                        {
                                            var tmpValores = (valoresQuery(registro.idRegistroMedida, registro.fInicio, registro.fFin, idEquipo)).ToList();
                                            if (tmpValores.Count > 0 && tmpValores != null)
                                            {
                                                valores.AddRange(tmpValores);
                                            }

                                        }
                                        promP += valores.Average(x => x.valorMedida);
                                        RowValue rowTotal = new RowValue
                                        {
                                            orderValue = j,
                                            idValorMedida = null,
                                            idHeader = idHeader,
                                            idSubheader = headersTable[j].subheaders[0].idHeader,
                                            valorMedida = Math.Round((double)promP, 2, MidpointRounding.AwayFromZero)
                                        };
                                        avgRow.Add(rowTotal);
                                    }
                                    if (registrosQ.Any())
                                    {
                                        double? promQ = 0;
                                        List<ValorVM>? valores = new List<ValorVM>();
                                        foreach (var registro in registrosQ)
                                        {
                                            var tmpValores = (valoresQuery(registro.idRegistroMedida, registro.fInicio, registro.fFin, idEquipo)).ToList();
                                            if (tmpValores.Count > 0 && tmpValores != null)
                                            {
                                                valores.AddRange(tmpValores);
                                            }

                                        }
                                        promQ += valores.Average(x => x.valorMedida);
                                        RowValue rowTotal = new RowValue
                                        {
                                            orderValue = j + 1,
                                            idValorMedida = null,
                                            idHeader = idHeader,
                                            idSubheader = headersTable[j].subheaders[1].idHeader,
                                            valorMedida = Math.Round((double)promQ, 2, MidpointRounding.AwayFromZero)
                                        };
                                        avgRow.Add(rowTotal);
                                    }
                                }
                                if (bodyTable.Any())
                                {
                                    RowBodyTable roWTotal = new RowBodyTable
                                    {
                                        idHour = (bodyTable.OrderByDescending(x => x.idHour).FirstOrDefault().idHour + 1),
                                        dateHour = "Prom.",
                                        rowValues = avgRow
                                    };

                                    bodyTable.Add(roWTotal);

                                    dataTablaVM tabla = new dataTablaVM
                                    {
                                        idTable = tableCount++,
                                        titleTable = tension.valorTension + " kV",
                                        bodyTable = bodyTable,
                                        headersTable = headersTable
                                    };
                                    tablas.Add(tabla);
                                }


                            }

                        }


                    }
                }
            }
            return tablas;
        }
        public List<dataTablaVM> totalesPQbarrsaLineas(int idEstacion, DateTime from, DateTime to)
        {
            //  **-- TABLA SIMPLE --** //
            List<dataTablaVM> tablas = new List<dataTablaVM>();
            var tensions = _context.HistTension.Where(x => x.activo == true).Distinct().ToList();
            if (tensions.Any())
            {
                var idTipoLATs = 2;
                var idTipoBarras = 4;
                var idMedidaP = 1;
                var idMedidaQ = 2;
                int tableCount = 1;

                foreach (var tension in tensions)
                {
                    var tmpLATs = equipoTensionMedidaQuery(idEstacion, idTipoLATs, tension.idTension,idMedidaP).ToList();
                    var tmpBarras = equipoTensionMedidaQuery(idEstacion, idTipoBarras, tension.idTension,idMedidaP).ToList();

                    if (tmpLATs.Any() || tmpBarras.Any())
                    {
                        int idHeaderCount = 1;
                        HeaderTable headerHora = new HeaderTable
                        {
                            idHeader = idHeaderCount++,
                            headerTitle = "Hora"
                        };
                        HeaderTable headerCosPhi = new HeaderTable
                        {
                            idHeader = idHeaderCount++,
                            headerTitle = "Cos φ"
                        };
                        HeaderTable headerP = new HeaderTable
                        {
                            idHeader = idHeaderCount++,
                            headerTitle = "P (MW)"
                        };
                        HeaderTable headerQ = new HeaderTable
                        {
                            idHeader = idHeaderCount++,
                            headerTitle = "Q (Mvar)"
                        };

                        List<HeaderTable>? headersTable = new List<HeaderTable>() { headerHora, headerCosPhi, headerP, headerQ };
                        List<RowBodyTable>? bodyTable = new List<RowBodyTable>();

                        if (headersTable.Any())
                        {
                            int countHour = 1;

                            List<double> allvaloresP = new List<double>();//hacer listas en vez de simples doubles
                            List<double> allvaloresQ = new List<double>();
                            List<double> allvaloresCosPhi = new List<double>();
                            for (DateTime i = from; i <= to; i = i.AddMinutes(5))
                            {
                                List<double> valoresP = new List<double>();//hacer listas en vez de simples doubles
                                List<double> valoresQ = new List<double>();
                                List<double> valoresCosPhi = new List<double>();

                                int orderValue = 1;
                                List<EquipoVM> equipos = new List<EquipoVM>();
                                equipos.AddRange(tmpLATs);
                                equipos.AddRange(tmpBarras);

                                double cosenoPHIcalculado = 0;
                                double potenciaAparenteScalculado = 0;
                                foreach (var equipo in equipos)
                                {
                                    var registrosP = registrosListQuery(equipo.idEquipo, tension.idTension, idMedidaP, from, to).ToList();
                                    var registrosQ = registrosListQuery(equipo.idEquipo, tension.idTension, idMedidaQ, from, to).ToList();
                                    if (registrosP.Any() && registrosQ.Any())
                                    {
                                        
                                        foreach (var registro in registrosP)
                                        {
                                            var valor = (valoresTimeQuery(registro.idRegistroMedida, i, equipo.idEquipo)).FirstOrDefault();
                                            if(valor != null)
                                            {
                                                valoresP.Add((double)valor.valorMedida);
                                                allvaloresP.Add((double)valor.valorMedida);
                                            }
                                            
                                        }
                                       
                                        foreach (var registro in registrosQ)
                                        {
                                            var valor = (valoresTimeQuery(registro.idRegistroMedida, i, equipo.idEquipo)).FirstOrDefault();
                                            if (valor != null)
                                            {
                                                valoresQ.Add((double)valor.valorMedida);
                                                allvaloresQ.Add((double)valor.valorMedida);
                                            }
                                        }
                                    }
                                    
                                }
                                //COSENO PHI => angulo entre potencia activa (P) y potencia aparente (S)
                                // cos = P/S
                                // cos = P/ ( (p^2 + q^2)^1/2)

                                //sino dice la Romi hacerlo como en el excel, pero por ahora está como dice google
                                //RAIZ(1/((AK6/AJ6)^2+1)

                                if(valoresP.Count > 0 && valoresQ.Count > 0)
                                {
                                    potenciaAparenteScalculado = Math.Sqrt(Math.Pow(valoresP.Sum(), 2) + Math.Pow(valoresQ.Sum(), 2));
                                    if(potenciaAparenteScalculado != 0) cosenoPHIcalculado = valoresP.Sum() / potenciaAparenteScalculado;
                                    valoresCosPhi.Add(cosenoPHIcalculado);
                                    allvaloresCosPhi.Add(cosenoPHIcalculado);

                                    RowValue rowCosPhi = new RowValue
                                    {
                                        orderValue = orderValue++, 
                                        idHeader = headersTable.Find(x => x.headerTitle.Contains("Cos")).idHeader,
                                        idValorMedida = null,
                                        valorMedida = Math.Round(cosenoPHIcalculado, 2, MidpointRounding.AwayFromZero)
                                    };
                                    RowValue rowP = new RowValue
                                    {
                                        orderValue = orderValue++,
                                        idHeader = headersTable.Find(x => x.headerTitle.Contains("P (MW)")).idHeader,
                                        idValorMedida = null,
                                        valorMedida = Math.Round(valoresP.Sum(), 2, MidpointRounding.AwayFromZero)
                                    };
                                    RowValue rowQ = new RowValue
                                    {
                                        orderValue = orderValue++,
                                        idHeader = headersTable.Find(x => x.headerTitle.Contains("Q (Mvar)")).idHeader,
                                        idValorMedida = null,
                                        valorMedida = Math.Round(valoresQ.Sum(), 2, MidpointRounding.AwayFromZero)
                                    };

                                    List<RowValue>? rowValues = new List<RowValue>() { rowCosPhi,rowP,rowQ};

                                    RowBodyTable rowBodyTable = new RowBodyTable
                                    {
                                        idHour = countHour++,
                                        dateHour = (i.ToString("g", CultureInfo.GetCultureInfo("es-AR"))),
                                        rowValues = rowValues
                                    };
                                    bodyTable.Add(rowBodyTable);
                                }
                            }
                            if(allvaloresCosPhi.Any() && allvaloresP.Any() && allvaloresQ.Any())
                            {
                                //Promedio
                                int orderValueAvg = 1;
                                RowValue rowCosPhiAvg = new RowValue
                                {
                                    orderValue = orderValueAvg++,
                                    idHeader = headersTable.Find(x => x.headerTitle.Contains("Cos")).idHeader,
                                    idValorMedida = null,
                                    valorMedida = Math.Round(allvaloresCosPhi.Average(), 2, MidpointRounding.AwayFromZero)
                                };
                                RowValue rowPAvg = new RowValue
                                {
                                    orderValue = orderValueAvg++,
                                    idHeader = headersTable.Find(x => x.headerTitle.Contains("P (MW)")).idHeader,
                                    idValorMedida = null,
                                    valorMedida = Math.Round(allvaloresP.Average(), 2, MidpointRounding.AwayFromZero)
                                };
                                RowValue rowQAvg = new RowValue
                                {
                                    orderValue = orderValueAvg++,
                                    idHeader = headersTable.Find(x => x.headerTitle.Contains("Q (Mvar)")).idHeader,
                                    idValorMedida = null,
                                    valorMedida = Math.Round(allvaloresQ.Average(), 2, MidpointRounding.AwayFromZero)
                                };

                                List<RowValue>? avgRow = new List<RowValue>() { rowCosPhiAvg, rowPAvg, rowQAvg };

                                RowBodyTable roWTotal = new RowBodyTable
                                {
                                    idHour = (bodyTable.OrderByDescending(x => x.idHour).FirstOrDefault().idHour + 1),
                                    dateHour = "Prom.",
                                    rowValues = avgRow
                                };
                                bodyTable.Add(roWTotal);
                            }
                        }
                        if (bodyTable.Any())
                        {
                            dataTablaVM tabla = new dataTablaVM
                            {
                                idTable = tableCount++,
                                titleTable = tension.valorTension + " kV",
                                bodyTable = bodyTable,
                                headersTable = headersTable
                            };
                            tablas.Add(tabla);
                        }
                    }
                }
            }
            return tablas;
        }
        public List<dataTablaVM> corrienteBarrasLineas (int idEstacion, DateTime from, DateTime to)
        {
            //  **-- TABLA SIMPLE --** //
            List<dataTablaVM> tablas = new List<dataTablaVM>();
            var tensions = _context.HistTension.Where(x => x.activo == true).Distinct().ToList();
            if (tensions.Any())
            {
                var idTipoLATs = 2;
                var idTipoBarras = 4;
                var idMedidaI = 5;

                int tableCount = 1;
                foreach (var tension in tensions)
                {
                    var lineas = equipoTensionMedidaQuery(idEstacion, idTipoLATs, tension.idTension,idMedidaI).ToList();
                    var barras = equipoTensionMedidaQuery(idEstacion, idTipoBarras,tension.idTension,idMedidaI).ToList();

                    if(lineas.Any() || barras.Any())
                    {
                        List<HeaderTable>? headersTable = new List<HeaderTable>();
                        int idHeaderCount = 1;
                        HeaderTable headerHora = new HeaderTable
                        {
                            idHeader = idHeaderCount++,
                            headerTitle = "Hora"
                        };
                        headersTable.Add(headerHora);
                        if (lineas.Any())
                        {
                            foreach(var linea in lineas)
                            {
                                HeaderTable headerLAT = new HeaderTable
                                {
                                    idHeader = idHeaderCount++,
                                    idEquipo = linea.idEquipo,
                                    headerTitle = linea.nombreEquipo,
                                    subheaders = null
                                };
                                headersTable.Add(headerLAT);
                            }
                        }
                        if (barras.Any())
                        {
                            foreach(var barra in barras)
                            {
                                HeaderTable headerBarra = new HeaderTable
                                {
                                    idHeader = idHeaderCount++,
                                    idEquipo = barra.idEquipo,
                                    headerTitle = barra.nombreEquipo,
                                    subheaders = null
                                };
                                headersTable.Add(headerBarra);
                            }
                        }

                        //
                        List<RowBodyTable>? bodyTable = new List<RowBodyTable>();
                        int countHour = 1;
                        for (DateTime i = from; i <= to; i = i.AddMinutes(5))
                        {
                            List<RowValue>? rowValues = new List<RowValue>();
                            int orderValue = 1;
                            for (int j = 1; j < (headersTable.Count); j++)
                            {
                                int? idEquipo = headersTable[j].idEquipo;
                                int? idHeader = headersTable[j].idHeader;

                                var registrosI = registrosListQuery((int)idEquipo, tension.idTension, idMedidaI, from, to).ToList();
                                if (registrosI.Any())
                                {
                                    foreach (var registro in registrosI)
                                    {
                                        var valor = (valoresTimeQuery(registro.idRegistroMedida, i, idEquipo)).FirstOrDefault();
                                        if (valor != null)
                                        {
                                            RowValue rowValue = new RowValue
                                            {
                                                orderValue = orderValue++,
                                                idHeader = idHeader,
                                                idValorMedida = valor.idValorMedida,
                                                valorMedida = Math.Round((double)valor.valorMedida, 2, MidpointRounding.AwayFromZero)
                                            };
                                            rowValues.Add(rowValue);
                                        }
                                    }
                                }
                                else
                                {
                                    headersTable.RemoveAt(j);
                                }
                            }
                            if (rowValues.Any())
                            {
                                RowBodyTable rowBodyTable = new RowBodyTable
                                {
                                    idHour = countHour++,
                                    dateHour = (i.ToString("g", CultureInfo.GetCultureInfo("es-AR"))),
                                    rowValues = rowValues
                                };
                                bodyTable.Add(rowBodyTable);
                            }
                        }
                        if (bodyTable.Any())
                        {
                            List<RowValue>? avgRow = new List<RowValue>();
                            for (int j = 1; j < headersTable.Count; j++)
                            {
                                int? idEquipo = headersTable[j].idEquipo;
                                int? idHeader = headersTable[j].idHeader;

                                var registrosI = registrosListQuery((int)idEquipo, tension.idTension, idMedidaI, from, to).ToList();
                                
                                if (registrosI.Any())
                                {
                                    double? promI = 0;
                                    List<ValorVM>? valores = new List<ValorVM>();
                                    foreach (var registro in registrosI)
                                    {
                                        var tmpValores = (valoresQuery(registro.idRegistroMedida, registro.fInicio, registro.fFin, idEquipo)).ToList();
                                        if (tmpValores.Count > 0 && tmpValores != null)
                                        {
                                            valores.AddRange(tmpValores);
                                        }

                                    }
                                    promI += valores.Average(x => x.valorMedida);
                                    RowValue rowTotal = new RowValue
                                    {
                                        orderValue = j,
                                        idValorMedida = null,
                                        idHeader = idHeader,
                                        valorMedida = Math.Round((double)promI, 2, MidpointRounding.AwayFromZero)
                                    };
                                    avgRow.Add(rowTotal);
                                }
                                
                            }
                            RowBodyTable rowTotalAvg = new RowBodyTable
                            {
                                idHour = (bodyTable.OrderByDescending(x => x.idHour).FirstOrDefault().idHour + 1),
                                dateHour = "Prom.",
                                rowValues = avgRow
                            };

                            bodyTable.Add(rowTotalAvg);
                            dataTablaVM tabla = new dataTablaVM
                            {
                                idTable = tableCount++,
                                titleTable = "Corriente | "+tension.valorTension + " kV",
                                bodyTable = bodyTable,
                                headersTable = headersTable
                            };
                            tablas.Add(tabla);
                        }
                        
                    }
                }
            }
            return tablas;
        }
        public List<dataTablaVM> totalesCorrienteBarrasLineas(int idEstacion, DateTime from, DateTime to)
        {
            //  **-- TABLA SIMPLE --** //
            List<dataTablaVM> tablas = new List<dataTablaVM>();
            var tensions = _context.HistTension.Where(x => x.activo == true).Distinct().ToList();
            if (tensions.Any())
            {
                var idTipoLATs = 2;
                var idTipoBarras = 4;
                var idMedidaI = 5;

                int tableCount = 1;

                foreach (var tension in tensions)
                {
                    var lineas = equipoTensionMedidaQuery(idEstacion, idTipoLATs, tension.idTension, idMedidaI).ToList();
                    var barras = equipoTensionMedidaQuery(idEstacion, idTipoBarras, tension.idTension, idMedidaI).ToList();

                    if (lineas.Any() || barras.Any())
                    {
                        int idHeaderCount = 1;
                        HeaderTable headerHora = new HeaderTable
                        {
                            idHeader = idHeaderCount++,
                            headerTitle = "Hora"
                        };
                        HeaderTable headerTotal = new HeaderTable
                        {
                            idHeader = idHeaderCount++,
                            headerTitle = "Total"
                        };

                        List<HeaderTable>? headersTable = new List<HeaderTable>() { headerHora, headerTotal};
                        List<RowBodyTable>? bodyTable = new List<RowBodyTable>();

                        if (headersTable.Any())
                        {
                            int countHour = 1;

                            List<double> allvalores = new List<double>();//hacer listas en vez de simples doubles
                            for (DateTime i = from; i <= to; i = i.AddMinutes(5))
                            {
                                List<double> valores = new List<double>();//hacer listas en vez de simples doubles

                                int orderValue = 1;
                                List<EquipoVM> equipos = new List<EquipoVM>();
                                equipos.AddRange(lineas);
                                equipos.AddRange(barras);

                                double potenciaAparenteScalculado = 0;
                                foreach (var equipo in equipos)
                                {
                                    var registrosValores = registrosListQuery(equipo.idEquipo, tension.idTension, idMedidaI, from, to).ToList();
                                    if (registrosValores.Any())
                                    {

                                        foreach (var registro in registrosValores)
                                        {
                                            var valor = (valoresTimeQuery(registro.idRegistroMedida, i, equipo.idEquipo)).FirstOrDefault();
                                            if (valor != null)
                                            {
                                                valores.Add((double)valor.valorMedida);
                                                allvalores.Add((double)valor.valorMedida);
                                            }

                                        }
                                    }
                                }
                                //COSENO PHI => angulo entre potencia activa (P) y potencia aparente (S)
                                // cos = P/S
                                // cos = P/ ( (p^2 + q^2)^1/2)

                                //sino dice la Romi hacerlo como en el excel, pero por ahora está como dice google
                                //RAIZ(1/((AK6/AJ6)^2+1)

                                if (valores.Count > 0)
                                {
                                    potenciaAparenteScalculado = Math.Sqrt(Math.Pow(valores.Sum(), 2));

                                    RowValue rowP = new RowValue
                                    {
                                        orderValue = orderValue++,
                                        idHeader = headersTable.Find(x => x.headerTitle.Contains("Total")).idHeader,
                                        idValorMedida = null,
                                        valorMedida = Math.Round(valores.Sum(), 2, MidpointRounding.AwayFromZero)
                                    };

                                    List<RowValue>? rowValues = new List<RowValue>() {  rowP };

                                    RowBodyTable rowBodyTable = new RowBodyTable
                                    {
                                        idHour = countHour++,
                                        dateHour = (i.ToString("g", CultureInfo.GetCultureInfo("es-AR"))),
                                        rowValues = rowValues
                                    };
                                    bodyTable.Add(rowBodyTable);
                                }
                            }
                            if (allvalores.Any())
                            {
                                //Promedio
                                int orderValueAvg = 1;
                                RowValue rowPAvg = new RowValue
                                {
                                    orderValue = orderValueAvg++,
                                    idHeader = headersTable.Find(x => x.headerTitle.Contains("Total")).idHeader,
                                    idValorMedida = null,
                                    valorMedida = Math.Round(allvalores.Average(), 2, MidpointRounding.AwayFromZero)
                                };

                                List<RowValue>? avgRow = new List<RowValue>() {  rowPAvg };

                                RowBodyTable roWTotal = new RowBodyTable
                                {
                                    idHour = (bodyTable.OrderByDescending(x => x.idHour).FirstOrDefault().idHour + 1),
                                    dateHour = "Prom.",
                                    rowValues = avgRow
                                };
                                bodyTable.Add(roWTotal);
                            }
                        }
                        if (bodyTable.Any())
                        {
                            dataTablaVM tabla = new dataTablaVM
                            {
                                idTable = tableCount++,
                                titleTable = tension.valorTension + " kV",
                                bodyTable = bodyTable,
                                headersTable = headersTable
                            };
                            tablas.Add(tabla);
                        }
                        
                    }
                }
            }
            return tablas;
        }
        public List<dataTablaVM> amperesLineasCDP(int idEstacion, DateTime from, DateTime to)
        {
            //  **-- TABLA GROUPED --** //
            List<dataTablaVM> tablas = new List<dataTablaVM>();
            var tensions = _context.HistTension.Where(x => x.activo == true).Where(x => x.idTension == 4 || x.idTension == 5).Distinct().ToList();
            //var latTensions = _context.HistEquipo.Where(x => x.idEstacion == 6).Distinct().ToList();
            if (tensions.Any())
            {
                var idTipoLATs = 2;
                var idTipoBarras = 4;
                var idMedidaP = 1;
                var idMedidaQ = 2;
                var idMedidaT = 4;
                int tableCount = 1;

                double mayorValorBarras = 0;
                double resultado = 0;

                foreach (var tension in tensions)
                {
                    var tmpLATs = equipoTensionMedidaQuery(idEstacion, idTipoLATs, tension.idTension, idMedidaP).ToList();
                    var tmpBarras = equipoTensionMedidaQuery(idEstacion, idTipoBarras, tension.idTension, idMedidaP).ToList();

                    if (tmpLATs.Any() || tmpBarras.Any())
                    {
                        List<HeaderTable>? headersTable = new List<HeaderTable>();

                        int idHeaderCount = 1;
                        HeaderTable headerHora = new HeaderTable
                        {
                            idHeader = idHeaderCount++,
                            headerTitle = "Hora"
                        };
                        headersTable.Add(headerHora);

                        if (tmpLATs.Any())
                        {
                            foreach (var lat in tmpLATs)
                            {

                                if (lat !=null && lat.nombreEquipo != null)
                                {
                                    if (lat.nombreEquipo.Equals("LAT 1 Gran Mendoza", StringComparison.OrdinalIgnoreCase) ||
                                                                lat.nombreEquipo.Equals("LAT 2 Gran Mendoza", StringComparison.OrdinalIgnoreCase) ||
                                                                lat.nombreEquipo.Equals("LAT 1 Lujan de Cuyo", StringComparison.OrdinalIgnoreCase) ||
                                                                lat.nombreEquipo.Equals("LAT 2 Lujan de Cuyo", StringComparison.OrdinalIgnoreCase) ||
                                                                lat.nombreEquipo.Equals("LAT Anchoris", StringComparison.OrdinalIgnoreCase) ||
                                                                lat.nombreEquipo.Equals("LAT Agua Del Toro", StringComparison.OrdinalIgnoreCase) ||
                                                                lat.nombreEquipo.Equals("LAT San Juan", StringComparison.OrdinalIgnoreCase))
                                    {
                                        HeaderTable headerA = new HeaderTable
                                        {
                                            idHeader = 1,
                                            headerTitle = "amperes"
                                        };
                                        List<HeaderTable>? subheaders = new List<HeaderTable> { headerA };

                                        HeaderTable headerLAT = new HeaderTable
                                        {
                                            idHeader = idHeaderCount++,
                                            idEquipo = lat.idEquipo,
                                            headerTitle = lat.nombreEquipo,
                                            subheaders = subheaders
                                        };
                                        headersTable.Add(headerLAT);
                                    } 
                                }
                            }
                        }
                        if (tmpBarras.Any())
                        {
                            foreach (var barra in tmpBarras)
                            {
                                HeaderTable headerA = new HeaderTable
                                {
                                    idHeader = 1,
                                    headerTitle = "amperes"
                                };
                                List<HeaderTable>? subheaders = new List<HeaderTable> { headerA, };

                                HeaderTable headerBarra = new HeaderTable
                                {
                                    idHeader = idHeaderCount++,
                                    idEquipo = barra.idEquipo,
                                    headerTitle = barra.nombreEquipo,
                                    subheaders = subheaders
                                };
                                headersTable.Add(headerBarra);

                            }
                        }

                        List<RowBodyTable>? bodyTable = new List<RowBodyTable>();

                        if (headersTable.Count > 1)
                        {
                            int countHour = 1;
                            for (DateTime i = from; i <= to; i = i.AddMinutes(5))
                            {
                                List<RowValue>? rowValues = new List<RowValue>();
                                List<RowValue>? rowValues1 = new List<RowValue>();
                                int orderValue = 1;

                                for (int j = 1; j < (headersTable.Count); j++)
                                {
                                    int? idEquipo = headersTable[j].idEquipo;
                                    int? idHeader = headersTable[j].idHeader;

                                    var registrosP = registrosListQuery((int)idEquipo, tension.idTension, idMedidaP, from, to).ToList();
                                    var registrosQ = registrosListQuery((int)idEquipo, tension.idTension, idMedidaQ, from, to).ToList();

                                    //prueba para el calculo
                                    if(registrosP.Any() && registrosQ.Any())
                                    {
                                        ValorVM? valorP = new ValorVM();
                                        ValorVM? valorQ = new ValorVM();
                                        foreach (var registro in registrosP)
                                        {
                                            valorP = (valoresTimeQuery(registro.idRegistroMedida, i, idEquipo)).FirstOrDefault();
                                        }
                                        foreach (var registro2 in registrosQ)
                                        {
                                            valorQ = (valoresTimeQuery(registro2.idRegistroMedida, i, idEquipo)).FirstOrDefault();
                                        }
                                        if(valorP != null && valorQ != null)
                                        {
                                            double sumaCuadrados = ((double)valorP.valorMedida * (double)valorP.valorMedida + (double)valorQ.valorMedida * (double)valorQ.valorMedida);
                                            //en este punto se necesitan los valores de las barra a y b para comparar y ver por cual valor dividir
                                            if (tension.valorTension == "132")
                                            {
                                                var barraAT = consultaEquipo("Barra a", idEstacion).FirstOrDefault();
                                                var barraBT = consultaEquipo("Barra b", idEstacion).FirstOrDefault();

                                                if (barraAT != null && barraBT != null)
                                                {
                                                    var registrosBarraAT = registrosListQuery(barraAT.idEquipo, tension.idTension, idMedidaT, from, to).ToList();
                                                    var registrosBarraBT = registrosListQuery(barraBT.idEquipo, tension.idTension, idMedidaT, from, to).ToList();
                                                    if (registrosBarraAT != null && registrosBarraAT.Any() && registrosBarraBT != null && registrosBarraBT.Any())
                                                    {
                                                        //hay que revisar el registrosBarraAT[0] ya que cuando trae mas de un registro puede venir desordenado
                                                        //y esto causa problemas, deberia ver como hacer que use el que coincida con la fecha
                                                        ValorVM? valorBarraA = new ValorVM();
                                                        ValorVM? valorBarraB = new ValorVM();
                                                        foreach (var registro in registrosBarraAT)
                                                        {

                                                            var valor = (valoresTimeQuery(registro.idRegistroMedida, i, idEquipo)).FirstOrDefault();
                                                            if (valor != null) valorBarraA = valor;
                                                        }
                                                        foreach (var registro in registrosBarraBT)
                                                        {
                                                            var valor = (valoresTimeQuery(registro.idRegistroMedida, i, idEquipo)).FirstOrDefault();
                                                            if (valor != null) valorBarraB = valor;
                                                        }
                                                        if (valorBarraA.valorMedida > valorBarraB.valorMedida)
                                                        {
                                                            mayorValorBarras = (double)valorBarraA.valorMedida;
                                                        }
                                                        else
                                                        {
                                                            mayorValorBarras = Math.Round((double)valorBarraB.valorMedida, 2, MidpointRounding.AwayFromZero);
                                                        }
                                                        if (mayorValorBarras != 0) resultado = (Math.Sqrt(sumaCuadrados) * 1000) / (mayorValorBarras * 1.73205);
                                                    } 
                                                }

                                            }
                                            else
                                            {
                                               var registrosTLat = registrosListQuery((int)idEquipo, tension.idTension, idMedidaT, from, to).ToList();
                                                ValorVM? valorTLat = new ValorVM();
                                                foreach (var registro in registrosTLat)
                                                {

                                                    var valor = (valoresTimeQuery(registro.idRegistroMedida, i, idEquipo)).FirstOrDefault();
                                                    if (valor != null) valorTLat = valor;
                                                }
                                                if(valorTLat!=null && valorTLat.valorMedida != 0)resultado = (Math.Sqrt(sumaCuadrados) * 1000) / ((double)valorTLat.valorMedida * 1.73205);
                                            }
                                                
                                            RowValue rowValue = new RowValue
                                            {
                                                orderValue = orderValue++,
                                                idHeader = idHeader,
                                                idSubheader = headersTable[j].subheaders[0].idHeader,
                                                idValorMedida = (valorP != null) ? valorP.idValorMedida : null,
                                                valorMedida = (valorP != null) ? Math.Round(resultado, 2, MidpointRounding.AwayFromZero) : null
                                            };
                                            rowValues.Add(rowValue);

                                        }

                                        
                                    }

                                    //fin de prueba
                                    //if (registrosP.Any() || registrosQ.Any())
                                    //{
                                    //    if (registrosP.Any())
                                    //    {
                                    //        foreach (var registro in registrosP)
                                    //        {
                                    //            var valor = (valoresTimeQuery(registro.idRegistroMedida, i, idEquipo)).FirstOrDefault();
                                    //            if (valor != null)
                                    //            {
                                    //                RowValue rowValue = new RowValue
                                    //                {
                                    //                    orderValue = orderValue++,
                                    //                    idHeader = idHeader,
                                    //                    idSubheader = headersTable[j].subheaders[0].idHeader,
                                    //                    idValorMedida = (valor != null) ? valor.idValorMedida : null,
                                    //                    valorMedida = (valor != null) ? Math.Round((double)valor.valorMedida, 2, MidpointRounding.AwayFromZero) : null
                                    //                };
                                    //                rowValues.Add(rowValue);
                                    //            }


                                    //        }
                                    //    }
                                    //    if (registrosQ.Any())
                                    //    {
                                    //        foreach (var registro in registrosQ)
                                    //        {
                                    //            var valor = (valoresTimeQuery(registro.idRegistroMedida, i, idEquipo)).FirstOrDefault();

                                    //            if (valor != null)
                                    //            {
                                    //                RowValue rowValue = new RowValue
                                    //                {
                                    //                    orderValue = orderValue++,
                                    //                    idHeader = idHeader,
                                    //                    idSubheader = headersTable[j].subheaders[1].idHeader,
                                    //                    idValorMedida = (valor != null) ? valor.idValorMedida : null,
                                    //                    valorMedida = (valor != null) ? Math.Round((double)valor.valorMedida, 2, MidpointRounding.AwayFromZero) : null
                                    //                };
                                    //                rowValues.Add(rowValue);
                                    //            }


                                    //        }
                                    //    }
                                    //}
                                    //else
                                    //{
                                    //    headersTable.RemoveAt(j);
                                    //}

                                }
                                if (rowValues.Any())
                                {
                                    RowBodyTable rowBodyTable = new RowBodyTable
                                    {
                                        idHour = countHour++,
                                        dateHour = (i.ToString("g", CultureInfo.GetCultureInfo("es-AR"))),
                                        rowValues = rowValues
                                    };
                                    bodyTable.Add(rowBodyTable);
                                }

                            }

                            if (headersTable.Count > 1)
                            {
                                List<RowValue>? avgRow = new List<RowValue>();
                                for (int j = 1; j < headersTable.Count; j++)
                                {
                                    int? idEquipo = headersTable[j].idEquipo;
                                    int? idHeader = headersTable[j].idHeader;

                                    var registrosP = registrosListQuery((int)idEquipo, tension.idTension, idMedidaP, from, to).ToList();
                                    var registrosQ = registrosListQuery((int)idEquipo, tension.idTension, idMedidaQ, from, to).ToList();
                                    if (registrosP.Any())
                                    {
                                        double? promP = 0;
                                        List<ValorVM>? valores = new List<ValorVM>();
                                        foreach (var registro in registrosP)
                                        {
                                            var tmpValores = (valoresQuery(registro.idRegistroMedida, registro.fInicio, registro.fFin, idEquipo)).ToList();
                                            if (tmpValores.Count > 0 && tmpValores != null)
                                            {
                                                valores.AddRange(tmpValores);
                                            }

                                        }
                                        promP += valores.Average(x => x.valorMedida);
                                        RowValue rowTotal = new RowValue
                                        {
                                            orderValue = j,
                                            idValorMedida = null,
                                            idHeader = idHeader,
                                            idSubheader = headersTable[j].subheaders[0].idHeader,
                                            valorMedida = Math.Round((double)promP, 2, MidpointRounding.AwayFromZero)
                                        };
                                        avgRow.Add(rowTotal);
                                    }
                                    //if (registrosQ.Any())
                                    //{
                                    //    double? promQ = 0;
                                    //    List<ValorVM>? valores = new List<ValorVM>();
                                    //    foreach (var registro in registrosQ)
                                    //    {
                                    //        var tmpValores = (valoresQuery(registro.idRegistroMedida, registro.fInicio, registro.fFin, idEquipo)).ToList();
                                    //        if (tmpValores.Count > 0 && tmpValores != null)
                                    //        {
                                    //            valores.AddRange(tmpValores);
                                    //        }

                                    //    }
                                    //    promQ += valores.Average(x => x.valorMedida);
                                    //    RowValue rowTotal = new RowValue
                                    //    {
                                    //        orderValue = j + 1,
                                    //        idValorMedida = null,
                                    //        idHeader = idHeader,
                                    //        idSubheader = headersTable[j].subheaders[1].idHeader,
                                    //        valorMedida = Math.Round((double)promQ, 2, MidpointRounding.AwayFromZero)
                                    //    };
                                    //    avgRow.Add(rowTotal);
                                    //}
                                }
                                if (bodyTable.Any())
                                {
                                    RowBodyTable roWTotal = new RowBodyTable
                                    {
                                        idHour = (bodyTable.OrderByDescending(x => x.idHour).FirstOrDefault().idHour + 1),
                                        dateHour = "Prom.",
                                        rowValues = avgRow
                                    };

                                    bodyTable.Add(roWTotal);

                                    dataTablaVM tabla = new dataTablaVM
                                    {
                                        idTable = tableCount++,
                                        titleTable = tension.valorTension + " kV",
                                        bodyTable = bodyTable,
                                        headersTable = headersTable
                                    };
                                    tablas.Add(tabla);
                                }


                            }

                        }


                    }
                }
            }
            return tablas;
        }

        public List<dataTablaVM> amperesLineasAnchoris(int idEstacion, DateTime from, DateTime to)
        {
            //  **-- TABLA GROUPED --** //
            List<dataTablaVM> tablas = new List<dataTablaVM>();
            var tensions = _context.HistTension.Where(x => x.activo == true).Where(x => x.idTension == 4).Distinct().ToList();
            //var latTensions = _context.HistEquipo.Where(x => x.idEstacion == 6).Distinct().ToList();
            if (tensions.Any())
            {
                var idTipoLATs = 2;
                var idTipoBarras = 4;
                var idMedidaP = 1;
                var idMedidaQ = 2;
                var idMedidaT = 4;
                int tableCount = 1;

                double mayorValorBarras = 0;
                double resultado = 0;

                foreach (var tension in tensions)
                {
                    var tmpLATs = equipoTensionMedidaQuery(idEstacion, idTipoLATs, tension.idTension, idMedidaP).ToList();
                    var tmpBarras = equipoTensionMedidaQuery(idEstacion, idTipoBarras, tension.idTension, idMedidaP).ToList();

                    if (tmpLATs.Any() || tmpBarras.Any())
                    {
                        List<HeaderTable>? headersTable = new List<HeaderTable>();

                        int idHeaderCount = 1;
                        HeaderTable headerHora = new HeaderTable
                        {
                            idHeader = idHeaderCount++,
                            headerTitle = "Hora"
                        };
                        headersTable.Add(headerHora);

                        if (tmpLATs.Any())
                        {
                            foreach (var lat in tmpLATs)
                            {
                                if (lat.nombreEquipo == "LAT Cruz De Piedra" ||
                                    lat.nombreEquipo == "LAT Capiz" ||
                                    lat.nombreEquipo == "LAT Bajo Rio Tunuyan")
                                {
                                    HeaderTable headerA = new HeaderTable
                                    {
                                        idHeader = 1,
                                        headerTitle = "amperes"
                                    };
                                    List<HeaderTable>? subheaders = new List<HeaderTable> { headerA };

                                    HeaderTable headerLAT = new HeaderTable
                                    {
                                        idHeader = idHeaderCount++,
                                        idEquipo = lat.idEquipo,
                                        headerTitle = lat.nombreEquipo,
                                        subheaders = subheaders
                                    };
                                    headersTable.Add(headerLAT);
                                }
                            }
                        }
                        if (tmpBarras.Any())
                        {
                            foreach (var barra in tmpBarras)
                            {
                                HeaderTable headerA = new HeaderTable
                                {
                                    idHeader = 1,
                                    headerTitle = "amperes"
                                };
                                List<HeaderTable>? subheaders = new List<HeaderTable> { headerA, };

                                HeaderTable headerBarra = new HeaderTable
                                {
                                    idHeader = idHeaderCount++,
                                    idEquipo = barra.idEquipo,
                                    headerTitle = barra.nombreEquipo,
                                    subheaders = subheaders
                                };
                                headersTable.Add(headerBarra);

                            }
                        }

                        List<RowBodyTable>? bodyTable = new List<RowBodyTable>();

                        if (headersTable.Count > 1)
                        {
                            int countHour = 1;
                            for (DateTime i = from; i <= to; i = i.AddMinutes(5))
                            {
                                List<RowValue>? rowValues = new List<RowValue>();
                                List<RowValue>? rowValues1 = new List<RowValue>();
                                int orderValue = 1;

                                for (int j = 1; j < (headersTable.Count); j++)
                                {
                                    int? idEquipo = headersTable[j].idEquipo;
                                    int? idHeader = headersTable[j].idHeader;

                                    var registrosP = registrosListQuery((int)idEquipo, tension.idTension, idMedidaP, from, to).ToList();
                                    var registrosQ = registrosListQuery((int)idEquipo, tension.idTension, idMedidaQ, from, to).ToList();

                                    //prueba para el calculo
                                    if (registrosP.Any() && registrosQ.Any())
                                    {
                                        ValorVM? valorP = new ValorVM();
                                        ValorVM? valorQ = new ValorVM();
                                        foreach (var registro in registrosP)
                                        {
                                            valorP = (valoresTimeQuery(registro.idRegistroMedida, i, idEquipo)).FirstOrDefault();
                                        }
                                        foreach (var registro2 in registrosQ)
                                        {
                                            valorQ = (valoresTimeQuery(registro2.idRegistroMedida, i, idEquipo)).FirstOrDefault();
                                        }
                                        if (valorP != null && valorQ != null)
                                        {
                                            double sumaCuadrados = ((double)valorP.valorMedida * (double)valorP.valorMedida + (double)valorQ.valorMedida * (double)valorQ.valorMedida);
                                            //en este punto se necesitan los valores de las barra a y b para comparar y ver por cual valor dividir
                                            if (tension.valorTension == "132")
                                            {
                                                var registrosBarraAT = registrosListQuery((int)2012, tension.idTension, idMedidaT, from, to).ToList();
                                                var registrosBarraBT = registrosListQuery((int)2013, tension.idTension, idMedidaT, from, to).ToList();
                                                if (registrosBarraAT != null && registrosBarraBT != null)
                                                {
                                                    //hay que revisar el registrosBarraAT[0] ya que cuando trae mas de un registro puede venir desordenado
                                                    //y esto causa problemas, deberia ver como hacer que use el que coincida con la fecha
                                                    ValorVM? valorBarraA = new ValorVM();
                                                    ValorVM? valorBarraB = new ValorVM();
                                                    foreach (var registro in registrosBarraAT)
                                                    {

                                                        var valor = (valoresTimeQuery(registro.idRegistroMedida, i, idEquipo)).FirstOrDefault();
                                                        if (valor != null) valorBarraA = valor;
                                                    }
                                                    foreach (var registro in registrosBarraBT)
                                                    {
                                                        var valor = (valoresTimeQuery(registro.idRegistroMedida, i, idEquipo)).FirstOrDefault();
                                                        if (valor != null) valorBarraB = valor;
                                                    }
                                                    if (valorBarraA.valorMedida > valorBarraB.valorMedida)
                                                    {
                                                        mayorValorBarras = (double)valorBarraA.valorMedida;
                                                    }
                                                    else
                                                    {
                                                        mayorValorBarras = Math.Round((double)valorBarraB.valorMedida, 2, MidpointRounding.AwayFromZero);
                                                    }
                                                    if (mayorValorBarras != 0) resultado = (Math.Sqrt(sumaCuadrados) * 1000) / (mayorValorBarras * 1.73205);
                                                }

                                            }
                                            else
                                            {
                                                var registrosTLat = registrosListQuery((int)idEquipo, tension.idTension, idMedidaT, from, to).ToList();
                                                ValorVM? valorTLat = new ValorVM();
                                                foreach (var registro in registrosTLat)
                                                {

                                                    var valor = (valoresTimeQuery(registro.idRegistroMedida, i, idEquipo)).FirstOrDefault();
                                                    if (valor != null) valorTLat = valor;
                                                }
                                                if (valorTLat != null && valorTLat.valorMedida != 0) resultado = (Math.Sqrt(sumaCuadrados) * 1000) / ((double)valorTLat.valorMedida * 1.73205);
                                            }


                                            if (valorP != null && valorQ != null)
                                            {
                                                RowValue rowValue = new RowValue
                                                {
                                                    orderValue = orderValue++,
                                                    idHeader = idHeader,
                                                    idSubheader = headersTable[j].subheaders[0].idHeader,
                                                    idValorMedida = (valorP != null) ? valorP.idValorMedida : null,
                                                    valorMedida = (valorP != null) ? Math.Round(resultado, 2, MidpointRounding.AwayFromZero) : null
                                                };
                                                rowValues.Add(rowValue);
                                            }

                                        }
                                    }

                                    //fin de prueba
                                    //if (registrosP.Any() || registrosQ.Any())
                                    //{
                                    //    if (registrosP.Any())
                                    //    {
                                    //        foreach (var registro in registrosP)
                                    //        {
                                    //            var valor = (valoresTimeQuery(registro.idRegistroMedida, i, idEquipo)).FirstOrDefault();
                                    //            if (valor != null)
                                    //            {
                                    //                RowValue rowValue = new RowValue
                                    //                {
                                    //                    orderValue = orderValue++,
                                    //                    idHeader = idHeader,
                                    //                    idSubheader = headersTable[j].subheaders[0].idHeader,
                                    //                    idValorMedida = (valor != null) ? valor.idValorMedida : null,
                                    //                    valorMedida = (valor != null) ? Math.Round((double)valor.valorMedida, 2, MidpointRounding.AwayFromZero) : null
                                    //                };
                                    //                rowValues.Add(rowValue);
                                    //            }


                                    //        }
                                    //    }
                                    //    if (registrosQ.Any())
                                    //    {
                                    //        foreach (var registro in registrosQ)
                                    //        {
                                    //            var valor = (valoresTimeQuery(registro.idRegistroMedida, i, idEquipo)).FirstOrDefault();

                                    //            if (valor != null)
                                    //            {
                                    //                RowValue rowValue = new RowValue
                                    //                {
                                    //                    orderValue = orderValue++,
                                    //                    idHeader = idHeader,
                                    //                    idSubheader = headersTable[j].subheaders[1].idHeader,
                                    //                    idValorMedida = (valor != null) ? valor.idValorMedida : null,
                                    //                    valorMedida = (valor != null) ? Math.Round((double)valor.valorMedida, 2, MidpointRounding.AwayFromZero) : null
                                    //                };
                                    //                rowValues.Add(rowValue);
                                    //            }


                                    //        }
                                    //    }
                                    //}
                                    //else
                                    //{
                                    //    headersTable.RemoveAt(j);
                                    //}

                                }
                                if (rowValues.Any())
                                {
                                    RowBodyTable rowBodyTable = new RowBodyTable
                                    {
                                        idHour = countHour++,
                                        dateHour = (i.ToString("g", CultureInfo.GetCultureInfo("es-AR"))),
                                        rowValues = rowValues
                                    };
                                    bodyTable.Add(rowBodyTable);
                                }

                            }

                            if (headersTable.Count > 1)
                            {
                                List<RowValue>? avgRow = new List<RowValue>();
                                for (int j = 1; j < headersTable.Count; j++)
                                {
                                    int? idEquipo = headersTable[j].idEquipo;
                                    int? idHeader = headersTable[j].idHeader;

                                    var registrosP = registrosListQuery((int)idEquipo, tension.idTension, idMedidaP, from, to).ToList();
                                    var registrosQ = registrosListQuery((int)idEquipo, tension.idTension, idMedidaQ, from, to).ToList();
                                    if (registrosP.Any())
                                    {
                                        double? promP = 0;
                                        List<ValorVM>? valores = new List<ValorVM>();
                                        foreach (var registro in registrosP)
                                        {
                                            var tmpValores = (valoresQuery(registro.idRegistroMedida, registro.fInicio, registro.fFin, idEquipo)).ToList();
                                            if (tmpValores.Count > 0 && tmpValores != null)
                                            {
                                                valores.AddRange(tmpValores);
                                            }

                                        }
                                        promP += valores.Average(x => x.valorMedida);
                                        RowValue rowTotal = new RowValue
                                        {
                                            orderValue = j,
                                            idValorMedida = null,
                                            idHeader = idHeader,
                                            idSubheader = headersTable[j].subheaders[0].idHeader,
                                            valorMedida = Math.Round((double)promP, 2, MidpointRounding.AwayFromZero)
                                        };
                                        avgRow.Add(rowTotal);
                                    }
                                    //if (registrosQ.Any())
                                    //{
                                    //    double? promQ = 0;
                                    //    List<ValorVM>? valores = new List<ValorVM>();
                                    //    foreach (var registro in registrosQ)
                                    //    {
                                    //        var tmpValores = (valoresQuery(registro.idRegistroMedida, registro.fInicio, registro.fFin, idEquipo)).ToList();
                                    //        if (tmpValores.Count > 0 && tmpValores != null)
                                    //        {
                                    //            valores.AddRange(tmpValores);
                                    //        }

                                    //    }
                                    //    promQ += valores.Average(x => x.valorMedida);
                                    //    RowValue rowTotal = new RowValue
                                    //    {
                                    //        orderValue = j + 1,
                                    //        idValorMedida = null,
                                    //        idHeader = idHeader,
                                    //        idSubheader = headersTable[j].subheaders[1].idHeader,
                                    //        valorMedida = Math.Round((double)promQ, 2, MidpointRounding.AwayFromZero)
                                    //    };
                                    //    avgRow.Add(rowTotal);
                                    //}
                                }
                                if (bodyTable.Any())
                                {
                                    RowBodyTable roWTotal = new RowBodyTable
                                    {
                                        idHour = (bodyTable.OrderByDescending(x => x.idHour).FirstOrDefault().idHour + 1),
                                        dateHour = "Prom.",
                                        rowValues = avgRow
                                    };

                                    bodyTable.Add(roWTotal);

                                    dataTablaVM tabla = new dataTablaVM
                                    {
                                        idTable = tableCount++,
                                        titleTable = tension.valorTension + " kV",
                                        bodyTable = bodyTable,
                                        headersTable = headersTable
                                    };
                                    tablas.Add(tabla);
                                }


                            }

                        }


                    }
                }
            }
            return tablas;
        }

        public dataTablaVM porcentajeCargaTrafosTabla(int idEstacion, DateTime from, DateTime to, string tension)
        {
            int idTipoEquipo = 1;
            int idTipoMedida = 10;
            return getTable(idEstacion, from, to, tension, idTipoEquipo, idTipoMedida);
        }
        public dataTablaVM porcentajeCargaATRsTabla(int idEstacion, DateTime from, DateTime to, string tension)
        {
            int idTipoEquipo = 2002;
            int idTipoMedida = 10;
            return getTable(idEstacion, from, to, tension, idTipoEquipo, idTipoMedida);
        }
        public dataTablaVM tensionBarrasTabla(int idEstacion, DateTime from, DateTime to, string tension)
        {
            int idTipoEquipo = 2;
            int idTipoMedida = 4;
            return getTable(idEstacion, from, to, tension, idTipoEquipo, idTipoMedida);
        }
        public List<dataTablaVM> cosenoPhiLineas66ATR13(int idEstacion, DateTime from, DateTime to,Boolean? is5Min)
        {
            //  **-- TABLA SIMPLE --** //
            List<dataTablaVM> tablas = new List<dataTablaVM>();
            try
            {
                var stationExists = _context.HistEstacion.Where(x => x.activo == true && x.idEstacion == idEstacion).FirstOrDefault();
                if (stationExists != null)
                {
                    #region Declaración de constantes
                    int tensionLevelLineas = 3;//66 kV
                    int tensionLevelATRs = 1;//13.2 kV
                    int idMedidaP = 1;
                    int idMedidaQ = 2;
                    int idTipoLineas = 2;
                    int idTipoATR = 2002;
                    int timeSpan = (is5Min != null && (bool)is5Min) ? timeSpan = 5 : timeSpan = 30;
                    #endregion
                     var tensiones = _context.HistTension.Where(x => x.idTension==tensionLevelLineas || x.idTension==tensionLevelATRs).Distinct().ToList();
                    foreach(var tension in tensiones)
                    {

                        #region Declaración de estructuras de tabla
                        List<HeaderTable>? headers = new List<HeaderTable>();
                        List<RowBodyTable>? bodyTable = new List<RowBodyTable>();

                        List<RowValue>? rowvaluesMinMax = new List<RowValue>();
                        List<RowValue>? rowMinMaxDates = new List<RowValue>();

                        int headerCount = 1;
                        int countHour = 0;
                        int tableCount = 0;

                        HeaderTable headerHora = new HeaderTable
                        {
                            idHeader = headerCount++,
                            headerTitle = "Hora"
                        };
                        headers.Add(headerHora);
                        #endregion
                        List<EquipoVM>? lineas = new List<EquipoVM>();
                        List<EquipoVM>? autotrafos = new List<EquipoVM>();
                        if (tension.idTension == tensionLevelLineas) lineas = equipoTensionMedidaQuery(idEstacion, idTipoLineas, tensionLevelLineas, idMedidaP).ToList();
                        if (tension.idTension == tensionLevelATRs) autotrafos = equipoTensionMedidaQuery(idEstacion, idTipoATR, tensionLevelATRs, idMedidaP).ToList();
                        List<EquipoVM>? equipos = new List<EquipoVM>();
                        if (lineas.Any()) equipos.AddRange(lineas);
                        if (autotrafos.Any()) equipos.AddRange(autotrafos);

                        foreach (var equipo in equipos)
                        {
                            string tensionNombre = tension.valorTension +"kV";
                            #region Headers
                            HeaderTable headerEquip = new HeaderTable
                            {
                                idHeader = headerCount++,
                                idEquipo = equipo.idEquipo,
                                headerTitle = equipo.nombreEquipo + $" ({tensionNombre})",
                                subheaders = null
                            };
                            headers.Add(headerEquip);
                            #endregion
                        }
                        if(headers.Count > 1)
                        {
                            for (DateTime i = from; i <= to; i = i.AddMinutes(timeSpan))
                            {
                                List<RowValue>? rowValues = new List<RowValue>();
                                int orderValue = 1;

                                for (int j = 1; j < (headers.Count); j++)
                                {
                                    int? idEquipo = headers[j].idEquipo;
                                    int? idHeader = headers[j].idHeader;

                                    EquipoVM? equipo = consultaEquipo((int)idEquipo).FirstOrDefault();

                                    if(equipo != null)
                                    {
                                        int tensionID = tension.idTension;
                                        var registrosP = registrosListQuery((int)idEquipo, tensionID, idMedidaP, from, to).ToList();
                                        var registrosQ = registrosListQuery((int)idEquipo, tensionID, idMedidaQ, from, to).ToList();


                                        if (registrosP.Any() && registrosQ.Any())
                                        {
                                            List<double> valoresP = new List<double>();
                                            foreach(var registro in registrosP)
                                            {
                                                var valor = (valoresTimeQuery(registro.idRegistroMedida, i, equipo.idEquipo)).FirstOrDefault();
                                                if (valor != null)
                                                {
                                                    valoresP.Add((double)valor.valorMedida);
                                                }
                                            }
                                            List<double> valoresQ = new List<double>();
                                            foreach (var registro in registrosQ)
                                            {
                                                var valor = (valoresTimeQuery(registro.idRegistroMedida, i, equipo.idEquipo)).FirstOrDefault();
                                                if (valor != null)
                                                {
                                                    valoresQ.Add((double)valor.valorMedida);
                                                }
                                            }
                                            double cosenoPHIcalculado = 0;
                                            //double potenciaAparenteScalculado = 0;
                                            if(valoresP.Any() && valoresQ.Any())
                                            {
                                                //potenciaAparenteScalculado = Math.Sqrt(Math.Pow(valoresP.Average(), 2) + Math.Pow(valoresQ.Average(), 2));
                                                //if (potenciaAparenteScalculado != 0) cosenoPHIcalculado = valoresP.Average() / potenciaAparenteScalculado;

                                                //Así es cómo lo resuelven en el archivo histórico(creo) RAIZ(1 / ((AK6 / AJ6) ^ 2 + 1))
                                                if (valoresP.Average() > 0)
                                                {
                                                    var P = valoresP.Average();
                                                    var Q = valoresQ.Average();
                                                    cosenoPHIcalculado = Math.Sqrt(1 / (Math.Pow((Q / P), 2) + 1));
                                                }
                                            }

                                            RowValue rowValue = new RowValue
                                            {
                                                orderValue = orderValue++,
                                                idHeader = idHeader,
                                                valorMedida = Math.Round((double)cosenoPHIcalculado, 3, MidpointRounding.AwayFromZero)
                                            };
                                            rowValues.Add(rowValue);

                                        }
                                        else
                                        {
                                            headers.RemoveAt(j);
                                        }
                                    }
  
                                }
                                if (rowValues.Any())
                                {
                                    RowBodyTable rowBodyTable = new RowBodyTable
                                    {
                                        idHour = countHour++,
                                        dateHour = (i.ToString("g", CultureInfo.GetCultureInfo("es-AR"))),
                                        rowValues = rowValues
                                    };
                                    bodyTable.Add(rowBodyTable);
                                }
                            
                            }

                            if (headers.Count > 1)
                            {
                                List<RowValue>? avgRow = new List<RowValue>();
                                for (int j = 1; j < headers.Count; j++)
                                {
                                    int? idEquipo = headers[j].idEquipo;
                                    int? idHeader = headers[j].idHeader;

                                    EquipoVM? equipo = consultaEquipo((int)idEquipo).FirstOrDefault();

                                    if (equipo != null)
                                    {
                                        int tensionID = (equipo.idTipoEquipo == idTipoLineas) ? tensionLevelLineas : tensionLevelATRs;
                                        var registrosP = registrosListQuery((int)idEquipo, tensionID, idMedidaP, from, to).ToList();
                                        var registrosQ = registrosListQuery((int)idEquipo, tensionID, idMedidaQ, from, to).ToList();

                                        if (registrosP.Any() && registrosQ.Any())
                                        {
                                            List<ValorVM>? valoresP = new List<ValorVM>();
                                            foreach (var registro in registrosP)
                                            {
                                                var tmpValores = (valoresQuery(registro.idRegistroMedida, registro.fInicio, registro.fFin, idEquipo)).ToList();
                                                if (tmpValores.Count > 0 && tmpValores != null)
                                                {
                                                    valoresP.AddRange(tmpValores);
                                                }

                                            }
                                            List<ValorVM>? valoresQ = new List<ValorVM>();
                                            foreach (var registro in registrosQ)
                                            {
                                                var tmpValores = (valoresQuery(registro.idRegistroMedida, registro.fInicio, registro.fFin, idEquipo)).ToList();
                                                if (tmpValores.Count > 0 && tmpValores != null)
                                                {
                                                    valoresQ.AddRange(tmpValores);
                                                }

                                            }
                                            double promP = 0;
                                            promP += (double)valoresP.Average(x => x.valorMedida);
                                            double promQ = 0;
                                            promQ += (double)valoresQ.Average(x => x.valorMedida);

                                            double cosenoPHIavg = 0;
                                            double potenciaAparenteSavg = 0;
                                            if (valoresP.Any() && valoresQ.Any())
                                            {
                                                potenciaAparenteSavg = Math.Sqrt(Math.Pow(promP, 2) + Math.Pow(promQ, 2));
                                                if (potenciaAparenteSavg != 0) cosenoPHIavg = promP / potenciaAparenteSavg;
                                            }

                                            RowValue rowTotal = new RowValue
                                            {
                                                orderValue = j,
                                                idValorMedida = null,
                                                idHeader = idHeader,
                                                valorMedida = Math.Round((double)cosenoPHIavg, 2, MidpointRounding.AwayFromZero)
                                            };
                                            avgRow.Add(rowTotal);
                                        }
                                    }
                                }
                                if (bodyTable.Any())
                                {
                                    RowBodyTable roWTotalAvg = new RowBodyTable
                                    {
                                        idHour = (bodyTable.OrderByDescending(x => x.idHour).FirstOrDefault().idHour + 1),
                                        dateHour = "Prom.",
                                        rowValues = avgRow
                                    };

                                    bodyTable.Add(roWTotalAvg);

                                    dataTablaVM tabla = new dataTablaVM
                                    {
                                        idTable = tableCount++,
                                        titleTable = $"{tension.valorTension}kV",
                                        bodyTable = bodyTable,
                                        headersTable = headers
                                    };
                                    tablas.Add(tabla);
                                } 
                            }
                        }
                    }
                }
                else
                {
                    throw new Exception("La estación no existe");
                }
            return tablas;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public dataTablaVM temperaturaAmbienteTabla(int idEstacion, DateTime from, DateTime to)
        {
            var infoEstacion = _context.HistEstacion.Where(x => x.idEstacion == idEstacion && x.activo == true).FirstOrDefault();
            if (infoEstacion != null)
            {
                TimeSpan interval = to - from;
                if (interval.TotalDays > 3) //si son más de 3 días
                {
                    to = from.AddDays(2).AddHours(23).AddMinutes(59);
                }
               
                int idTipoEq = 1002; //termómetro
                int idTipoMed = 7; //temperatura
                var consultaEquipo = equipoQuery(idEstacion, idTipoEq).FirstOrDefault();
                if (consultaEquipo != null)
                {
                    List<HeaderTable>? headersTable = new List<HeaderTable>();
                    {
                        HeaderTable headerHora = new HeaderTable
                        {
                            idHeader = 1,
                            headerTitle = "Hora"
                        };
                        headersTable.Add(headerHora);
                        HeaderTable headerTemp = new HeaderTable
                        {
                            idHeader = 2,
                            headerTitle = "°C"
                        };
                        headersTable.Add(headerTemp);

                    }
                    List<double?> allvalues = new List<double?>();
                    
                    List<RowBodyTable>? bodyTable = new List<RowBodyTable>();
                    {
                        int countHour = 1;
                        for (DateTime i = from; i <= to; i = i.AddMinutes(5))
                        {
                            List<RowValue>? rowValues = new List<RowValue>();
                            int orderValue = 1;

                            var registrosList = registroQuery(idTipoMed, from, to, consultaEquipo.idEquipo).ToList();

                            if (registrosList.Any())
                            {
                                foreach (var registro in registrosList)
                                {
                                    var valor = (valoresTimeQuery(registro.idRegistroMedida, i, consultaEquipo.idEquipo)).FirstOrDefault();
                                    allvalues.Add(Math.Round((double)valor.valorMedida, 2, MidpointRounding.AwayFromZero));

                                        RowValue rowValue = new RowValue
                                        {
                                            orderValue = orderValue,
                                            idValorMedida = valor.idValorMedida,
                                            valorMedida = (valor != null) ? Math.Round((double)valor.valorMedida, 2, MidpointRounding.AwayFromZero) : null
                                        };
                                        rowValues.Add(rowValue);
    
                                }
                                orderValue++;
                            }
                            RowBodyTable rowBodyTable = new RowBodyTable
                            {
                                idHour = countHour,
                                dateHour = (i.ToString("g", CultureInfo.GetCultureInfo("es-AR"))),
                                rowValues = rowValues
                            };
                            bodyTable.Add(rowBodyTable);

                            countHour++;
                        }
                        RowValue totalSumValue = new RowValue
                        {
                            orderValue = 1,
                            idValorMedida = null,
                            valorMedida = allvalues.Average(),
                        };

                        List<RowValue>? totalValues = new List<RowValue>{ totalSumValue };
                        RowBodyTable rowTotal = new RowBodyTable
                        {
                            idHour = countHour,
                            dateHour = "Prom.",
                            rowValues = totalValues
                        };
                        bodyTable.Add(rowTotal);

                    }

                    dataTablaVM dataTablaVM = new dataTablaVM
                    {
                        titleTable = "Temperatura ambiente",
                        headersTable = headersTable,
                        bodyTable = bodyTable
                    };

                return dataTablaVM;
                }
            }
            return null;
        }
        public dataTablaVM demandaTotalTabla(int idEstacion, DateTime from, DateTime to)
        {
            var infoEstacion = _context.HistEstacion.Where(x => x.idEstacion == idEstacion && x.activo == true).FirstOrDefault();
            if (infoEstacion != null)
            {
                TimeSpan interval = to - from;
                if (interval.TotalDays > 3) //si son más de 3 días
                {
                    to = from.AddDays(2).AddHours(23).AddMinutes(59);
                }

                List<HeaderTable>? headersTable = new List<HeaderTable>();
                {
                    HeaderTable headerHora = new HeaderTable
                    {
                        idHeader = 1,
                        headerTitle = "Hora"
                    };
                    headersTable.Add(headerHora);
                    HeaderTable headerP = new HeaderTable
                    {
                        idHeader = 2,
                        headerTitle = "P (MW)"
                    };
                    headersTable.Add(headerP);
                    HeaderTable headerQ = new HeaderTable
                    {
                        idHeader = 3,
                        headerTitle = "Q (Mvar)"
                    };
                    headersTable.Add(headerQ);
                    HeaderTable headerS = new HeaderTable
                    {
                        idHeader = 4,
                        headerTitle = "(MVA)"
                    };
                    headersTable.Add(headerS);
                }

                int idTipoEq = 0;
                int idMedidaS = 3;
                int idMedidaP = 1;
                int idMedidaQ = 2;
                var countTrafos = (equipoQuery(idEstacion, 1)).Count();
                var countATR = (equipoQuery(idEstacion, 2002)).Count();
                if (countTrafos != 0)
                {
                    idTipoEq = 1;
                }
                else if (countATR != 0)
                {
                    idTipoEq = 2002;
                }

                if (idTipoEq != 0)
                {
                    var tmpTrafos = (equipoQuery(idEstacion, idTipoEq)).ToList();

                    List<ValorVM>? tmpValuesS = new List<ValorVM>();
                    List<ValorVM>? tmpValuesP = new List<ValorVM>();
                    List<ValorVM>? tmpValuesQ = new List<ValorVM>();

                    List<double?> promS = new List<double?>();
                    List<double?> promP = new List<double?>();
                    List<double?> promQ = new List<double?>();

                    foreach (var equipo in tmpTrafos)
                    {
                        var puntoScadaPotenciaS = (from eq in _context.HistEquipo
                                                   join puntoscada in _context.HistPuntoScada
                                                   on equipo.idEquipo equals puntoscada.idEquipo
                                                   join registromedida in _context.HistRegistroMedida
                                                   on puntoscada.idPuntoScada equals registromedida.idPuntoScada
                                                   where eq.idEquipo == equipo.idEquipo && eq.activo == true
                                                   && puntoscada.activo == true && registromedida.idTipoMedida == idMedidaS
                                                   && registromedida.fInicio >= @from && registromedida.fFin <= @to && registromedida.activo == true
                                                   select new PuntoScadaVM
                                                   {
                                                       idPuntoScada = puntoscada.idPuntoScada,
                                                       nroScada = puntoscada.nroPuntoScada,
                                                       fechaCreacionPunto = puntoscada.fechaCreacionPunto,
                                                       fechaFinPunto = puntoscada.fechaFinPunto,
                                                       activo = puntoscada.activo,
                                                       idEquipo = puntoscada.idEquipo,
                                                       idEstacion = eq.idEstacion,
                                                       idTension = puntoscada.idTension,
                                                       lado = puntoscada.lado,
                                                   }).Distinct().OrderByDescending(x => x.idTension).FirstOrDefault();

                        var puntoScadaPotenciaP = (from eq in _context.HistEquipo
                                                   join puntoscada in _context.HistPuntoScada
                                                   on equipo.idEquipo equals puntoscada.idEquipo
                                                   join registromedida in _context.HistRegistroMedida
                                                   on puntoscada.idPuntoScada equals registromedida.idPuntoScada
                                                   where eq.idEquipo == equipo.idEquipo && eq.activo == true
                                                   && puntoscada.activo == true && registromedida.idTipoMedida == idMedidaP
                                                   && registromedida.fInicio >= @from && registromedida.fFin <= @to && registromedida.activo == true
                                                   select new PuntoScadaVM
                                                   {
                                                       idPuntoScada = puntoscada.idPuntoScada,
                                                       nroScada = puntoscada.nroPuntoScada,
                                                       fechaCreacionPunto = puntoscada.fechaCreacionPunto,
                                                       fechaFinPunto = puntoscada.fechaFinPunto,
                                                       activo = puntoscada.activo,
                                                       idEquipo = puntoscada.idEquipo,
                                                       idEstacion = eq.idEstacion,
                                                       idTension = puntoscada.idTension,
                                                       lado = puntoscada.lado,
                                                   }).Distinct().OrderByDescending(x => x.idTension).FirstOrDefault();

                        var puntoScadaPotenciaQ = (from eq in _context.HistEquipo
                                                   join puntoscada in _context.HistPuntoScada
                                                   on equipo.idEquipo equals puntoscada.idEquipo
                                                   join registromedida in _context.HistRegistroMedida
                                                   on puntoscada.idPuntoScada equals registromedida.idPuntoScada
                                                   where eq.idEquipo == equipo.idEquipo && eq.activo == true
                                                   && puntoscada.activo == true && registromedida.idTipoMedida == idMedidaQ
                                                   && registromedida.fInicio >= @from && registromedida.fFin <= @to && registromedida.activo == true
                                                   select new PuntoScadaVM
                                                   {
                                                       idPuntoScada = puntoscada.idPuntoScada,
                                                       nroScada = puntoscada.nroPuntoScada,
                                                       fechaCreacionPunto = puntoscada.fechaCreacionPunto,
                                                       fechaFinPunto = puntoscada.fechaFinPunto,
                                                       activo = puntoscada.activo,
                                                       idEquipo = puntoscada.idEquipo,
                                                       idEstacion = eq.idEstacion,
                                                       idTension = puntoscada.idTension,
                                                       lado = puntoscada.lado,
                                                   }).Distinct().OrderByDescending(x => x.idTension).FirstOrDefault();
                        if (puntoScadaPotenciaS != null)
                        {
                            var valuesS = (from valor in _context.HistValorMedida
                                           join registrosmedida in _context.HistRegistroMedida
                                           on valor.idRegistroMedida equals registrosmedida.idRegistroMedida
                                           join puntoscada in _context.HistPuntoScada
                                           on registrosmedida.idPuntoScada equals puntoscada.idPuntoScada

                                           where registrosmedida.idTipoMedida == idMedidaS
                                               && registrosmedida.fInicio >= @from && registrosmedida.fFin <= @to && registrosmedida.activo == true
                                               && registrosmedida.idPuntoScada == puntoScadaPotenciaS.idPuntoScada
                                           select new ValorVM
                                           {
                                               idValorMedida = valor.idValorMedida,
                                               valorMedida = valor.valorMedida,
                                               horaMedida = valor.horaMedida,
                                               activo = true,
                                           }).Distinct().ToList();

                            tmpValuesS.AddRange(valuesS);
                        }
                        if (puntoScadaPotenciaP != null)
                        {
                            var valuesP = (from valor in _context.HistValorMedida
                                           join registrosmedida in _context.HistRegistroMedida
                                               on valor.idRegistroMedida equals registrosmedida.idRegistroMedida
                                           join puntoscada in _context.HistPuntoScada
                                               on registrosmedida.idPuntoScada equals puntoscada.idPuntoScada

                                           where registrosmedida.idTipoMedida == idMedidaP
                                               && registrosmedida.fInicio >= @from && registrosmedida.fFin <= @to && registrosmedida.activo == true
                                               && registrosmedida.idPuntoScada == puntoScadaPotenciaP.idPuntoScada
                                           select new ValorVM
                                           {
                                               idValorMedida = valor.idValorMedida,
                                               valorMedida = valor.valorMedida,
                                               horaMedida = valor.horaMedida,
                                               activo = true,
                                           }).Distinct().ToList();

                            tmpValuesP.AddRange(valuesP);
                        }
                        if (puntoScadaPotenciaQ != null)
                        {
                            var valuesQ = (from valor in _context.HistValorMedida
                                           join registrosmedida in _context.HistRegistroMedida
                                               on valor.idRegistroMedida equals registrosmedida.idRegistroMedida
                                           join puntoscada in _context.HistPuntoScada
                                               on registrosmedida.idPuntoScada equals puntoscada.idPuntoScada

                                           where registrosmedida.idTipoMedida == idMedidaQ
                                               && registrosmedida.fInicio >= @from && registrosmedida.fFin <= @to && registrosmedida.activo == true
                                               && registrosmedida.idPuntoScada == puntoScadaPotenciaQ.idPuntoScada
                                           select new ValorVM
                                           {
                                               idValorMedida = valor.idValorMedida,
                                               valorMedida = valor.valorMedida,
                                               horaMedida = valor.horaMedida,
                                               activo = true,
                                           }).Distinct().ToList();

                            tmpValuesQ.AddRange(valuesQ);
                        }
                    }

                    List<RowBodyTable>? bodyTable = new List<RowBodyTable>();
                    int countHour = 1;
                    for (DateTime i = from; i <= to; i = i.AddMinutes(5))
                    {
                        List<RowValue>? rowValues = new List<RowValue>();
                        int orderValue = 1;

                        //P
                        var existP = tmpValuesP.Where(y => y.horaMedida == i).ToList();
                        double? valorP = existP?.Sum(x => x.valorMedida).Value;
                        if (valorP != null)
                        {
                            promP.Add(valorP); ;
                            RowValue rowValue = new RowValue
                            {
                                orderValue = orderValue++,
                                idValorMedida = null,
                                valorMedida = Math.Round((double)valorP, 2, MidpointRounding.AwayFromZero)
                            };
                            rowValues.Add(rowValue);
                        }
                        //Q
                        var existQ = tmpValuesQ.Where(y => y.horaMedida == i).ToList();
                        double? valorQ = existQ?.Sum(x => x.valorMedida).Value;
                        if (valorQ != null)
                        {
                            promQ.Add(valorQ);
                            RowValue rowValue = new RowValue
                            {
                                orderValue = orderValue++,
                                idValorMedida = null,
                                valorMedida = Math.Round((double)valorQ, 2, MidpointRounding.AwayFromZero)
                            };
                            rowValues.Add(rowValue);
                        }
                        //S
                        var existS = tmpValuesS.Where(y => y.horaMedida == i).ToList();
                        double? valorS = existS?.Sum(x => x.valorMedida).Value;
                        if (valorS != null)
                        {
                            promS.Add(valorS);
                            RowValue rowValue = new RowValue
                            {
                                orderValue = orderValue++,
                                idValorMedida = null,
                                valorMedida = Math.Round((double)valorS, 2, MidpointRounding.AwayFromZero)
                            };
                            rowValues.Add(rowValue);
                        }

                        RowBodyTable rowBodyTable = new RowBodyTable
                        {
                            idHour = countHour,
                            dateHour = (i.ToString("g", CultureInfo.GetCultureInfo("es-AR"))),
                            rowValues = rowValues
                        };
                        bodyTable.Add(rowBodyTable);
                        countHour++;
                    }

                    List<RowValue> totalValues = new List<RowValue>();
                    {
                        RowValue totalP = new RowValue
                        {
                            orderValue = 1,
                            idValorMedida = null,
                            valorMedida = Math.Round((double) promP.Average(), 2, MidpointRounding.AwayFromZero)
                        };
                        totalValues.Add(totalP);
                        RowValue totalQ = new RowValue
                        {
                            orderValue = 2,
                            idValorMedida = null,
                            valorMedida = Math.Round((double)promQ.Average(), 2, MidpointRounding.AwayFromZero)
                        };
                        totalValues.Add(totalQ);
                        RowValue totalS = new RowValue
                        {
                            orderValue = 3,
                            idValorMedida = null,
                            valorMedida = Math.Round((double)promS.Average(), 2, MidpointRounding.AwayFromZero)
                        };
                        totalValues.Add(totalS);
                    }
                    
                    RowBodyTable totalRow = new RowBodyTable
                    {
                        idHour = countHour,
                        dateHour = "Prom.",
                        rowValues = totalValues
                    };
                    bodyTable.Add(totalRow);

                    dataTablaVM dataTablaVM = new dataTablaVM
                    {
                        titleTable = "Demanda total E.T. ",
                        headersTable = headersTable,
                        bodyTable = bodyTable
                    };

                    return dataTablaVM;

                }
            }

            return null;
        }
        public dataTablaVM maximosminimosTRsATRs(int idEstacion, DateTime from, DateTime to)
        {
            try
            {
                var infoEstacion = _context.HistEstacion.Where(x => x.idEstacion == idEstacion && x.activo == true).FirstOrDefault();
                if (infoEstacion != null)
                {
                    TimeSpan interval = to - from;
                    if (interval.TotalDays > 3) //si son más de 3 días
                    {
                        to = from.AddDays(2).AddHours(23).AddMinutes(59);
                    }

                    var idTipoMedida = 3;
                    var tmpAtrs = (equipoQuery(idEstacion, 2002)).ToList();
                    var tmpTrafos = (equipoQuery(idEstacion, 1)).ToList();


                    if (tmpTrafos.Any() || tmpAtrs.Any())
                    {
                        List<EquipoVM> equipos = new List<EquipoVM>();
                        equipos.AddRange(tmpTrafos);
                        equipos.AddRange(tmpAtrs);

                        List<HeaderTable>? headers = new List<HeaderTable>();
                        List<HeaderTable>? subheaders = new List<HeaderTable>();
                        List<RowBodyTable>? bodyTable = new List<RowBodyTable>();

                        List<RowValue>? rowvaluesMinMax = new List<RowValue>();
                        List<RowValue>? rowMinMaxDates = new List<RowValue>();

                        int headerCount = 0;
                        int subheaderCount = 0;
                        int horaCount = 0;
                        foreach (var trafo in equipos)
                        {
                            { //Headers
                                HeaderTable headerEquipo = new HeaderTable
                                {
                                    idHeader = headerCount++,
                                    headerTitle = trafo.nombreEquipo,
                                    idEquipo = trafo.idEquipo,
                                    hasSide = false
                                };
                                headers.Add(headerEquipo);

                                HeaderTable max = new HeaderTable
                                {
                                    idHeader = subheaderCount++,
                                    headerTitle = "Máxima",
                                    idEquipo = trafo.idEquipo,
                                    hasSide = false
                                };
                                subheaders.Add(max);

                                HeaderTable min = new HeaderTable
                                {
                                    idHeader = subheaderCount++,
                                    headerTitle = "Mínima",
                                    idEquipo = trafo.idEquipo,
                                    hasSide = false
                                };
                                subheaders.Add(min);
                            }

                            var registrosList = registroQuery(idTipoMedida, from, to, trafo.idEquipo).ToList();
                            List<ValorVM>? allRegistros = new List<ValorVM>();
                            foreach (var registro in registrosList)
                            {
                                var tmpValores = (valoresQuery(registro.idRegistroMedida, registro.fInicio, registro.fFin, trafo.idEquipo)).ToList();
                                allRegistros.AddRange(tmpValores);
                            }

                            var minValue = allRegistros.Min(x => x.valorMedida).Value;
                            var maxValue = allRegistros.Max(x => x.valorMedida).Value;

                            RowValue rowMax = new RowValue
                            {
                                orderValue = 1,
                                idValorMedida = null,
                                valorMedida = Math.Round((double)maxValue, 2, MidpointRounding.AwayFromZero)
                            };
                            RowValue rowMin = new RowValue
                            {
                                orderValue = 2,
                                idValorMedida = null,
                                valorMedida = Math.Round((double)minValue, 2, MidpointRounding.AwayFromZero)
                            };
                            rowvaluesMinMax.Add(rowMax);
                            rowvaluesMinMax.Add(rowMin);



                            DateTime minValueTime = (DateTime)allRegistros.Where(x => x.valorMedida == minValue).FirstOrDefault().horaMedida;
                            DateTime maxValueTime = (DateTime)allRegistros.Where(x => x.valorMedida == maxValue).FirstOrDefault().horaMedida;

                            var fechaHoraMax = maxValueTime.ToString("g", CultureInfo.GetCultureInfo("es-AR")).Split(" ");
                            var fechaHoraMin = minValueTime.ToString("g", CultureInfo.GetCultureInfo("es-AR")).Split(" ");

                            var fechaMAX = fechaHoraMax[0].Split("/");
                            var horaMAX = fechaHoraMax[1];
                            var stringHoraMax = fechaMAX[0] + '/' + fechaMAX[1] + ' ' + horaMAX; //quedaría 31/10 17:04

                            var fechaMIN = fechaHoraMin[0].Split("/");
                            var horaMIN = fechaHoraMin[1];
                            var stringHoraMin = fechaMIN[0] + '/' + fechaMIN[1] + ' ' + horaMIN; //quedaría 31/10 17:04


                            RowValue rowMaxDate = new RowValue
                            {
                                orderValue = 1,
                                dateHour = stringHoraMax

                            };
                            RowValue rowMinDate = new RowValue
                            {
                                orderValue = 2,
                                dateHour = stringHoraMin
                            };
                            rowMinMaxDates.Add(rowMaxDate);
                            rowMinMaxDates.Add(rowMinDate);

                        }
                        RowBodyTable horaRow = new RowBodyTable
                        {
                            idHour = horaCount++,
                            rowValues = rowMinMaxDates
                        };
                        bodyTable.Add(horaRow);

                        RowBodyTable minmaxRow = new RowBodyTable
                        {
                            idHour = horaCount++,
                            rowValues = rowvaluesMinMax
                        };
                        bodyTable.Add(minmaxRow);

                        dataTablaVM tablaMinMax = new dataTablaVM
                        {
                            titleTable = "Carga de Trafos",
                            headersTable = headers,
                            subheaderTables = subheaders,
                            bodyTable = bodyTable
                        };

                        return tablaMinMax;

                    }
                }

                return null;
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
            
        }
        private dataTablaVM getTable(int idEstacion, DateTime from, DateTime to, string tension, int idTipoEquipo, int idTipoMedida)
        {
            TimeSpan interval = to - from;
            if (interval.TotalDays > 3) //si son más de 3 días
            {
                to = from.AddDays(2).AddHours(23).AddMinutes(59);
            }
            HistTension? tensionInfo = _context.HistTension.Where(x => x.activo == true && x.valorTension.Contains(tension)).FirstOrDefault();
            if(tensionInfo != null)
            {
                List<HeaderTable>? headersTable = new List<HeaderTable>();

                if(idTipoEquipo == 2 || idTipoEquipo == 4)
                {
                    List<HeaderTable> barrasHeaders = getTableHeaders(from, to, idEstacion, 4, idTipoMedida, tensionInfo.idTension);
                    List<HeaderTable> lineasHeaders = getTableHeaders(from, to, idEstacion, 2, idTipoMedida, tensionInfo.idTension);
                    if (barrasHeaders.Any())
                    {
                        lineasHeaders.RemoveAt(0);
                        headersTable.AddRange(barrasHeaders);
                        headersTable.AddRange(lineasHeaders);
                    }
                    else if (lineasHeaders.Any())
                    {
                        headersTable.AddRange(lineasHeaders);
                    }
                    
                    
                }
                else
                {
                    headersTable = getTableHeaders(from, to, idEstacion, idTipoEquipo, idTipoMedida, tensionInfo.idTension);
                }

                List<RowBodyTable>? bodyTable = getTableBody(from, to, headersTable, tensionInfo.idTension, idTipoMedida);

                dataTablaVM dataTablaVM = new dataTablaVM
                {
                    titleTable = tensionInfo.valorTension + " kV",
                    headersTable = headersTable,
                    bodyTable = bodyTable
                };

                return dataTablaVM;

            }

            return null;
        }
        private List<HeaderTable> getTableHeaders (DateTime from, DateTime to,int idEstacion, int idTipoEquipo, int idTipoMedida, int idTension)
        {
            List<HeaderTable>? headersTable = new List<HeaderTable>();
            int headerCount = 1;
            HeaderTable headerHora = new HeaderTable
            {
                idHeader = headerCount,
                headerTitle = "Hora"
            };
            headersTable.Add(headerHora);
            headerCount++;
            var consultaEquipos = equipoTensionMedidaQuery(idEstacion, idTipoEquipo, idTension, idTipoMedida).ToList();

            foreach (var equipo in consultaEquipos)
            {
                var registrosList = registrosListQuery(equipo.idEquipo, idTension, idTipoMedida, from, to).ToList();

                if (registrosList.Any())
                {
                    var registrosLado = registrosList.Where(x => x.lado != null).Distinct().ToList();
                    var registrosSinLado = registrosList.Where(x => x.lado == null).Distinct().ToList();
                    if (registrosLado.Any())
                    {
                        HeaderTable headerEquipo = new HeaderTable
                        {
                            idHeader = headerCount++,
                            headerTitle = equipo.nombreEquipo + " Lado " + registrosLado.Select(x => x.lado).FirstOrDefault(),
                            idEquipo = equipo.idEquipo,
                            hasSide = true,
                        };
                        headersTable.Add(headerEquipo);
                    }
                    if (registrosSinLado.Any())
                    {
                        HeaderTable headerEquipo = new HeaderTable
                        {
                            idHeader = headerCount++,
                            headerTitle = equipo.nombreEquipo,
                            idEquipo = equipo.idEquipo,
                            hasSide = false
                        };
                        headersTable.Add(headerEquipo);
                    }
                }
            }
            return headersTable;

        }
        private List<RowBodyTable> getTableBody(DateTime from, DateTime to, List<HeaderTable> headersTable, int idTension, int idTipoMedida)
        {
            List<RowBodyTable>? bodyTable = new List<RowBodyTable>();
            int countHour = 1;

            for (DateTime i = from; i <= to; i = i.AddMinutes(5))
            {
                List<RowValue>? rowValues = new List<RowValue>();
                int orderValue = 1;

                for (int j = 1; j < (headersTable.Count ); j++)
                {
                    int? idEquipo = headersTable[j].idEquipo;
                    bool? hasSide = headersTable[j].hasSide;

                    var registrosList = registrosListQuery((int)idEquipo, idTension, idTipoMedida, from, to).ToList();
                    if ((bool)hasSide)
                    {
                        var registrosLado = registrosList.Where(x => x.lado != null).Distinct().ToList();
                        if (registrosLado.Any())
                        {
                            foreach (var registro in registrosLado)
                            {
                                var valor = (valoresTimeQuery(registro.idRegistroMedida, i, idEquipo)).FirstOrDefault();

                                    RowValue rowValue = new RowValue
                                    {
                                        orderValue = orderValue,
                                        idValorMedida = valor.idValorMedida,
                                        valorMedida = (valor != null) ? Math.Round((double)valor.valorMedida, 2, MidpointRounding.AwayFromZero) : null
                                    };
                                    rowValues.Add(rowValue);
                            }
                        }
                        orderValue++;
                    }
                    else
                    {
                        var registrosSinLado = registrosList.Where(x => x.lado == null).Distinct().ToList();
                        if (registrosSinLado.Any())
                        {
                            foreach (var registro in registrosSinLado)
                            {
                                var valor = (valoresTimeQuery(registro.idRegistroMedida, i, idEquipo)).FirstOrDefault();

                                    RowValue rowValue = new RowValue
                                    {
                                        orderValue = orderValue,
                                        idValorMedida = valor.idValorMedida,
                                        valorMedida = (valor != null) ? Math.Round((double)valor.valorMedida, 2, MidpointRounding.AwayFromZero) : null
                                    };
                                    rowValues.Add(rowValue);

                                
                            }
                            orderValue++;
                        }
                    }
                }

                RowBodyTable rowBodyTable = new RowBodyTable
                {
                    idHour = countHour,
                    dateHour = (i.ToString("g", CultureInfo.GetCultureInfo("es-AR"))),
                    rowValues = rowValues
                };
                bodyTable.Add(rowBodyTable);

                countHour++;
            }

            List<RowValue>? rowvalueTotal = new List<RowValue>();
            
            //Esto es para el promedio
            for (int j = 1; j < (headersTable.Count); j++)
            {
                int? idEquipo = headersTable[j].idEquipo;
                bool? hasSide = headersTable[j].hasSide;

                var registrosList = registrosListQuery((int)idEquipo, idTension, idTipoMedida, from, to).ToList();
                if ((bool)hasSide)
                {
                    var registrosLado = registrosList.Where(x => x.lado != null).Distinct().ToList();
                    if (registrosLado.Any())
                    {
                       
                        double? prom = 0;
                        List<ValorVM>? valores = new List<ValorVM>();
                        foreach (var registro in registrosLado)
                        {
                            var tmpValores = (valoresQuery(registro.idRegistroMedida, registro.fInicio, registro.fFin, idEquipo)).ToList();
                            if (tmpValores.Count > 0 && tmpValores != null)
                            {
                                valores.AddRange(tmpValores);
                            }
                        }
                        prom += valores.Average(x => x.valorMedida);
                        RowValue rowTotal = new RowValue
                        {
                            orderValue = j,
                            idValorMedida = null,
                            valorMedida = Math.Round((double)prom, 2, MidpointRounding.AwayFromZero)
                        };
                        rowvalueTotal.Add(rowTotal);
                    }
                    
                }
                else
                {
                    var registrosSinLado = registrosList.Where(x => x.lado == null).Distinct().ToList();
                    if (registrosSinLado.Any())
                    {
                        double? prom = 0;
                        List<ValorVM>? valores = new List<ValorVM>();
                        foreach (var registro in registrosSinLado)
                        {
                            var tmpValores = (valoresQuery(registro.idRegistroMedida, registro.fInicio, registro.fFin, idEquipo)).ToList();
                            if (tmpValores.Count > 0 && tmpValores != null)
                            {
                                valores.AddRange(tmpValores);
                            }
                        }
                        prom += valores.Average(x => x.valorMedida);
                        RowValue rowTotal = new RowValue
                        {
                            orderValue = j,
                            idValorMedida = null,
                            valorMedida = Math.Round((double)prom, 2, MidpointRounding.AwayFromZero)
                        };
                        rowvalueTotal.Add(rowTotal);

                    }
                }
            }

            RowBodyTable roWTotal = new RowBodyTable
            {
                idHour = (bodyTable.OrderByDescending(x=>x.idHour).FirstOrDefault().idHour +1),
                dateHour = "Prom.",
                rowValues = rowvalueTotal
            };
            bodyTable.Add(roWTotal);

            return bodyTable;
        }
        private dataTablaVM getTableTrAtr(int idEstacion, DateTime from, DateTime to, string tension, int idTipoEquipo)
        {
            TimeSpan interval = to - from;
            if (interval.TotalDays > 3) //si son más de 3 días
            {
                to = from.AddDays(2).AddHours(23).AddMinutes(59);
            }
            HistTension? tensionInfo = _context.HistTension.Where(x => x.activo == true && x.valorTension.Contains(tension)).FirstOrDefault();
            if (tensionInfo != null)
            {
                List<HeaderTable>? headersTable = new List<HeaderTable>();

                headersTable = getHeadersTrAtrs(from, to, idEstacion, idTipoEquipo, tensionInfo.idTension);


                List<RowBodyTable>? bodyTable = getBodyTrAtr(from, to, headersTable, tensionInfo.idTension);

                dataTablaVM dataTablaVM = new dataTablaVM
                {
                    titleTable = tensionInfo.valorTension + " kV",
                    headersTable = headersTable,
                    bodyTable = bodyTable
                };

                return dataTablaVM;

            }
            return null;
        }
        private List<HeaderTable> getHeadersTrAtrs(DateTime from, DateTime to, int idEstacion, int idTipoEquipo,  int idTension)
        {
            List<HeaderTable>? headersTable = new List<HeaderTable>();

            int headerCount = 1;
            HeaderTable headerHora = new HeaderTable
            {
                idHeader = headerCount,
                headerTitle = "Hora"
            };
            headersTable.Add(headerHora);
            headerCount++;

            int idMedidaP = 1;
            int idMedidaQ = 2;
            int idMedidaMVA = 3;

            int idSubheader = 1;

            var equiposConMVAxTension = equipoTensionMedidaQuery(idEstacion, idTipoEquipo, idTension, idMedidaMVA).ToList();

            foreach (var equipo in equiposConMVAxTension)
            {
                
                var registrosList = registrosListQuery(equipo.idEquipo, idTension, 3, from, to).ToList();

                if (registrosList.Any())
                {
                    var registrosLado = registrosList.Where(x => x.lado != null && x.lado != string.Empty).Distinct().ToList();
                    var registrosSinLado = registrosList.Where(x => x.lado == null || x.lado == string.Empty).Distinct().ToList();
                    if (registrosLado.Any())
                    {
                        List<HeaderTable>? subheaders = new List<HeaderTable>();
                        {
                            HeaderTable subheaderP = new HeaderTable
                            {
                                idHeader = idSubheader++,
                                headerTitle = "P (MW)",
                                idEquipo = equipo.idEquipo,
                                hasSide = true
                            };
                            HeaderTable subheaderQ = new HeaderTable
                            {
                                idHeader = idSubheader++,
                                headerTitle = "Q (Mvar)",
                                idEquipo = equipo.idEquipo,
                                hasSide = true
                            };
                            HeaderTable subheaderMVA = new HeaderTable
                            {
                                idHeader = idSubheader++,
                                headerTitle = "(MVA)",
                                idEquipo = equipo.idEquipo,
                                hasSide = true
                            };
                            subheaders.Add(subheaderP);
                            subheaders.Add(subheaderQ);
                            subheaders.Add(subheaderMVA);

                        };
                        string? lado = registrosLado.Select(x => x.lado).FirstOrDefault();
                        string? headerTitleIfLado = (lado != null && lado != string.Empty && lado.Contains("lado", StringComparison.OrdinalIgnoreCase)) 
                                    ? string.Empty : " Lado " + lado;
                        HeaderTable headerEquipo = new HeaderTable
                        {
                            idHeader = headerCount++,
                            headerTitle = equipo.nombreEquipo + headerTitleIfLado,
                            idEquipo = equipo.idEquipo,
                            hasSide = true,
                            subheaders = subheaders
                        };
                        headersTable.Add(headerEquipo);
                    }
                    if (registrosSinLado.Any())
                    {
                        List<HeaderTable>? subheaders = new List<HeaderTable>();
                        {
                            HeaderTable subheaderP = new HeaderTable
                            {
                                idHeader = idSubheader++,
                                headerTitle = "P (MW)",
                                idEquipo = equipo.idEquipo,
                                hasSide = false
                            };
                            HeaderTable subheaderQ = new HeaderTable
                            {
                                idHeader = idSubheader++,
                                headerTitle = "Q (Mvar)",
                                idEquipo = equipo.idEquipo,
                                hasSide = false
                            };
                            HeaderTable subheaderMVA = new HeaderTable
                            {
                                idHeader = idSubheader++,
                                headerTitle = "(MVA)",
                                idEquipo = equipo.idEquipo,
                                hasSide = false
                            };
                            subheaders.Add(subheaderP);
                            subheaders.Add(subheaderQ);
                            subheaders.Add(subheaderMVA);

                        };
                        HeaderTable headerEquipo = new HeaderTable
                        {
                            idHeader = headerCount++,
                            headerTitle = equipo.nombreEquipo,
                            idEquipo = equipo.idEquipo,
                            hasSide = false,
                            subheaders = subheaders
                        };
                        headersTable.Add(headerEquipo);
                    }
                }
            }
            return headersTable;

        }
        private List<RowBodyTable> getBodyTrAtr(DateTime from, DateTime to, List<HeaderTable> headersTable, int idTension)
        {
            List<RowBodyTable>? bodyTable = new List<RowBodyTable>();
            int countHour = 1;

            int idMedidaP = 1;
            int idMedidaQ = 2;
            int idMedidaMVA = 3;

            for (DateTime i = from; i <= to; i = i.AddMinutes(5))
            {
                List<RowValue>? rowValues = new List<RowValue>();
                int orderValue = 1;

                for (int j = 1; j < (headersTable.Count ); j++)
                {
                    int? idEquipo = headersTable[j].idEquipo;
                    bool? hasSide = headersTable[j].hasSide;
                    int? idHeader = headersTable[j].idHeader;

                    var registrosMVA = registrosListQuery((int)idEquipo, idTension, idMedidaMVA, from, to).ToList();
                    var registrosP = registrosListQuery((int)idEquipo, idTension, idMedidaP, from, to).ToList();
                    var registrosQ = registrosListQuery((int)idEquipo, idTension, idMedidaQ, from, to).ToList();

                    if ((bool)hasSide)
                    {
                        var registrosPLado = registrosP.Where(x => x.lado != null && x.lado != string.Empty).Distinct().ToList();
                        var registrosQLado = registrosQ.Where(x => x.lado != null && x.lado != string.Empty).Distinct().ToList();
                        var registrosMVALado = registrosMVA.Where(x => x.lado != null && x.lado != string.Empty).Distinct().ToList();
                        if (registrosP.Any())
                        {
                            foreach (var registro in registrosPLado)
                            {
                                ValorVM? valor = (valoresTimeQuery(registro.idRegistroMedida, i, idEquipo)).FirstOrDefault();
                                if (valor != null)
                                {
                                    RowValue rowValue = new RowValue
                                    {
                                        orderValue = orderValue++,
                                        idHeader = idHeader,
                                        idSubheader = headersTable[j].subheaders[0].idHeader,
                                        idValorMedida = valor.idValorMedida,
                                        valorMedida = (valor != null) ? Math.Round((double)valor.valorMedida, 2, MidpointRounding.AwayFromZero) : null
                                    };
                                    rowValues.Add(rowValue);
                                }

                            }
                        }
                        if (registrosQLado.Any())
                        {
                            foreach (var registro in registrosQLado)
                            {
                                ValorVM? valor = (valoresTimeQuery(registro.idRegistroMedida, i, idEquipo)).FirstOrDefault();
                                if (valor != null)
                                {
                                    RowValue rowValue = new RowValue
                                    {
                                        orderValue = orderValue++,
                                        idHeader = idHeader,
                                        idSubheader = headersTable[j].subheaders[1].idHeader,
                                        idValorMedida = valor.idValorMedida,
                                        valorMedida = (valor != null) ? Math.Round((double)valor.valorMedida, 2, MidpointRounding.AwayFromZero) : null
                                    };
                                    rowValues.Add(rowValue);
                                }

                            }
                        }
                        if (registrosMVALado.Any())
                        {
                            foreach (var registro in registrosMVALado)
                            {
                                ValorVM? valor = (valoresTimeQuery(registro.idRegistroMedida, i, idEquipo)).FirstOrDefault();
                                if (valor != null)
                                {
                                    RowValue rowValue = new RowValue
                                    {
                                        orderValue = orderValue++,
                                        idHeader = idHeader,
                                        idSubheader = headersTable[j].subheaders[2].idHeader,
                                        idValorMedida = valor.idValorMedida,
                                        valorMedida = (valor != null) ? Math.Round((double)valor.valorMedida, 2, MidpointRounding.AwayFromZero) : null
                                    };
                                    rowValues.Add(rowValue);
                                }

                            }
                        }

                    }
                    else
                    {
                        var registrosPSinLado = registrosP.Where(x => x.lado == null || x.lado == string.Empty).Distinct().ToList();
                        var registrosQSinLado = registrosQ.Where(x => x.lado == null || x.lado == string.Empty).Distinct().ToList();
                        var registrosMVASinLado = registrosMVA.Where(x => x.lado == null || x.lado == string.Empty).Distinct().ToList();
                        if (registrosPSinLado.Any())
                        {
                            foreach (var registro in registrosPSinLado)
                            {
                                ValorVM? valor = (valoresTimeQuery(registro.idRegistroMedida, i, idEquipo)).FirstOrDefault();
                                if(valor != null)
                                {
                                    RowValue rowValue = new RowValue
                                    {
                                        orderValue = orderValue++,
                                        idHeader = idHeader,
                                        idSubheader = headersTable[j].subheaders[0].idHeader,
                                        idValorMedida = valor.idValorMedida,
                                        valorMedida = (valor != null) ? Math.Round((double)valor.valorMedida, 2, MidpointRounding.AwayFromZero) : null
                                    };
                                    rowValues.Add(rowValue);
                                }
                                
                               
                            }
                        }
                        if (registrosQSinLado.Any())
                        {
                            foreach (var registro in registrosQSinLado)
                            {
                                ValorVM? valor = (valoresTimeQuery(registro.idRegistroMedida, i, idEquipo)).FirstOrDefault();
                                if (valor != null)
                                {
                                    RowValue rowValue = new RowValue
                                    {
                                        orderValue = orderValue++,
                                        idHeader = idHeader,
                                        idSubheader = headersTable[j].subheaders[1].idHeader,
                                        idValorMedida = valor.idValorMedida,
                                        valorMedida = (valor != null) ? Math.Round((double)valor.valorMedida, 2, MidpointRounding.AwayFromZero) : null
                                    };
                                    rowValues.Add(rowValue);
                                }
                                
                            }
                        }
                        if (registrosMVASinLado.Any())
                        {
                            foreach (var registro in registrosMVASinLado)
                            {
                                ValorVM? valor = (valoresTimeQuery(registro.idRegistroMedida, i, idEquipo)).FirstOrDefault();
                                if (valor != null)
                                {
                                    RowValue rowValue = new RowValue
                                    {
                                        orderValue = orderValue++,
                                        idHeader = idHeader,
                                        idSubheader = headersTable[j].subheaders[2].idHeader,
                                        idValorMedida = valor.idValorMedida,
                                        valorMedida = (valor != null) ? Math.Round((double)valor.valorMedida, 2, MidpointRounding.AwayFromZero) : null
                                    };
                                    rowValues.Add(rowValue);
                                }

                            }
                        }

                    }
                }

                RowBodyTable rowBodyTable = new RowBodyTable
                {
                    idHour = countHour,
                    dateHour = (i.ToString("g", CultureInfo.GetCultureInfo("es-AR"))),
                    rowValues = rowValues
                };
                bodyTable.Add(rowBodyTable);

                countHour++;
            }

            List<RowValue>? rowvalueTotal = new List<RowValue>();

            //Esto es para el total
            
            for (int j = 1; j < (headersTable.Count); j++)
            {
                int? idEquipo = headersTable[j].idEquipo;
                bool? hasSide = headersTable[j].hasSide;
                int? idHeader = headersTable[j].idHeader;
                

                var registrosMVA = registrosListQuery((int)idEquipo, idTension, idMedidaMVA, from, to).ToList();
                var registrosP = registrosListQuery((int)idEquipo, idTension, idMedidaP, from, to).ToList();
                var registrosQ = registrosListQuery((int)idEquipo, idTension, idMedidaQ, from, to).ToList();

                if ((bool)hasSide)
                {
                    var registrosPLado = registrosP.Where(x => x.lado != null && x.lado != string.Empty).Distinct().ToList();
                    var registrosQLado = registrosQ.Where(x => x.lado != null && x.lado != string.Empty).Distinct().ToList();
                    var registrosMVALado = registrosMVA.Where(x => x.lado != null && x.lado != string.Empty).Distinct().ToList();
                    
                    if (registrosP.Any())
                    {
                        double? promP = 0;
                        foreach (var registro in registrosPLado)
                        {
                            var tmpValores = (valoresQuery(registro.idRegistroMedida, registro.fInicio, registro.fFin, idEquipo)).ToList();
                            if (tmpValores.Count > 0 && tmpValores != null)
                            {
                                promP += tmpValores.Average(x => x.valorMedida);
                            }

                        }
                        RowValue rowTotal = new RowValue
                        {
                            orderValue = j,
                            idValorMedida = null,
                            idHeader = idHeader,
                            idSubheader = headersTable[j].subheaders[0].idHeader,
                            valorMedida = Math.Round((double)promP, 2, MidpointRounding.AwayFromZero)
                        };
                        rowvalueTotal.Add(rowTotal);
                    }
                    if (registrosQLado.Any())
                    {
                        double? promQ = 0;
                        foreach (var registro in registrosQLado)
                        {
                            var tmpValores = (valoresQuery(registro.idRegistroMedida, registro.fInicio, registro.fFin, idEquipo)).ToList();
                            if (tmpValores.Count > 0 && tmpValores != null)
                            {
                                promQ += tmpValores.Average(x => x.valorMedida);
                            }

                        }
                        RowValue rowTotal = new RowValue
                        {
                            orderValue = j + 1,
                            idValorMedida = null,
                            idHeader = idHeader,
                            idSubheader = headersTable[j].subheaders[1].idHeader,
                            valorMedida = Math.Round((double)promQ, 2, MidpointRounding.AwayFromZero)
                        };
                        rowvalueTotal.Add(rowTotal);
                    }
                    if (registrosMVALado.Any())
                    {
                        double? promMVA = 0;
                        foreach (var registro in registrosMVALado)
                        {
                            var tmpValores = (valoresQuery(registro.idRegistroMedida, registro.fInicio, registro.fFin, idEquipo)).ToList();
                            if (tmpValores.Count > 0 && tmpValores != null)
                            {
                                promMVA += tmpValores.Average(x => x.valorMedida);
                            }

                        }
                        RowValue rowTotal = new RowValue
                        {
                            orderValue = j + 2,
                            idValorMedida = null,
                            idHeader = idHeader,
                            idSubheader = headersTable[j].subheaders[2].idHeader,
                            valorMedida = Math.Round((double)promMVA, 2, MidpointRounding.AwayFromZero)
                        };
                        rowvalueTotal.Add(rowTotal);
                    }
                }
                else
                {
                    var registrosPLado = registrosP.Where(x => x.lado == null || x.lado == string.Empty).Distinct().ToList();
                    var registrosQLado = registrosQ.Where(x => x.lado == null || x.lado == string.Empty).Distinct().ToList();
                    var registrosMVALado = registrosMVA.Where(x => x.lado == null || x.lado == string.Empty).Distinct().ToList();

                    int i = 0;
                    if (registrosP.Any())
                    {
                        double? promP = 0;
                        foreach (var registro in registrosPLado)
                        {
                            var tmpValores = (valoresQuery(registro.idRegistroMedida, registro.fInicio, registro.fFin, idEquipo)).ToList();
                            if (tmpValores.Count > 0 && tmpValores != null)
                            {
                                promP += tmpValores.Average(x => x.valorMedida);
                            }

                        }
                        RowValue rowTotal = new RowValue
                        {
                            orderValue = j,
                            idValorMedida = null,
                            idHeader = idHeader,
                            idSubheader = headersTable[j].subheaders[i].idHeader,
                            valorMedida = Math.Round((double)promP, 2, MidpointRounding.AwayFromZero)
                        };
                        rowvalueTotal.Add(rowTotal);
                        i++;
                    }
                    if (registrosQLado.Any())
                    {
                        double? promQ = 0;
                        foreach (var registro in registrosQLado)
                        {
                            var tmpValores = (valoresQuery(registro.idRegistroMedida, registro.fInicio, registro.fFin, idEquipo)).ToList();
                            if (tmpValores.Count > 0 && tmpValores != null)
                            {
                                promQ += tmpValores.Average(x => x.valorMedida);
                            }

                        }
                        RowValue rowTotal = new RowValue
                        {
                            orderValue = j + i,
                            idValorMedida = null,
                            idHeader = idHeader,
                            idSubheader = headersTable[j].subheaders[i].idHeader,
                            valorMedida = Math.Round((double)promQ, 2, MidpointRounding.AwayFromZero)
                        };
                        rowvalueTotal.Add(rowTotal);
                        i++;
                    }
                    if (registrosMVALado.Any())
                    {
                        double? promMVA = 0;
                        foreach (var registro in registrosMVALado)
                        {
                            var tmpValores = (valoresQuery(registro.idRegistroMedida, registro.fInicio, registro.fFin, idEquipo)).ToList();
                            if (tmpValores.Count > 0 && tmpValores != null)
                            {
                                promMVA += tmpValores.Average(x => x.valorMedida);
                            }

                        }
                        RowValue rowTotal = new RowValue
                        {
                            orderValue = j + i,
                            idValorMedida = null,
                            idHeader = idHeader,
                            idSubheader = headersTable[j].subheaders[i].idHeader,
                            valorMedida = Math.Round((double)promMVA, 2, MidpointRounding.AwayFromZero)
                        };
                        rowvalueTotal.Add(rowTotal);
                    }
                }                
            }

            RowBodyTable roWTotal = new RowBodyTable
            {
                idHour = (bodyTable.OrderByDescending(x => x.idHour).FirstOrDefault().idHour + 1),
                dateHour = "Prom.",
                rowValues = rowvalueTotal
            };
            bodyTable.Add(roWTotal);

            return bodyTable;
        }
        [Obsolete]
        public TablaVM tensionBarraTabla(int idEstacion, DateTime from, DateTime to, int idTension)
        {
            List<DataTablaVM>? dataTablaVMs = new List<DataTablaVM>();
            List<string?> nombreEquipos = new List<string?>();

            var tensionInfo = _context.HistTension.Where(x => x.idTension == idTension).FirstOrDefault();
            if (tensionInfo != null) //corroboro que el id ingresado exista
            {
                int idMedida = 4; // ESTE ID ES DE TENSION
                var tmpBarras = (equipoQuery(idEstacion, 4)).ToList(); //idTipoEquipo = 4 es BARRAS

                if (tmpBarras != null && tmpBarras.Count > 0) //corroboro que no vuelva vacío
                {
                    dataTablaVMs = GetDataTable(from, to, idMedida, tmpBarras,idTension);
                    foreach (var equipo in tmpBarras)
                    {
                        nombreEquipos.Add(equipo.nombreEquipo);
                    }
                }
            }
            string labelTabla = "Tensión de barras de " + tensionInfo.valorTension + " kv";
            TablaVM tabla = new TablaVM
            {
                labelTabla = labelTabla,
                dataTablaVMs = dataTablaVMs,
                labelEncabezado = nombreEquipos
            };
            return tabla;
        }
        [Obsolete]
        public TablaVM demandaTabla(int idEstacion, DateTime from, DateTime to)
        {

            int idTipoEq = 0;

            var countTrafos = (equipoQuery(idEstacion, 1)).Count();
            var countATR = (equipoQuery(idEstacion, 2002)).Count();
            if (countTrafos != 0)
            {
                idTipoEq = 1;
            }
            else if (countATR != 0)
            {
                idTipoEq = 2002;
            }
            
            if (idTipoEq != 0)
            {
                List<DataTablaVM>? dataTablaVMs = new List<DataTablaVM>();
                var tmpTrafos = (equipoQuery(idEstacion, idTipoEq)).ToList();
                dataTablaVMs = GetDataTableDemanda(from, to, tmpTrafos);
                List<string> labelEncabezado = new List<string> { "P (MW)", "Q (Mvar)", "(MVA)" };

                TablaVM tabla = new TablaVM
                {
                    labelTabla = "Demanda Total",
                    dataTablaVMs = dataTablaVMs,
                    labelEncabezado = labelEncabezado
                };
                return tabla;
            }
            
            return null;
        }
        [Obsolete]
        private List<DataTablaVM>? GetDataTable(DateTime min, DateTime max, int idMedida, List<EquipoVM>? equipos) //este metodo esta generalizado
        {

            List<DataTablaVM>? dataTablaVMs = new List<DataTablaVM>();
            int idHora = 0;
            for (DateTime i = min; i <= max; i = i.AddMinutes(5))
            {
                List<double?> datosTabla = new List<double?>();
                string horaString;
                if (i.TimeOfDay.Ticks == 0) //probar???
                {
                    var fechaHora = (i.ToString("g", CultureInfo.GetCultureInfo("es-AR"))).Split(" "); //g Format Specifier   es-ES Culture  31/10/2008 17:04
                    var fecha = fechaHora[0].Split("/");
                    var hora = fechaHora[1];
                    var stringHora = fecha[0] + '/' + fecha[1] + ' ' + hora; //quedaría 31/10 17:04

                    horaString = stringHora;
                }
                else
                {
                    var fechaHora = (i.ToString("g", CultureInfo.GetCultureInfo("es-AR"))).Split(" "); //g Format Specifier   es-ES Culture  31/10/2008 17:04
                    var hora = fechaHora[1];
                    var stringHora = hora; //quedaría sólo 17:04

                    horaString = stringHora;
                }

                foreach (var equipo in equipos)
                {
                    var tmpRegistros = (registroQuery(idMedida, min, max, equipo.idEquipo)).ToList();
                    foreach (var registro in tmpRegistros)
                    {
                        if (tmpRegistros != null && tmpRegistros.Count > 0)
                        {
                            var tmpValores = (valoresQuery(registro.idRegistroMedida, registro.fInicio, registro.fFin, equipo.idEquipo)).ToList();
                            if (tmpValores.Count > 0 && tmpValores != null)
                            {
                                var exist = tmpValores.Where(y => y.horaMedida == i).FirstOrDefault();
                                double? valor = exist?.valorMedida;
                                
                                datosTabla.Add((valor != null) ? Math.Round((double)valor, 2, MidpointRounding.AwayFromZero) : (double)0.0);
                            }
                        }
                        else
                        {
                            datosTabla.Add(null);
                        }
                    }
                }
                DataTablaVM tablaVM = new DataTablaVM
                {
                    idHora = idHora,
                    hora = horaString,
                    Valores = datosTabla,
                };
                dataTablaVMs.Add(tablaVM);
                idHora++;
            }
                   
            return dataTablaVMs;
        }
        [Obsolete]
        private List<DataTablaVM>? GetDataTable(DateTime min, DateTime max, int idMedida, List<EquipoVM>? equipos, int idTension) //este metodo esta generalizado
        {

            List<DataTablaVM>? dataTablaVMs = new List<DataTablaVM>();
            int idHora = 0;
            for (DateTime i = min; i <= max; i = i.AddMinutes(5))
            {
                List<double?> datosTabla = new List<double?>();
                string horaString;
                if (i.TimeOfDay.Ticks == 0) //probar???
                {
                    var fechaHora = (i.ToString("g", CultureInfo.GetCultureInfo("es-AR"))).Split(" "); //g Format Specifier   es-ES Culture  31/10/2008 17:04
                    var fecha = fechaHora[0].Split("/");
                    var hora = fechaHora[1];
                    var stringHora = fecha[0] + '/' + fecha[1] + ' ' + hora; //quedaría 31/10 17:04

                    horaString = stringHora;
                }
                else
                {
                    var fechaHora = (i.ToString("g", CultureInfo.GetCultureInfo("es-AR"))).Split(" "); //g Format Specifier   es-ES Culture  31/10/2008 17:04
                    var hora = fechaHora[1];
                    var stringHora = hora; //quedaría sólo 17:04

                    horaString = stringHora;
                }

                foreach (var equipo in equipos)
                {
                    var tmpRegistros = (registroQueryTension(idMedida, min, max, equipo.idEquipo, idTension)).ToList();
                    foreach (var registro in tmpRegistros)
                    {
                        if (tmpRegistros != null && tmpRegistros.Count > 0)
                        {
                            var tmpValores = (valoresQuery(registro.idRegistroMedida, registro.fInicio, registro.fFin, equipo.idEquipo)).ToList();
                            if (tmpValores.Count > 0 && tmpValores != null)
                            {
                                var exist = tmpValores.Where(y => y.horaMedida == i).FirstOrDefault();
                                double? valor = exist?.valorMedida;
                                datosTabla.Add((valor != null) ? Math.Round((double)valor, 2, MidpointRounding.AwayFromZero) : (double)0.0);
                            }
                            else
                            {
                                datosTabla.Add(null);
                            }
                        }
                    }
                }
                DataTablaVM tablaVM = new DataTablaVM
                {
                    idHora = idHora,
                    hora = horaString,
                    Valores = datosTabla,
                };
                dataTablaVMs.Add(tablaVM);
                idHora++;
            }

            return dataTablaVMs;
        }
        [Obsolete]
        private List<DataTablaVM>? GetDataTableDemanda(DateTime min, DateTime max, List<EquipoVM>? equipos)
        {
            int idTipoEquipo = 1; //Trafos
            int idP = 1;
            int idQ = 2;
            int idPA = 3;

            List<DataTablaVM>? dataTablaVMs = new List<DataTablaVM>();
            //int idHora = 0;
            //for (DateTime i = min; i <= max; i = i.AddMinutes(5))
            //{
            //    List<double?> datosTabla = new List<double?>();
            //    string horaString;
            //    if (i.TimeOfDay.Ticks == 0) //probar???
            //    {
            //        var fechaHora = (i.ToString("g", CultureInfo.GetCultureInfo("es-AR"))).Split(" "); //g Format Specifier   es-ES Culture  31/10/2008 17:04
            //        var fecha = fechaHora[0].Split("/");
            //        var hora = fechaHora[1];
            //        var stringHora = fecha[0] + '/' + fecha[1] + ' ' + hora; //quedaría 31/10 17:04

            //        horaString = stringHora;
            //    }
            //    else
            //    {
            //        var fechaHora = (i.ToString("g", CultureInfo.GetCultureInfo("es-AR"))).Split(" "); //g Format Specifier   es-ES Culture  31/10/2008 17:04
            //        var hora = fechaHora[1];
            //        var stringHora = hora; //quedaría sólo 17:04

            //        horaString = stringHora;
            //    }

            //    var listadoP = (valoresQuery(min, max, idTipoEquipo, idP)).ToList();
            //    var listadoQ = (valoresQuery(min, max, idTipoEquipo, idQ)).ToList();
            //    var listadoPA = (valoresQuery(min, max, idTipoEquipo, idPA)).ToList();

            //    if (listadoP.Count > 0 && listadoP != null)
            //    {
            //        double? valorP = listadoP.Where(y => y.horaMedida == i).Sum(x => x.valorMedida).Value;
            //        datosTabla.Add((valorP != null) ? Math.Round((double)valorP, 2, MidpointRounding.AwayFromZero) : (double)0.0);
            //    }
            //    else
            //    {
            //        datosTabla.Add(null);
            //    }
            //    if (listadoQ.Count > 0 && listadoQ != null)
            //    {
            //        double? valorQ = listadoQ.Where(y => y.horaMedida == i).Sum(x=> x.valorMedida).Value;
            //        datosTabla.Add((valorQ != null) ? Math.Round((double)valorQ, 2, MidpointRounding.AwayFromZero) : (double)0.0);
            //    }
            //    else
            //    {
            //        datosTabla.Add(null);
            //    }
            //    if (listadoPA.Count > 0 && listadoPA != null)
            //    {
            //        double? valorPA = listadoPA.Where(y => y.horaMedida == i).Sum(x=> x.valorMedida).Value;
            //        datosTabla.Add((valorPA != null) ? Math.Round((double)valorPA, 2, MidpointRounding.AwayFromZero) : (double)0.0);
            //    }
            //    else
            //    {
            //    datosTabla.Add(null);
            //    }

            //    DataTablaVM tablaVM = new DataTablaVM
            //    {
            //        idHora = idHora,
            //        hora = horaString,
            //        Valores = datosTabla,
            //    };
            //    dataTablaVMs.Add(tablaVM);
            //    idHora++;
            //}

            return dataTablaVMs;

        }
        #endregion

    }
}

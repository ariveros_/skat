﻿using SKATDesarrollo.Context;
using SKATDesarrollo.Models;

namespace SKATDesarrollo.Repository
{
    public class ServicioLog : IServicioLog
    {
        private readonly SegSkaContext _context;

        public ServicioLog(SegSkaContext context)
        {
            _context = context;
        }

        public void LogIn(int userID)
        {
            SegLog l = new SegLog
            {
                TimeLog = DateTime.Now,
                TipoLogId = 1,
                UserId = userID
            };
            _context.SegLogs.Add(l);
            _context.SaveChanges();
        }

        public void LogOut(int userID)
        {
            SegLog l = new SegLog
            {
                TimeLog = DateTime.Now,
                TipoLogId = 2,
                UserId = userID
            };
            _context.SegLogs.Add(l);
            _context.SaveChanges();
        }
    }
}

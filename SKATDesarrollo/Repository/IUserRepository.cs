﻿using SKATDesarrollo.Models;
using SKATDesarrollo.Models.ViewModel;

namespace SKATDesarrollo.Repository
{
    public interface IUserRepository
    {
        //Autenticación
        List<SegUser> GetSegUsers();
        UsuarioVM GetLogin(string email, string password);
        UsuarioVM GetByID (int id);
        UsuarioVM GetByEmail(string email);
        UsuarioVM GetByResetToken(string token);
        //Reset password token
        void enviarMailCambioContraseña(int? idDestinatario);
        //void ValidateResetToken(string token);
        //string generateResetToken();
        //ABM Usuarios
        Object ObtenerUsuarios(bool activo);
        UsuarioVM ConsultarUser(int id);
        int AltaUser(UsuarioVM usuario);
        int ModificarUser(UsuarioVM usuario);
        int BajaRecuperarUsuario(UsuarioVM usuario);
        int CambioContraseña(int idUser, string newPassword);
        int ResetPassword(string token, string newPassword);
    }
}

﻿using SKATDesarrollo.Context;
using SKATDesarrollo.Models;
using SKATDesarrollo.Models.ViewModel;
using System.Text.Json;

namespace SKATDesarrollo.Repository
{
    public class ServicioEstacion : IServicioEstacion
    {
        private readonly SegSkaContext _context;
        public ServicioEstacion(SegSkaContext context)
        {
            _context = context;
        }
        #region ABM - Estacion
        public List<HistEstacion> AllStations()
        {
            return _context.HistEstacion.ToList();
        }
        public List<HistEstacion> AllActiveStations()
        {
            var list = _context.HistEstacion.Where(x => x.activo == true).ToList();
            var json = JsonSerializer.Serialize(list);
            return list;
        }

        //Mostrar estaciones
        public Object ObtenerEstaciones (bool activo)
        {
            var estaciones= _context.HistEstacion.Where(x => x.activo == activo).ToList();
            var json = JsonSerializer.Serialize (estaciones);
            return json;
        }
        public HistEstacion ObtenerEstacion (int id)
        {
            var estacion = _context.HistEstacion.Where(x => x.idEstacion == id).FirstOrDefault();
            if (estacion == null) return estacion;
            else return null;
        }
        //Alta
        public int AltaEstacion(HistEstacion estacion)
        {
            var consultaEstacion = _context.HistEstacion.Where(x=> x.idEstacion == estacion.idEstacion).FirstOrDefault();
            if(consultaEstacion == null)
            {
                try
                {
                    HistEstacion e = new HistEstacion()
                    {
                        nombreEstacion = estacion.nombreEstacion,
                        idZonaEstacion = estacion.idZonaEstacion,
                        idTipoEstacion = estacion.idTipoEstacion,
                        idCentroControl = estacion.idCentroControl,
                        activo = true
                    };
                    _context.HistEstacion.Add(e);
                    _context.SaveChanges();
                    return 200;
                }
                catch(Exception ex)
                {
                    return 400;
                }
            }
            else
            {
                return 422;
            }
        }
        //Baja y recuperar
        public int BajaRecuperarEstacion(int id)
        {
            var consultaEstacion = _context.HistEstacion.Where(x => x.idEstacion == id).FirstOrDefault();
            if (consultaEstacion != null)
            {
                try
                {


                    if (consultaEstacion.activo == true) //darle de baja
                    {
                        consultaEstacion.activo = false;
                    }
                    else if (consultaEstacion.activo == false) //recuperar
                    {
                        consultaEstacion.activo = true;
                    }
                    _context.SaveChanges();
                    return 200;
                }
                catch (Exception ex)
                {
                    return 400;
                }
            }
            else
            {
                return 404;
            }
        }
        //Modificar estacion
        public int ModificarEstacion(int id, string nombre, int zona)
        {
            var consultaEstacion = _context.HistEstacion.Where(x => x.idEstacion == id).FirstOrDefault();
            if(consultaEstacion != null)
            {
                try
                {
                    if(nombre != null)
                    {
                        consultaEstacion.nombreEstacion = nombre;
                    }
                    if(zona != 0)
                    {
                        consultaEstacion.idZonaEstacion = zona;
                    }
                    _context.SaveChanges();
                    return 200;
                }catch (Exception ex)
                {
                    return 400;
                }
            }
            else
            {
                return 404;
            }
        }
        #endregion
    }
}

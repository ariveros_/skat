﻿using Newtonsoft.Json.Linq;
using SKATDesarrollo.Models;
using SKATDesarrollo.Models.ViewModel;

namespace SKATDesarrollo.Repository
{
    public interface IServicioRol
    {
       List<SegRol> AllRoles();
        List<SegRol> AllRolesActivos();
        object ObtenerRoles(bool activo);
        RolVM ObtenerRol(int id);
        RolVM ConsultarRol(int id);
        int AltaRol(RolVM rol);
        int BajaRecuperarRol(int roleID);

        int ModificarRol(int idRol, string nombreRol);
        int agregarMenu(int idRol, List<MenuVM> menu);
        int eliminarMenu(int idRol, int idMenu);
        List<MenuVM> GetOtrosMenus(int idRol);
        List<MenuVM> GetMenusByRol(int idRol);
        SidebarVM GetSidebar(int idRol);

        //Usuarios
        //object ObtenerPersonasRoles(bool? activo);
        List<UsuarioRolVM> ObtenerPersonasRoles(bool? activo);
        int AltaRolUsuario(int idRol, int idUser);
        UsuarioRolVM ConsultaUnRol(int idRolUsuario);
        int ModificarRolUsuario(int idRol, int idUserRol);
        int BajaRecuperarRolUsuario(int idUserRol);

    }
}

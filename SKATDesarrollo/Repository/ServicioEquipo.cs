﻿using SKATDesarrollo.Context;
using SKATDesarrollo.Models;
using System.Text.Json;

namespace SKATDesarrollo.Repository
{
    public class ServicioEquipo : IServicioEquipo
    {
        private readonly SegSkaContext _context;
        public ServicioEquipo(SegSkaContext context)
        {
            _context = context;
        }
        #region ABM
        public List<HistEquipo> AllEquipos()
        {
            return _context.HistEquipo.ToList();
        }
        public List<HistEquipo> AllEquiposActivos()
        {
            var list = _context.HistEquipo.Where(x => x.activo == true).ToList();
            var json = JsonSerializer.Serialize(list);
            return list;
        }
        #endregion
    }
}

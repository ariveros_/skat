﻿using SKATDesarrollo.Models;

namespace SKATDesarrollo.Repository
{
    public interface IServicioTipoMedida
    {
        List<HistTipoMedida> GetHistTipoMedidas();
    }
}

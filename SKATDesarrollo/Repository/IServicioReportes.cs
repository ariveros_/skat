﻿using SKATDesarrollo.Models;
using SKATDesarrollo.Models.ViewModel;


namespace SKATDesarrollo.Repository
{
    public interface IServicioReportes
    {
        
        List<dataGraficoVM> cargaTrafosV2(int idEstacion, DateTime from, DateTime to);
        
        List<dataGraficoVM> cargaATRV2(int idEstacion, DateTime from, DateTime to);
        
        List<dataGraficoVM> tensionBarrasV2(int idEstacion, DateTime from, DateTime to);
       
        List<dataGraficoVM> porcentajeCargaTR(int idEstacion, DateTime from, DateTime to);
        List<dataGraficoVM> porcentajeCargaATR(int idEstacion, DateTime from, DateTime to);
        dataGraficoVM demandaTotal(int idEstacion, DateTime from, DateTime to);
        List<dataGraficoVM> demandaApilable(int idEstPrimera, int? idEstSegunda, DateTime from, DateTime to);
        dataGraficoVM temperaturaEstacion(int idEstacion, DateTime from, DateTime to);
       
        dataTablaVM cargaTrafosTabla(int idEstacion, DateTime from, DateTime to, string tension);
        dataTablaVM cargaATRsTabla(int idEstacion, DateTime from, DateTime to, string tension);
        dataTablaVM porcentajeCargaTrafosTabla(int idEstacion, DateTime from, DateTime to, string tension);
        dataTablaVM porcentajeCargaATRsTabla(int idEstacion, DateTime from, DateTime to, string tension);
        dataTablaVM tensionBarrasTabla(int idEstacion, DateTime from, DateTime to, string tension);
        dataTablaVM demandaTotalTabla(int idEstacion, DateTime from, DateTime to);
        dataTablaVM temperaturaAmbienteTabla(int idEstacion, DateTime from, DateTime to);
        dataTablaVM maximosminimosTRsATRs(int idEstacion, DateTime from, DateTime to);
        List<dataTablaVM> potenciasPQbarrasLineas(int idEstacion, DateTime from, DateTime to);
        List<dataTablaVM> totalesPQbarrsaLineas(int idEstacion, DateTime from, DateTime to);
        List<dataTablaVM> corrienteBarrasLineas(int idEstacion, DateTime from, DateTime to);
        List<dataTablaVM> totalesCorrienteBarrasLineas(int idEstacion, DateTime from, DateTime to);
        List<dataTablaVM> amperesLineasCDP(int idEstacion, DateTime from, DateTime to);
        List<dataTablaVM> amperesLineasAnchoris(int idEstacion, DateTime from, DateTime to);
        List<dataTablaVM> cosenoPhiLineas66ATR13(int idEstacion, DateTime from, DateTime to,Boolean? is5Min);
        Boolean pruebaCalculoPotenciaAparenteS();
        Boolean pruebaCalculoPorcentaje();
        Boolean pruebaCargaValores();
        Boolean pruebaEquipo();
        Boolean duplica();
        //Boolean pruebaPuntoScada();
        //Boolean pruebaRegistro();
    }
}

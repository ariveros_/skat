﻿using SKATDesarrollo.Context;
using SKATDesarrollo.Models;

namespace SKATDesarrollo.Repository
{
    public class ServicioTipoMedida : IServicioTipoMedida
    {
        private readonly SegSkaContext _context;
        public ServicioTipoMedida(SegSkaContext context)
        {
            _context = context;
        }

        #region ABM
        public List<HistTipoMedida> GetHistTipoMedidas()
        {
            return _context.HistTipoMedida.Where(x=> x.activo == true).ToList();
        }
        #endregion
    }
}

﻿using SKATDesarrollo.Models;
using SKATDesarrollo.Models.ViewModel;

namespace SKATDesarrollo.Repository
{
    public interface IServicioEstacion
    {
        List<HistEstacion> AllStations();
        List<HistEstacion> AllActiveStations();
        Object ObtenerEstaciones(bool activo);
        HistEstacion ObtenerEstacion(int id);
        int AltaEstacion(HistEstacion estacion);
        int BajaRecuperarEstacion(int id);
        int ModificarEstacion(int id, string nombre, int zona);

    }
}

﻿using Newtonsoft.Json.Linq;
using SKATDesarrollo.Context;
using SKATDesarrollo.Models;
using SKATDesarrollo.Models.ViewModel;
using System.Text.Json;

namespace SKATDesarrollo.Repository
{
    public class ServicioRol : IServicioRol
    {
        private readonly SegSkaContext _context;
        public ServicioRol(SegSkaContext context)
        {
            _context = context;
        }

        #region ABM ROL - PERMISO
        public List<SegRol> AllRoles()
        {
            return _context.SegRols.ToList();
        }
        public List<SegRol> AllRolesActivos()
        {
            var list = _context.SegRols.Where(x => x.Activo == true).ToList();
            var json = JsonSerializer.Serialize(list);
            return list;
        }

        //MOSTRAR UN ROL
        public Object ObtenerRoles(bool activo)
        {
                var roles = (from r in _context.SegRols
                     where r.Activo == activo
                     select new RolVM
                     {
                         activo = r.Activo,
                         roleID = r.IdRol,
                         nombreRol = r.NombreRol,
                     }).ToList();

            var json = JsonSerializer.Serialize(roles);
            return json;
        }

        public RolVM ObtenerRol(int id)
        {
            var rol = (from r in _context.SegRols
                         where r.IdRol == id
                         select new RolVM
                         {
                             roleID = r.IdRol,
                             nombreRol = r.NombreRol,
                         }).FirstOrDefault();
            if (rol != null)
            {
                return rol;
            }
            else return null;

        }
        //DETALLES de un rol
        private IQueryable<RolVM> ConsultaRol(int idRol)
        {
            var rol = (from r in _context.SegRols
                       where r.IdRol == idRol && r.Activo == true
                       select new RolVM
                       {
                           activo = r.Activo,
                           roleID = r.IdRol,
                           nombreRol = r.NombreRol,
                       });
            return rol.Distinct();
        }
        public RolVM ConsultarRol(int id)
        {
            //RolVM rol;
            var rol = (from r in _context.SegRols
                       where r.IdRol == id && r.Activo==true
                       select new RolVM
                       {
                           activo = r.Activo,
                           roleID = r.IdRol,
                           nombreRol = r.NombreRol,
                       }).FirstOrDefault();
            if (rol != null)
            {
                rol.menuVMs = new List<MenuVM>();
                var menus = (from permisos in _context.SegPermisosMenus
                             join menu in _context.SegMenus on permisos.idMenu equals menu.idMenu
                             where permisos.idRol == id && permisos.activo == true && menu.activo == true
                             select new MenuVM {
                                idMenu = permisos.idMenu,
                                nombreMenu = menu.nombreMenu,
                                codigoMenu = menu.codigoMenu,
                                urlMenu = menu.urlMenu,
                                iconoMenu = menu.iconoMenu,
                                nombreMenuOpen = menu.nombreMenuOpen,
                                esPadre = menu.esPadre,
                                posicionMenu = menu.posicionMenu,
                                idPadre = menu.idPadre,
                                activo = menu.activo
                             }).ToList();
                if(menus != null && menus.Count > 0)
                {
                    foreach(var menu in menus)
                    {
                        if (menu.esPadre != null && (bool)menu.esPadre)
                        {
                            var submenus = (from segmenus in _context.SegMenus
                                            where segmenus.idPadre == menu.idMenu && segmenus.activo == true
                                            select new MenuVM
                                            {
                                                idMenu = segmenus.idMenu,
                                                nombreMenu = segmenus.nombreMenu,
                                                codigoMenu = segmenus.codigoMenu,
                                                urlMenu = segmenus.urlMenu,
                                                iconoMenu = segmenus.iconoMenu,
                                                nombreMenuOpen = segmenus.nombreMenuOpen,
                                                esPadre = segmenus.esPadre,
                                                posicionMenu = segmenus.posicionMenu,
                                                idPadre = segmenus.idPadre,
                                                activo = segmenus.activo
                                            }).ToList();
                            if (submenus.Any()) menu.submenus = submenus;
                        }
                    }
                    rol.menuVMs.AddRange(menus);
                }
            }
            return rol;
        }

        //ALTA
        public int AltaRol(RolVM rol)
        {
            var consultaRol = _context.SegRols.Where(x => x.IdRol == rol.roleID).FirstOrDefault();

            if (consultaRol == null)
            {
                try
                {
                    SegRol r = new SegRol
                    {
                        NombreRol = rol.nombreRol,
                        Activo = true,
                    };

                    _context.SegRols.Add(r);
                    _context.SaveChanges();

                    if (rol.menuVMs != null)
                    {
                        var roleId = _context.SegRols.Where(x => x.Activo == true).OrderByDescending(x => x.IdRol).FirstOrDefault().IdRol;
                        foreach (var menu in rol.menuVMs)
                        {
                            SegPermisosMenu segPermisosMenu = new SegPermisosMenu
                            {
                                idRol = rol.roleID,
                                idMenu = menu.idMenu,
                                activo = true
                            };
                            _context.SegPermisosMenus.Add(segPermisosMenu);
                        }
                        _context.SaveChanges();
                    }

                    return 200; //si está bien
                }
                catch (Exception)
                {
                    return 400; //error
                }
            }
            else
            {
                return 422; // si ya existe
            }

        }

        //BAJA
        public int BajaRecuperarRol(int idRol)
        {
            var consultaRol = _context.SegRols.Where(x => x.IdRol == idRol).FirstOrDefault();
            var consultaPermisos = _context.SegPermisosMenus.Where(x => x.idRol == idRol).ToList();

            if (consultaRol != null)
            {
                if (consultaRol.Activo == true) // eliminar
                {
                    consultaRol.Activo = false;
                    foreach (var menu in consultaPermisos)
                    {
                        menu.activo = false;
                    }
                }
                else if (consultaRol.Activo == false) //recuperar
                {
                    consultaRol.Activo = true;
                    foreach (var menu in consultaPermisos)
                    {
                        menu.activo = true;
                    }
                }
                _context.SaveChanges();
                return 200;
            }
            return 404;
        }

        //MODIFICAR
        public int ModificarRol(int idRol, string nuevoNombreRol)
        {
            try
            {
                var consultaRol = _context.SegRols.Where(x => x.IdRol == idRol).FirstOrDefault();
                if (consultaRol != null)
                {
                    //Rol
                    if (nuevoNombreRol != null)
                    {
                        consultaRol.NombreRol = nuevoNombreRol;
                        _context.SaveChanges();
                    }

                    return 200;
                }
                else return 422;
            }
            catch (Exception)
            {
                return 404;
            }
        }
        public int agregarMenu(int idRol, List<MenuVM> menu)
        {
            try
            {
                var consultaRol = _context.SegRols.Where(x => x.IdRol == idRol && x.Activo == true).FirstOrDefault(); //traigo rol
                if (consultaRol != null)
                {
                    if (menu != null && menu.Count > 0)
                    {
                        foreach (var i in menu)
                        {
                            var consultaPermiso = _context.SegPermisosMenus.Where(x => x.idMenu == i.idMenu && x.idRol == idRol).FirstOrDefault();
                            if (consultaPermiso == null)
                            {
                                SegPermisosMenu permiso = new SegPermisosMenu
                                {
                                    idRol = idRol,
                                    idMenu = i.idMenu,
                                    activo = true
                                };
                                _context.SegPermisosMenus.Add(permiso);
                                _context.SaveChanges();
                            }
                        }
                    }
                    else return 422;
                }
                return 200;
            }
            catch (Exception)
            {
                return 404;
            }
        }
        public int eliminarMenu(int idRol, int idMenu)
        {
            try
            {
                var consultaRol = _context.SegRols.Where(x => x.IdRol == idRol).FirstOrDefault(); //traigo rol
                if (consultaRol != null)
                {
                    var consultaPermiso = _context.SegPermisosMenus.Where(x => x.idRol == idRol && x.idMenu == idMenu).FirstOrDefault();
                    if (consultaPermiso != null)
                    {
                        consultaPermiso.activo = false;
                    }
                    else return 422;
                    _context.SaveChanges();
                }
                return 200;
            }
            catch (Exception)
            {
                return 404;
            }
        }
        private IQueryable<MenuVM> AllMenusList()
        {
            var consultaMenusRol = (from menu in _context.SegMenus
                                    where menu.activo == true
                                    select new MenuVM
                                    {
                                        idMenu = menu.idMenu,
                                        nombreMenu = menu.nombreMenu,
                                        codigoMenu = menu.codigoMenu,
                                        urlMenu = menu.urlMenu,
                                        iconoMenu = menu.iconoMenu,
                                        nombreMenuOpen = menu.nombreMenuOpen,
                                        esPadre = menu.esPadre,
                                        posicionMenu = menu.posicionMenu,
                                        idPadre = menu.idPadre,
                                        activo = menu.activo
                                    });
            return consultaMenusRol.Distinct();
        }
        private IQueryable<MenuVM> AllMenusListWOSub()
        {
            var consultaMenusRol = (from menu in _context.SegMenus
                                    where menu.activo == true && menu.posicionMenu == null
                                    select new MenuVM
                                    {
                                        idMenu = menu.idMenu,
                                        nombreMenu = menu.nombreMenu,
                                        codigoMenu = menu.codigoMenu,
                                        urlMenu = menu.urlMenu,
                                        iconoMenu = menu.iconoMenu,
                                        nombreMenuOpen = menu.nombreMenuOpen,
                                        esPadre = menu.esPadre,
                                        posicionMenu = menu.posicionMenu,
                                        idPadre = menu.idPadre,
                                        activo = menu.activo
                                    });
            return consultaMenusRol.Distinct();
        }
        private IQueryable<MenuVM> AllMenusByRol(int idRol)
        {
            var consultaMenusRol = (from rol in _context.SegRols
                                    join permisos in _context.SegPermisosMenus on rol.IdRol equals permisos.idRol
                                    join menu in _context.SegMenus on permisos.idMenu equals menu.idMenu
                                    where rol.Activo == true && menu.activo == true && permisos.activo == true && rol.IdRol == idRol
                                    && menu.posicionMenu == null
                                    select new MenuVM
                                    {
                                        idMenu = menu.idMenu,
                                        nombreMenu = menu.nombreMenu,
                                        codigoMenu = menu.codigoMenu,
                                        urlMenu = menu.urlMenu,
                                        iconoMenu = menu.iconoMenu,
                                        nombreMenuOpen = menu.nombreMenuOpen,
                                        esPadre = menu.esPadre,
                                        posicionMenu = menu.posicionMenu,
                                        idPadre = menu.idPadre,
                                        activo = menu.activo
                                    });
            return consultaMenusRol.Distinct();
        }
        private IQueryable<MenuVM> GetChildren(int idPadre)
        {
            var consultaMenusRol = (from menu in _context.SegMenus
                                    where menu.activo == true && menu.idPadre == idPadre
                                    select new MenuVM
                                    {
                                        idMenu = menu.idMenu,
                                        nombreMenu = menu.nombreMenu,
                                        codigoMenu = menu.codigoMenu,
                                        urlMenu = menu.urlMenu,
                                        iconoMenu = menu.iconoMenu,
                                        nombreMenuOpen = menu.nombreMenuOpen,
                                        esPadre = menu.esPadre,
                                        posicionMenu = menu.posicionMenu,
                                        idPadre = menu.idPadre,
                                        activo = menu.activo
                                    });
            return consultaMenusRol.Distinct();
        }
        public List<MenuVM> GetOtrosMenus(int idRol)
        {
            List<MenuVM> menus = new List<MenuVM>();
            try
            {
                var consultaRol = _context.SegRols.Where(x => x.IdRol == idRol).FirstOrDefault(); //traigo rol
                if (consultaRol != null)
                {
                    var consultaMenusRol = AllMenusByRol(idRol).ToList();
                    var consultaMenus = AllMenusListWOSub().ToList();
                    foreach(var menu in consultaMenus)
                    {
                        if(!consultaMenusRol.Contains(menu) && menu.idMenu > 4)
                        {
                            menus.Add(menu);
                        }
                    }

                }
                return menus;
            }
            catch (Exception)
            {
                return menus;
            }
        }
        public List<MenuVM> GetMenusByRol(int idRol) //ordenados y sin submenus
        {
            List<MenuVM> menus = new List<MenuVM>();
            try
            {
                var consultaRol = _context.SegRols.Where(x => x.IdRol == idRol).FirstOrDefault(); //traigo rol
                if (consultaRol != null)
                {
                    var consultaMenusRol = AllMenusByRol(idRol).OrderBy(x=>x.idMenu).ToList();
                    if(consultaMenusRol.Count > 0)
                    {
                        menus.AddRange(consultaMenusRol);
                    }
                }
                return menus;
            }
            catch (Exception)
            {
                return menus;
            }
        }
        public SidebarVM GetSidebar(int idRol)
        {
            List<string> jsonMenu = new List<string>();
            RolVM rol = ConsultaRol(idRol).FirstOrDefault();
            List<MenuVM> menus = GetMenusByRol(idRol);
            if (menus.Count > 0)
            {
                foreach (var menu in menus)
                {
                    if (menu.idPadre == null && menu.esPadre == false) //No es hijo ni padre
                    {
                        string item =
                            "<li className={ this.isPathActive('" + menu.urlMenu + "') ? 'nav-item menu-items active' : 'nav-item menu-items' }>"
                             + "<Link className = 'nav-link' to = '" + menu.urlMenu + "'>"
                            + "<span className='menu-icon'><i className='" + menu.iconoMenu + "'></i></span>"
                            + "< span className = 'menu-title' >" + menu.nombreMenu + "</ span >"
                            + "</Link> </ li > ";

                        jsonMenu.Add(item);
                    }
                    if (menu.esPadre == true) //es padre
                    {
                        List<MenuVM> childMenus = GetChildren((int)menu.idMenu).OrderBy(x => x.posicionMenu).ToList();
                        List<string> childrenItems = new List<string>();
                        foreach (var child in childMenus)
                        {
                            string childItem =
                                "<li className='nav-item'> <Link className={ this.isPathActive('" + child.urlMenu + "') ? 'nav-link active' : 'nav-link' } to='" + child.urlMenu + "'>" + child.nombreMenu + "</Link></li>"
                                + " ";
                            childrenItems.Add(childItem);
                        }
                        string itemPadre =
                            "<li className={ this.isPathActive('" + menu.urlMenu + "') ? 'nav-item menu-items active' : 'nav-item menu-items' }>"
                            + " <div className={ this.state." + menu.nombreMenuOpen + " ? 'nav-link menu-expanded' : 'nav-link' } onClick={ () => this.toggleMenuState('" + menu.urlMenu + "') } data-toggle='collapse'>"
                            + "<span className='menu-icon'>< i className =" + menu.iconoMenu + "></i> </span >"
                            + "< span className='menu-title'> " + menu.nombreMenu + "</span> <i className = 'menu-arrow'> </i> </div>"
                            + "<Collapse in={ this.state." + menu.nombreMenuOpen + "}>"
                            + "<div> <ul className='nav flex-column sub-menu'>"
                            + string.Join(" ", childrenItems)
                            + "</ul> </div> </Collapse>"
                            + "</li>";
                        jsonMenu.Add(itemPadre);
                    }
                }
            }
            var list = string.Join(" ", jsonMenu);
            SidebarVM sidebarVM = new SidebarVM
            {
                idRol = idRol,
                nombreRol = rol.nombreRol,
                sidebar = list
            };
            return sidebarVM;
        }
        #endregion

        #region ABM ROLES - USUARIOS
        //Obtener roles
        public List<UsuarioRolVM> ObtenerPersonasRoles(bool? activo)
        {
            var listaUsuarioRol = (from u in _context.SegUserRols
                                   join r in _context.SegRols on u.RoleId equals r.IdRol
                                   where u.Activo == activo //agregarle que no muestre el FullAdmin ??
                                   select new UsuarioRolVM
                                   {
                                       IdUserRol = u.IdUserRol,
                                       IdRol = u.RoleId,
                                       nombreRol = r.NombreRol,
                                       IdUser = u.UserId,
                                       activo = u.Activo,
                                   }).ToList();
            foreach(var item in listaUsuarioRol)
            {
                var uMail = _context.SegUsers.Where(x => x.IdUser == item.IdUser).FirstOrDefault().Email;
                
                if(uMail != null)
                {
                    item.email = uMail;
                }
            }

            return listaUsuarioRol;
            
        }
        //CONSULTA
        public UsuarioRolVM ConsultaUnRol(int idRolUsuario)
        {

            var rol = (from u in _context.SegUserRols
                       join r in _context.SegRols on u.RoleId equals r.IdRol
                       join user in _context.SegUsers on u.UserId equals user.IdUser
                       where u.IdUserRol == idRolUsuario
                       select new UsuarioRolVM()
                       {
                           IdUserRol = idRolUsuario,
                           IdRol = u.RoleId,
                           IdUser = u.UserId,
                           activo = u.Activo,
                           email = user.Email,
                           nombreRol = r.NombreRol
                       }).FirstOrDefault();

            return rol;
        }
        //ALTA
        public int AltaRolUsuario(int idRol, int idUser)
        {
            try
            {
                var consultaRol = _context.SegUserRols.Where(x => x.UserId == idUser && x.RoleId == idRol).FirstOrDefault();
                if (consultaRol == null)
                {
                    SegUserRol u = new SegUserRol
                    {
                        RoleId = idRol,
                        UserId = idUser,
                        Activo = true
                    };
                    _context.SegUserRols.Add(u);
                    _context.SaveChanges();
                    return 200;
                }
                else
                {
                    return 422; //ya existe
                }
            }
            catch
            {
                return 404;
            }
            
        }
        //MODIFICACIONES
        public int ModificarRolUsuario(int idRol, int idUserRol)
        {
            try
            {
                var consultaRol = _context.SegUserRols.Where(x => x.IdUserRol == idUserRol && x.Activo==true).FirstOrDefault();
                if (consultaRol != null)
                {
                    consultaRol.RoleId = idRol;
                    consultaRol.Activo = true;
                    //consultaRol.UserId = consultaRol.UserId; //el que ya tenia, por las dudas
                }
                    _context.SaveChanges();
                    return 200;
            }
            catch (Exception)
            {
                return 404;
            }
        }
        //BAJA - RECUPERAR
        public int BajaRecuperarRolUsuario(int idUserRol)
        {
            var consultaRol = _context.SegUserRols.Where(x => x.IdUserRol == idUserRol).FirstOrDefault();
            if (consultaRol != null)
            {
                if(consultaRol.Activo == true) //eliminar
                {
                    consultaRol.Activo = false;

                } 
                else if (consultaRol.Activo == false) //recuperar
                {
                    consultaRol.Activo = true;
                }
                _context.SaveChanges();
                return 200;
            }
            else return 404;
        }

        #endregion

    }
}

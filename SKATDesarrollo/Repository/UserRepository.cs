﻿
using SKATDesarrollo.Models;
using SKATDesarrollo.Context;
using SKATDesarrollo.Models.ViewModel;
using Microsoft.EntityFrameworkCore;
using System.Text.Json;
using System.Security.Cryptography;
using SKATDesarrollo.Utilities;

namespace SKATDesarrollo.Repository
{
    public class UserRepository : IUserRepository
    {
        private readonly SegSkaContext _context;
        private readonly IEnvioMails _mails;
        public UserRepository(SegSkaContext context, IEnvioMails mails )
        {
            _context = context;
            _mails = mails;
        }

        public List<SegUser> GetSegUsers()
        {
            return _context.SegUsers.Where(x=>x.ActivoUser == true).ToList();
        }
        #region AUTH
        public UsuarioVM GetLogin(string email, string password)
        {
            var user = _context.SegUsers.Where(u => u.Email == email).FirstOrDefault();
            
            var usuarioVM = new UsuarioVM();
            if (user != null) { usuarioVM.email = user.Email; usuarioVM.userID = user.IdUser;
                var pass = _context.SegPasswords.Where(p => p.UserId == user.IdUser && p.Hasta == null).FirstOrDefault();
                if (pass != null) { usuarioVM.password = pass.HashPassword; }
            }
            return usuarioVM;
        }
        public UsuarioVM GetByID(int id)
        {
            var user = _context.SegUsers.Where(u => u.IdUser == id).FirstOrDefault();
            var pass = _context.SegPasswords.Where(p => p.UserId == user.IdUser && p.Hasta == null).FirstOrDefault();
            var rolUser = _context.SegUserRols.Where(r => r.UserId == user.IdUser).FirstOrDefault();
            var rolDatos = _context.SegRols.Where(r => r.IdRol == rolUser.RoleId).FirstOrDefault();

            //Creo el usuario de manera poco eficiente :)
            var usuarioVM = new UsuarioVM();
            if (user != null) { usuarioVM.email = user.Email; usuarioVM.userID = user.IdUser; usuarioVM.activo = user.ActivoUser; }
            if (pass != null) usuarioVM.password = pass.HashPassword;
            if (rolUser != null) {usuarioVM.idRol = rolUser.RoleId;}
            if (rolDatos != null) { usuarioVM.rolName = rolDatos.NombreRol; }

            return usuarioVM;
        }
        public UsuarioVM GetByEmail(string email)
        {
            var user = _context.SegUsers.Where(x => x.Email == email).FirstOrDefault();
            var usuarioVM = new UsuarioVM();
            if(user != null)
            {
                usuarioVM.userID = user.IdUser;
                usuarioVM.email = user.Email;
                usuarioVM.activo = user.ActivoUser;

                //Busco más info del user
                var pass = _context.SegPasswords.Where(p => p.UserId == user.IdUser && p.Hasta == null).FirstOrDefault();
                var rolUser = _context.SegUserRols.Where(r => r.UserId == user.IdUser).FirstOrDefault();
                var rolDatos = _context.SegRols.Where(r => r.IdRol == rolUser.RoleId).FirstOrDefault();

                //la guardo en el VM
                if (pass != null && rolUser != null && rolDatos != null)
                {
                    usuarioVM.password = pass.HashPassword;
                    usuarioVM.idRol = rolDatos.IdRol;
                    usuarioVM.rolName = rolDatos.NombreRol;
                }

            }
            return usuarioVM ;
        }
        public UsuarioVM GetByResetToken(string token)
        {
            var user = _context.SegUsers.SingleOrDefault(x=> x.ResetToken == token && x.ResetTokenExpires > DateTime.UtcNow);

            var usuarioVM = new UsuarioVM();
            if (user != null)
            {
                usuarioVM.userID = user.IdUser;
                usuarioVM.activo = user.ActivoUser;
                usuarioVM.email = user.Email;
                //Busco más info del user
                var pass = _context.SegPasswords.Where(p => p.UserId == user.IdUser && p.Hasta == null).FirstOrDefault();
                var rolUser = _context.SegUserRols.Where(r => r.UserId == user.IdUser).FirstOrDefault();
                var rolDatos = _context.SegRols.Where(r => r.IdRol == rolUser.RoleId).FirstOrDefault();

                //la guardo en el VM
                if (pass != null && rolUser != null && rolDatos != null)
                {
                    usuarioVM.password = pass.HashPassword;
                    usuarioVM.idRol = rolDatos.IdRol;
                    usuarioVM.rolName = rolDatos.NombreRol;
                }
            }
            return usuarioVM;
        }


        public void enviarMailCambioContraseña(int? idDestinatario)
        {
            //código
            var user = _context.SegUsers.Where(x=> x.IdUser == idDestinatario).FirstOrDefault();

            if (user == null) return;

            user.ResetToken = generateResetToken();
            user.ResetTokenExpires = DateTime.UtcNow.AddDays(1);
            
            _context.SegUsers.Update(user);
            _context.SaveChanges();

            //mail
            var destinatario = user.Email;
            var subject = "Recuperación de contraseña";
            var body = "<table style='border: 10px solid #edeceb; padding-left: 20px;' width='100%'><tbody> <tr> <td><br /><br /><div style='color: #353543; font-family: Arial,sans-serif; font-size: 14px; line-height: 22px;'><br /> <strong>Estimado/a usuario/a: </strong><p> para cambiar su contraseña inserte el siguiente código de verificación:  " +
                        " <br /></p><br />" +
                        "<strong>"+ user.ResetToken + "</strong>"+
                        "</div></td></tr></tbody></table><table style='color: #353543; font-family: Arial; font-size: 10px; line-height: 22px; padding-left: 25px;' border='0' width='100%' bgcolor='#EDECEB'> <tbody><tr><td align='right'><p> Email enviado automaticamente, no responder.</p></td></tr></tbody></table>";

            _mails.Send(destinatario,subject, body);
        }
        //public void ValidateResetToken(string token)
        //{
        //    GetByResetToken(token);
        //}
        private string generateResetToken()
        {
            // token is a cryptographically strong random sequence of values
            var token = Convert.ToHexString(RandomNumberGenerator.GetBytes(64));

            // ensure token is unique by checking against db
            var tokenIsUnique = !_context.SegUsers.Any(x => x.ResetToken == token);
            if (!tokenIsUnique)
                return generateResetToken();

            return token;
        }
        #endregion

        #region ABM Usuarios
        //Obtener usuarios
        public Object ObtenerUsuarios(bool activo)
        {
            var users = (from u in _context.SegUsers
                         where u.ActivoUser == activo
                         select new UsuarioVM
                         {
                             activo = u.ActivoUser,
                             userID = u.IdUser,
                             email = u.Email,
                         }).ToList();
            var json = JsonSerializer.Serialize(users);
            return json;
        }
        //Consultar
        public UsuarioVM ConsultarUser(int id)
        {
            var user = (from u in _context.SegUsers
                        where u.IdUser == id 
                        select new UsuarioVM
                        {
                            userID=u.IdUser,
                            activo = u.ActivoUser,
                            email=u.Email,

                        }).FirstOrDefault();
            return user;
        }
        //Alta
        public int AltaUser(UsuarioVM usuario)
        {
            var consultaUsuario = _context.SegUsers.Where(x => x.IdUser == usuario.userID).FirstOrDefault();

            if (consultaUsuario == null)
            {
                try
                {
                    SegUser u = new SegUser
                    {
                        Email = usuario.email,
                        ActivoUser = true,
                    };

                    _context.SegUsers.Add(u);
                    _context.SaveChanges();

                    if(usuario.password != null)
                    {
                        var userID = _context.SegUsers.OrderByDescending(x => x.IdUser).FirstOrDefault().IdUser;
                        SegPassword segPassword = new SegPassword
                        {
                            UserId = userID,
                            ActivoPass = true,
                            Desde = DateTime.Today,
                            HashPassword = BCrypt.Net.BCrypt.HashPassword(usuario.password)
                        };
                        _context.SegPasswords.Add(segPassword);
                        _context.SaveChanges();
                    }

                    return 200; //si está bien
                }
                catch (Exception ex)
                {
                    return 400; //error
                }
            }
            else
            {
                return 422; // si ya existe
            }
        }
       //Modificar
       public int ModificarUser(UsuarioVM usuario)
        {
            try
            {
                var consultaUsuario = _context.SegUsers.Where(x => x.IdUser == usuario.userID).FirstOrDefault();
                
                if (consultaUsuario != null)
                {
                    if (usuario.email != "")
                    {
                        //User
                        consultaUsuario.Email = usuario.email;
                    }
                    if(usuario.password != "")
                    {
                        var consultaPassword = _context.SegPasswords.Where(x => x.UserId == usuario.userID && x.Hasta == null).FirstOrDefault(); //que traiga la ultima pass
                        
                        if(consultaPassword != null) consultaPassword.Hasta = DateTime.Now; //si existe, que se le asigne que ya terminó

                        //CUIDADO, cuando la contraseña es reestablecida por un administrador NO SE COMPRUEBA que sea distinta a la vieja
                        SegPassword segPassword = new SegPassword
                        {
                            UserId = consultaUsuario.IdUser,
                            ActivoPass = true,
                            Desde = DateTime.Today,
                            HashPassword = BCrypt.Net.BCrypt.HashPassword(usuario.password)
                        };
                        _context.SegPasswords.Add(segPassword);
                    }

                    _context.SaveChanges();
                    return 200;
                }
                else return 422;
            }
            catch (Exception ex)
            {
                return 404;
            }
        }

        //Baja y Recuperar
        public int BajaRecuperarUsuario(UsuarioVM usuario)
        {
            var consultaUsuario = _context.SegUsers.Where(x => x.IdUser == usuario.userID).FirstOrDefault();
            var listPass = _context.SegPasswords.Where(x => x.UserId == usuario.userID).ToList(); 

            if (consultaUsuario != null)
            {
                if (consultaUsuario.ActivoUser == true) // eliminar
                {
                    consultaUsuario.ActivoUser = false;
                    foreach (var consultaPass in listPass)
                    {
                        consultaPass.ActivoPass = false;
                        
                        //if(consultaPass.Hasta == null)
                        //{
                        //    consultaPass.Hasta = DateTime.Now;
                        //}
                    }
                }
                else if (consultaUsuario.ActivoUser == false) //recuperar
                {
                    consultaUsuario.ActivoUser = true;
                    foreach (var consultaPass in listPass)
                    {
                        consultaPass.ActivoPass = true;
                    }
                }
                _context.SaveChanges();
                return 200;
            }
            return 404;
        }
        //Cambio de contraseña normal
        public int CambioContraseña(int idUser, string newPassword)
        {
            try
            {
                var consultaUsuario = _context.SegUsers.Where(x => x.IdUser == idUser).FirstOrDefault();
                var list1 = (from p in _context.SegPasswords
                             where p.UserId == idUser
                             //orderby p.Desde //creo que es descending
                             select p).OrderByDescending(p=> p.Desde).Take(5).ToList();

                if (consultaUsuario != null)
                {
                    var count = 0;
                    if (list1.Count > 0)
                    {
                        foreach (var pass in list1)
                        {
                            if (BCrypt.Net.BCrypt.Verify(newPassword, pass.HashPassword))
                            {
                                count++;
                            }
                        }
                    }
                    if (count == 0)
                    {
                        var contraseñaActual = _context.SegPasswords.Where(x => x.UserId == idUser && x.Hasta == null).FirstOrDefault();
                        if(contraseñaActual != null) contraseñaActual.Hasta = DateTime.Today;

                        SegPassword segPassword = new SegPassword
                        {
                            UserId = idUser,
                            ActivoPass = true,
                            Desde = DateTime.Today,
                            HashPassword = BCrypt.Net.BCrypt.HashPassword(newPassword)
                        };
                        _context.SegPasswords.Add(segPassword);
                        _context.SaveChanges();

                        return 200;
                    }
                    else
                    {
                        return 422;
                    }
                }
                else return 404;

            }
            catch(Exception ex)
            {
                return 404;
            }

        }
        //Reset de contraseña
        public int ResetPassword(string token, string newPassword)
        {
            try
            {
                var usuarioVM = GetByResetToken(token);

                var consultaUsuario = _context.SegUsers.Where(x => x.IdUser == usuarioVM.userID).FirstOrDefault();

                if (consultaUsuario != null)
                {

                    var list1 = (from p in _context.SegPasswords
                                 where p.UserId == consultaUsuario.IdUser
                                 select p).OrderByDescending(p => p.Desde).Take(5).ToList();


                    var count = 0;
                    if (list1.Count > 0)
                    {
                        foreach (var pass in list1)
                        {
                            if (BCrypt.Net.BCrypt.Verify(newPassword, pass.HashPassword))
                            {
                                count++;
                            }
                        }
                    }
                    if (count == 0)
                    {
                        //Limpiamos reset
                        consultaUsuario.ResetToken = null;
                        consultaUsuario.ResetTokenExpires = null;

                        _context.SegUsers.Update(consultaUsuario);
                        //Cambio de contraseña
                        var contraseñaActual = _context.SegPasswords.Where(x => x.UserId == consultaUsuario.IdUser && x.Hasta == null).FirstOrDefault();
                        if (contraseñaActual != null) contraseñaActual.Hasta = DateTime.Today;

                        SegPassword segPassword = new SegPassword
                        {
                            UserId = consultaUsuario.IdUser,
                            ActivoPass = true,
                            Desde = DateTime.Today,
                            HashPassword = BCrypt.Net.BCrypt.HashPassword(newPassword)
                        };
                        _context.SegPasswords.Add(segPassword);
                        _context.SaveChanges();

                        return 200;
                    }
                    else
                    {
                        return 422;
                    }
                }
                else return 404;

            }
            catch (Exception ex)
            {
                return 400;
            }

        }

        #endregion

    }
}

using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using SKATDesarrollo.Context;
using SKATDesarrollo.Controllers.MIddleware;
using SKATDesarrollo.Helpers;
using SKATDesarrollo.Repository;
using SKATDesarrollo.Utilities;
using System.IdentityModel.Tokens.Jwt;
using System.Text;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
//builder.Services.AddCors();
builder.Services.AddCors(options =>
{
    options.AddDefaultPolicy(builder =>
    {
        builder.WithOrigins("http://localhost:3001/", "http://localhost:3000/", "http://172.16.1.4:3001", "https://172.173.176.147:4436", "http://172.16.1.4:8086")
               .AllowAnyHeader()
               .AllowAnyMethod()
               .AllowCredentials();
    });
});

builder.Services.AddControllers();

builder.Services.AddDbContext<SegSkaContext>(options => options.UseSqlServer(
    builder.Configuration.GetConnectionString("Local")));

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();


builder.Services.AddScoped<IUserRepository, UserRepository>(); //El repositorio es un tipo de IRepository
builder.Services.AddScoped<IJWTService,JWTService>();
builder.Services.AddScoped<IEnvioMails, EnvioMails>();
builder.Services.AddScoped<IServicioLog, ServicioLog>();
builder.Services.AddScoped<IServicioRol, ServicioRol>();
builder.Services.AddScoped<IServicioEstacion, ServicioEstacion>();
builder.Services.AddScoped<IServicioEquipo, ServicioEquipo>();
builder.Services.AddScoped<IServicioTipoMedida, ServicioTipoMedida>();
builder.Services.AddScoped<IServicioReportes, ServicioReportes>();
builder.Services.AddScoped<IServicioTension, ServicioTension>();


builder.Services.AddAuthentication(
    JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(options =>
    {
        options.TokenValidationParameters = new TokenValidationParameters
        {
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(builder.Configuration.GetSection("AppSettings:secureKey").Value)),
            ValidateIssuerSigningKey = true,
            ValidateIssuer = false,
            ValidateAudience = false
        };

    });



var app = builder.Build();

// Configure the HTTP request pipeline.
app.UseHttpsRedirection();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}


app.UseCors(options =>
{
    options.WithOrigins("http://localhost:3001", "http://localhost:3000", "http://192.168.220.56:3001", "http://172.16.1.4:3001", "https://172.16.1.4:3001", "https://172.16.1.4:4436", "http://172.16.1.4:8086");
    options.AllowAnyMethod();
    options.AllowAnyHeader();
    options.AllowCredentials();

});




app.UseMiddleware<JWTHeaderMiddleware>();
app.UseAuthentication(); //debe estar arriba de useAuth
app.UseAuthorization();

app.MapControllers();

app.Run();
